#!/bin/sh


FILE_APPS="abt3000-uitp-arena-antp-2023"
FILE_BARCO="barcode"
FILE_MOTI="moti"
FILE_START="demo_qt5.sh"
FILE_MOTISH="moti.sh"


chmod +x $FILE_APPS
chmod +x $FILE_MOTI
chmod +x $FILE_BARCO
chmod +x $FILE_START
chmod +x $FILE_MOTISH
chmod +x start_moti.sh


echo "copy files ..."

cp $FILE_APPS        /usr/bin
cp $FILE_MOTI        /usr/bin
cp $FILE_BARCO       /usr/bin
cp $FILE_START       /usr/bin
cp $FILE_MOTISH      /usr/bin
cp start_moti.sh     /usr/bin


rm $FILE_APPS
rm $FILE_MOTI
rm $FILE_BARCO
rm $FILE_START
rm $FILE_MOTISH
rm start_moti.sh

rm install_demo.sh
