#!/bin/sh


CONFIG_FILE=/etc/demo/demo.config


if [ ! -f $CONFIG_FILE ] 
then
	echo "No demo config file <$CONFIG_FILE>"
    exit
fi

RET=`cat $CONFIG_FILE | grep -i AUTO-START |cut -c21-` 

if [ "$RET" == "YES" ]
then
	echo "Autostart demo enabled"
else
	echo "Autostart demo disabled"
    exit
fi

/usr/bin/demo_qt5.sh &
