#!/bin/sh


FILE_APPS="abt3000-uitp-arena-antp-2023"
FILE_DEMO_ON="autostart-qt5-demo-on.sh"
FILE_DEMO_OFF="autostart-qt5-demo-off.sh"
FILE_CONFIG="demo.config"
FILE_INIT="demo-init.sh"
FILE_MOTI="start_moti.sh"
FILE_START="demo_qt5.sh"




chmod +x $FILE_APPS
chmod +x $FILE_DEMO_ON
chmod +x $FILE_DEMO_OFF
chmod +x $FILE_INIT
chmod +x $FILE_MOTI
chmod +x $FILE_START


echo "copy files ..."

cp $FILE_APPS           /usr/bin
cp $FILE_DEMO_ON        /usr/bin
cp $FILE_DEMO_OFF       /usr/bin
cp $FILE_START          /usr/bin
cp $FILE_MOTI           /usr/bin


mkdir -p /etc/demo
cp $FILE_CONFIG           /etc/demo/

cp $FILE_INIT /etc/init.d/
ln -sf /etc/init.d/$FILE_INIT /etc/rc5.d/S46$$FILE_INIT

sync
beep
echo "========================="
echo "Rebooting ..."
echo "========================="
sleep 2
reboot

