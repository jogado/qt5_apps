#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>

QString aTmp;
QString aTmp_barco;

int ret=0;

#define SPLASH_SCREEN     0
#define MAIN_SCREEN       1
#define SERVICE_SCREEN    2

int TOUCHSCREEN=0;
int DATA_LIVE_TIME=10;

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label2;
QString Label3;

char aBufOld[256];
char *pBufOld;

int BARCODE_IS_PRESENT = 0;

char Currency_char[10];
int NbAdult=1;
int NbChild=0;
float Total=0;
float fAdult=2.2;
float fChild=1.5;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    pBufOld = &aBufOld[0];

    ui->setupUi(this);

    ui->Counter->setAttribute   (Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute (Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;

    ret=system("echo \"-----\" > /tmp/text.txt");
    ret=system("echo \"\" > /tmp/moti.data");
    ret=system("echo \"\" > /tmp/rakinda.data");

    ui->TheDate->setVisible         (false);
    ui->TheTime->setVisible         (false);
    ui->pb_ValidCard->setVisible    (false);
    ui->pb_InvalidCard->setVisible  (false);
    ui->free_text->setVisible       (false);
    ui->Counter_2->setVisible       (true);


    ui->AdultCount->setVisible       (false);

    ui->label_header_00->setVisible       (false);
    ui->label_header_08->setVisible       (false);
    ui->label_header_09->setVisible       (false);



    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));


    read_header(0);
    read_header(1);
    read_header(2);
    read_header(3);
    read_header(4);
    read_header(5);
    read_header(6);
    read_header(7);
    read_header(8);
    read_header(9);

    MainTask->start(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}




//=============================================================================
int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}
//=============================================================================
void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}
//=============================================================================
void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}
//=============================================================================

int     loop=0;
int     read_ip=0;
int     refresh=0;

int     BARLED_IS_ON = 0;
int     BARLED_COLOR = 0;

char    aBuf[256];
const char *aBuf2;

int BeepIsOn=1;
int init_done=0;
int Timeout=50;

#define WAITING 0
#define WORKING 1

int STATE=WAITING;




//=============================================================================================
void MainWindow::Update_Values()
{
    Total= 2.2;
    QCoreApplication::processEvents();
}
//=========================================================================================


//=========================================================================================
void MainWindow::Led_RED()
{
    STATE=WORKING;

    //-------------------------------------------
    ui->pb_waiting->setVisible      (false);
    ui->pb_ValidCard->setVisible    (false);
    ui->pb_InvalidCard->setVisible  (true);

    QCoreApplication::processEvents();
    QCoreApplication::processEvents();
    //-------------------------------------------

    ui->label_header_06->setVisible       (false);
    ui->label_header_07->setVisible       (false);

    ui->label_header_08->setVisible       (false);
    ui->label_header_09->setVisible       (true);



    //-------------------------------------------
    ret=system("edgeled -c red -w 5 -stay_on");
    //-------------------------------------------

    //-------------------------------------------
    if( BeepIsOn )  ret=system("beep -f 100 -l 300 &");
    //-------------------------------------------
}
//=========================================================================================


//=========================================================================================
void MainWindow::Led_GREEN()
{
    STATE=WORKING;
    //-------------------------------------------
    ui->pb_waiting->setVisible      (false);
    ui->pb_InvalidCard->setVisible  (false);
    ui->pb_ValidCard->setVisible    (true);

    QCoreApplication::processEvents();
    QCoreApplication::processEvents();
    //-------------------------------------------


    ui->label_header_06->setVisible       (false);
    ui->label_header_07->setVisible       (false);

    ui->label_header_08->setVisible       (true);
    ui->label_header_09->setVisible       (false);


    //-------------------------------------------
    ret=system("edgeled -c green -w 5 -stay_on");
    //-------------------------------------------

    //-------------------------------------------
    if( BeepIsOn )  ret=system("beep &");
    //-------------------------------------------

}
void MainWindow::Led_OFF()
{
    STATE=WAITING;

    //-------------------------------------------
    ui->pb_InvalidCard->setVisible  (false);
    ui->pb_ValidCard->setVisible    (false);
    ui->pb_waiting->setVisible      (true);

    QCoreApplication::processEvents();
    QCoreApplication::processEvents();
    //-------------------------------------------


    ui->label_header_06->setVisible       (true);
    ui->label_header_07->setVisible       (true);

    ui->label_header_08->setVisible       (false);
    ui->label_header_09->setVisible       (false);



    //-------------------------------------------
    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    //-------------------------------------------
    QCoreApplication::processEvents();
}
//=========================================================================================










//=========================================================================================
void MainWindow::read_header(int header)
{
    QLabel *l;
    char aBuffer[20];

    l=ui->label_header_01;

    if(header==0)
    {
        read("/home/root/demo/header_00.txt");
        sprintf(Currency_char,"%s",aTmp.toLatin1().data());
        return;
    }

    if(header== 1) { read("/home/root/demo/header_01.txt"); l=ui->label_header_01;}
    if(header== 2) { read("/home/root/demo/header_02.txt"); l=ui->label_header_02;}
    if(header== 3) { read("/home/root/demo/header_03.txt"); l=ui->label_header_03;}
    if(header== 4) { read("/home/root/demo/header_04.txt"); l=ui->label_header_04;}
    if(header== 5) { read("/home/root/demo/header_05.txt"); l=ui->label_header_05;}
    if(header== 6) { read("/home/root/demo/header_06.txt"); l=ui->label_header_06;}
    if(header== 7) { read("/home/root/demo/header_07.txt"); l=ui->label_header_07;}
    if(header== 8) { read("/home/root/demo/header_08.txt"); l=ui->label_header_08;}
    if(header== 9) { read("/home/root/demo/header_09.txt"); l=ui->label_header_09;}

    if( header == 5  )
    {
        fAdult = aTmp.toFloat();
    }

    if( header == 7  )
    {
        fChild = aTmp.toFloat();
    }

    if( header == 7 )
    {
        sprintf(aBuffer,"  %s",aTmp.toLatin1().data());
        aBuffer[0]=Currency_char[0];
        l->setText(aBuffer);
        return;
    }

    if(aTmp.length()>2)
    {

        l->setText(aTmp);
    }
    else
    {
        l->setText("*******");
    }
}
//=========================================================================================























char    aTmp2[256];
int     Card_Present=0;
int     Toggle=0;
int     ClearDone=0;
void MainWindow::MainTask_loop()
{
    //QString res = "reset";
    int sec,min,hour,days;
    int len;
    unsigned char c;
    //char aBuf[100];

    QDateTime current = QDateTime::currentDateTime();
    //QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime   = current.toString("hh:mm:ss");
    QString TheDate   = current.toString("dd/MM/yyyy");

    ui->TheTime->setText(TheTime);
    ui->TheDate->setText(TheDate);

    loop++;

    //=============================================================================
    // CHecks that moti is alive
    //=============================================================================
    if( (loop%40) == 1 )
    {
        system("/usr/bin/start_moti.sh &");
    }
    //=============================================================================

    //=============================================================================
    if(loop%600==590) // Every 59 seconds
    {
        system("/usr/bin/DoClear.sh &");
    }
    //=============================================================================
    if(loop%10==5 )
    {
        if(Toggle)
        {
           Toggle=0;
           ui->pb_waiting->setVisible(false);
        }
        else
        {
            if(STATE==WAITING)
            {
               Toggle=1;
               ui->pb_waiting->setVisible(true);
            }
        }
    }
    //=============================================================================

    //=============================================================================
    if( DelayTimer)
    {
        DelayTimer--;

        Label2.sprintf("%d",DelayTimer);
        ui->Counter_2->setText(Label2);


        if( DelayTimer == 0 )
        {
            NbAdult=1;
            NbChild=0;
            Update_Values();
        }
    }
    //=============================================================================


    //=============================================================================
    if( DelayTimer == 0 && CurrentScreen != MAIN_SCREEN)
    {
        CurrentScreen = MAIN_SCREEN;
        ui->stackedWidget->setCurrentIndex(CurrentScreen);
        NbAdult=1;
        NbChild=0;
        Update_Values();
    }
    //=============================================================================

    //=============================================================================
    if(loop%10==0)
    {
        sec = loop/10;
        min = sec/60;
        hour = min/60;
        days = hour/24;
        Label2.sprintf("(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
        ui->Counter->setText(Label2);
    }
    //=============================================================================


    //=============================================================================
    if( refresh > 0)
    {
        refresh--;
        if(refresh==0)
        {
            ui->free_text->setText("");
        }
    }
    //=============================================================================

    //=============================================================================
    if( loop%30==29)
    {
        ret=system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
        read("/tmp/ip.txt");

        len=aTmp.length();
        if(len > 2)
        {
            aBuf2=aTmp.toLatin1().data();

            if( strstr ( aBuf2,"192.168.1.100") != NULL )
            {
                ret=system("/etc/init.d/networking &");
            }

            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }

    }
    //=============================================================================


    //=============================================================================
    if( (DelayTimer%30) == 15 )
    {
        ret=system("echo \"\" > /tmp/moti.data");
        ret=system("echo \"Clearing moti.data ....\n\r\" ");
    }
    //=============================================================================

    //=============================================================================
    read("/tmp/moti.data");
    if(aTmp.length()>10 )
    {
        refresh=30;
        len=aTmp.length();
        aBuf2=aTmp.toLatin1().data();
        c = aBuf2[len-4];

        ret=system("ifconfig eth0 up");

        ret=system("echo \"\" > /tmp/moti.data");

        DelayTimer = 30;

        if ( c%2==0 ) Led_RED();
        if ( c%2==1 ) Led_GREEN();

        BARLED_IS_ON = 7; // (7 x 100 ms)
        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
    }
    //=============================================================================

    //=============================================================================
    if( BARLED_IS_ON > 0 )
    {
        BARLED_IS_ON--;
        if( BARLED_IS_ON == 0 )
        {
             Led_OFF();
        }
    }
    //=============================================================================
    QCoreApplication::processEvents();
}



//=========================================================================================
void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}
//=========================================================================================








//=========================================================================================
void MainWindow::Clear_Service_buttons()
{
}
void MainWindow::on_pb_moti_clicked()
{
}
void MainWindow::on_pb_barcode_clicked()
{
}
void MainWindow::on_pb_reboot_clicked()
{
}
void MainWindow::on_pb_open_clicked()
{
}
