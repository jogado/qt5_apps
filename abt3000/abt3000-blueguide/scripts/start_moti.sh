#!/bin/sh

#===================================================
# From pv3000_start_moti.sh
#===================================================


pid=`pidof moti`

if [ "$pid" == "" ]
then
	# moti -uptime 30  -oneread &
	moti -repoll 20 &
else
	echo "moti is alive ( pid = $pid)"
fi


#   pid=`pidof barcode`
#
#   if [ "$pid" == "" ]
#   then
#   	barcode -timeout &
#   else
#   	echo "barcode is alive ( pid = $pid)"
#   fi

