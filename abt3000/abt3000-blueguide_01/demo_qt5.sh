#!/bin/sh

#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
kill_it()
{
	pid=`pidof $1`
      	if [ "$pid" != "" ]
		then
			echo "killing $1 (pid:$pid)"
        	kill -9 $pid
       	fi
}
killall_it()
{
	while [ 1 ]
	do
		pid=`pidof $1`
      	if [ "$pid" != "" ]
		then
			echo "killing $1 (pid:$pid)"
		    kill -9  $pid
		else
			break
       	fi
	done
}
rm_it()
{
      	if [ -e $1 ]; then

                rm $1
       	fi
}
#------------------------------------------------------------------------------
clean_up()
{
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT

#------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
echo $$ > $TMP_FOLDER/demo_qt5.pid
#------------------------------------------------------------------------------


echo 0 > /sys/class/graphics/fbcon/cursor_blink
export DISPLAY=:0.0
X -nocursor -s 0 &



if [ $DEVICE_TYPE = "abt3000_3_0" ] || [ $DEVICE_TYPE = "abt3000_3_1" ] || [ $DEVICE_TYPE = "abt3000_9_2" ]
then

	killall_it abt3000-ptcrb
	killall_it combridge
	killall_it combridge_modem.sh

    combridge_modem.sh

	# if [ -L $EMV3000_DEVICE ]
	# then
	# 	moti -uptime 5 &
	# fi

    abt3000-ptcrb
    clean_up
fi
#=======================================================================
clean_up





