#!/bin/sh

#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
kill_it()
{
	pid=`pidof $1`
      	if [ "$pid" != "" ]; then
                kill $pid
       	fi
}
killall_it()
{
	pid=`pidof $1`
      	if [ "$pid" != "" ]; then
                killall $1
       	fi
}
rm_it()
{
      	if [ -e $1 ]; then

                rm $1
       	fi
}
#------------------------------------------------------------------------------
clean_up()
{
	killall moti.sh
	killall moti
	killall barcode
	killall x11vnc
	echo "Cleaning_done"
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT 

#------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
echo $$ > $TMP_FOLDER/demo_qt5.pid
#------------------------------------------------------------------------------


#----------------------------------------------------------------
if [ -d "/usr/lib/fonts" ]
then
	echo "Fonts folder present"
else
	echo "Installing Fonts folder"
	ln -s /usr/share/fonts/ttf/ /usr/lib/fonts
fi
#----------------------------------------------------------------


echo 0 > /sys/class/graphics/fbcon/cursor_blink
export DISPLAY=:0.0
X -nocursor -s 0 &

if [ -f "/usr/bin/x11vnc" ]
then
	echo "VNC server present"
	if [ -d "/etc/vnc" ]
	then
		echo "VNC folder exist"
	else
		echo "VNC init *****"
		mkdir /etc/vnc
		x11vnc -storepasswd emsyscon /etc/vnc/passfile
		sleep 1
	fi
	x11vnc -rfbauth /etc/vnc/passfile &
fi
sleep 1




#=======================================================================
#=======================================================================
#=======================================================================







#=======================================================================
if [ $DEVICE_TYPE = "abt3000_3_1" ] ; then

    killall_it abt3000-ptcrb
    killall_it combridge
    killall_it combridge_modem.sh

    echo 1 > /sys/class/leds/gps-reset/brightness
    echo 1 > /sys/class/leds/wlan-reset/brightness


    combridge_modem.sh

    abt3000-ptcrb

    clean_up
fi
#=======================================================================
clean_up
