#!/bin/sh

pid=`pidof moti`

if [ "$pid" == "" ]
then
	moti -uptime 30  &
else
	echo "moti is alive ( pid = $pid)"
fi
