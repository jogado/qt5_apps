#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();




private slots:
    void MasterTimer_update();
    void MainTask_loop();

    void Led_GREEN();
    void Led_RED();
    void Led_OFF();

    void on_pb_moti_clicked();
    void on_pb_barcode_clicked();
    void on_pb_reboot_clicked();
    void Update_Values();
    void Clear_Service_buttons();

    void on_pb_open_clicked();
    void read_header(int header);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
