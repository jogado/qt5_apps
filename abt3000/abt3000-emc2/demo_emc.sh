#!/bin/sh

#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
{
      	if [ -e $1 ]; then

                rm $1
       	fi
}
#------------------------------------------------------------------------------
clean_up()
{
	killall moti.sh
	killall moti
	killall rtc.sh
	killall temperature.sh
	killall network.sh
	killall emc-test-abt3000
	echo "Cleaning_done"
	exit 0
}
trap clean_up SIGUP SIGTERM SIGINT SIGKILL EXIT 

#------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
echo $$ > $TMP_FOLDER/demo_emc.pid
#------------------------------------------------------------------------------


#----------------------------------------------------------------
if [ -d "/usr/lib/fonts" ]
then
	echo "Fonts folder present"
else
	echo "Installing Fonts folder"
	ln -s /usr/share/fonts/ttf/ /usr/lib/fonts
fi
#----------------------------------------------------------------


echo 0 > /sys/class/graphics/fbcon/cursor_blink
export DISPLAY=:0.0
X -nocursor &

sleep 1


EMV3000_DEVICE="/dev/emv3000"


/usr/bin/moti.sh &
/usr/bin/temperature.sh &
/usr/bin/rtc.sh &
/usr/bin/network.sh "192.168.1.1" "/tmp/network.txt" &


sleep 1

/usr/bin/emc-test-abt3000


clean_up

