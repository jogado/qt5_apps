#!/bin/sh

env_imx6ul()
{
	echo "env : imx6ul"
	export ARCH=arm
	export CROSS_COMPILE=arm-poky-linux-gnueabi-

	. /opt/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi 

}

env_imx6s()
{
	echo "env : imx6s"
	export ARCH=arm
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	. /opt/poky-emsyscon/2.4.1/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
}


if echo $1 | grep -q "imx6s"; then
	env_imx6s
elif  echo $1 | grep -q "imx6ul"; then
	env_imx6ul
else
	echo "syntax doit [ imx6s | imx6ul ] < 192.168.0.xxx >"
	exit
fi


make clean
qmake
make


ARG1=$1
FILE="abt3000-emc2"
if echo $2 | grep -q "192.168"; then

	scp $FILE root@$2:/usr/bin

	scp scripts/rtc.sh         	root@$2:/usr/bin
	scp scripts/temperature.sh  	root@$2:/usr/bin
	scp scripts/network.sh      	root@$2:/usr/bin
	scp scripts/demo_emc.sh  	root@$2:/usr/bin
	scp scripts/moti.sh 		root@$2:/usr/bin
	scp scripts/demo-init.sh	root@$2:/etc/init.d
fi

