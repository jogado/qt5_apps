#!/bin/sh

#---------------------------------------------------------------------------------
TEST_DESC="Test temperature loop"
#---------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------





GATEWAY="$1"
out="$2"

CMD="ping $GATEWAY -c 1 -W 1"

while [ 1 ]
do
	STATUS=`$CMD`

	RES=`echo $STATUS|grep -i "0% packet loss"`

	if [ -z "${RES}" ]
	then
		  echo "$GATEWAY BAD" 
		  echo "$GATEWAY BAD" > $out

                  echo "---------------------------------" >> /tmp/network.log
                  echo "$STATUS" >> /tmp/network.log
                  echo "$GATEWAY BAD" >> /tmp/network.log


	else
		  echo "$GATEWAY OK" 
		  echo "$GATEWAY OK" > $out
	fi


	sleep 1
done

