#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>


#include <QTimer>

QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;

char aBarco[] = "Barcode input";


#define MAIN_SCREEN       0
#define CARD_SCREEN       1
#define SPLASH_SCREEN     2
#define BARCODE_SCREEN    3
#define ABOUT_SCREEN      4
#define SATINFO_2         5
#define SATINFO_3         6

int CurrentScreen = BARCODE_SCREEN;
int DelayTimer = 20;
QString Label2;



QColor barColorRed      = QColor(255,  0,  0);
QColor barColorGreen    = QColor(  0,255,  0);
QColor barColorBlue     = QColor(  0,  0,255);

QString styleRed   = QString("QProgressBar::chunk  { background-color: %1;width: 20px; }").arg(barColorRed.name());
QString styleGreen = QString("QProgressBar::chunk  { background-color: %1;width: 20px; }").arg(barColorGreen.name());
QString styleBlue  = QString("QProgressBar::chunk  { background-color: %1;width: 20px; }").arg(barColorBlue.name());


char UsedFixSat[20];
GPS g_gps;


//===========================================================================
int IsFixSat(int SatId)
{
    int i;

    for ( i=0 ; i<15 ; i++)
    {
        if( UsedFixSat[i] ==  SatId)
        {
            return(0);
        }
    }
    return(1);
}
//===========================================================================

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    memset(&g_gps,0x00,sizeof(GPS));

    ui->setupUi(this);


    ui->Counter->setVisible(true);


    /*
    ui->BarcodeScreen->setStyleSheet ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
    ui->CardScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
    ui->MainScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/mainscreen.jpg\"); background-position: center;" );
    ui->AboutScreen->setStyleSheet   ( "background-image:url(\"/etc/demo/jpg/aboutscreen.jpg\"); background-position: center;" );
    */
    //ui->SplashScreen->setStyleSheet  ( "background-image:url(\"./BG3_320x240.jpg\"); background-position: center;" );
    //ui->MainScreen->setStyleSheet    ( "background-image:url(\"./BG2_320x240.jpg\"); background-position: center;" );

    ui->Counter->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->Head->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);


    ui->stackedWidget->setCurrentIndex(SATINFO_3);
    CurrentScreen = SATINFO_3;
    DelayTimer = 20;

    // system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    //system("echo \"-----\" > /tmp/text.txt");
    //system("echo \"\" > /tmp/moti.data");
    //system("echo \"\" > /tmp/rakinda.data");

    ui->vLine_1->setText (" - ");
    ui->vLine_2->setText (" - ");
    ui->vLine_3->setText (" - ");
    ui->vLine_4->setText (" - ");
    ui->vLine_5->setText (" - ");
    ui->vLine_6->setText (" - ");
    ui->vLine_7->setText (" - ");
    ui->vLine_8->setText (" - ");
    ui->vLine_9->setText (" - ");


    ui->GPGVS_LINE_1->setText("");
    ui->GPGVS_LINE_2->setText("");
    ui->GPGVS_LINE_3->setText("");

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

    }

MainWindow::~MainWindow()
{
    delete ui;
}



int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}






int loop=0;
int read_ip=0;
int refresh=0;
int Fix = 0;




void MainWindow::MainTask_loop()
{

    QString res = "reset";
    int sec,min,hour,days;
    QStringList fields;
    int i;
    int SatId=0;


    //=============================================================================
    loop++;
    sec = loop/10;
    min = sec/60;
    hour = min/60;
    days = hour/24;
    //=============================================================================
    if( DelayTimer) DelayTimer--;

    if( (DelayTimer == 0) && (CurrentScreen != BARCODE_SCREEN))
    {
     //  ui->stackedWidget->setCurrentIndex(BARCODE_SCREEN);
     //  CurrentScreen = BARCODE_SCREEN ;
    }
    //=============================================================================
    if( (loop%5)==0)
    {
        Label2.sprintf("(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
        ui->Line_10->setText(Label2);
    }
    //=============================================================================













    //=============================================================================
    read("/tmp/gps.GNGSA.used_sat");
    if(aTmp.length()>=1)
    {
        fields = aTmp.split(',');
        if( fields.at(0).toInt() <= 64 && fields.at(0).toInt() > 0 )
        {
            for (i = 0; i < 12 ; i ++)
            {
                UsedFixSat[i]= fields.at(i).toInt() ;
            }

            ui->SAT_FIX_01->setText(fields.at(0));
            ui->SAT_FIX_02->setText(fields.at(1));
            ui->SAT_FIX_03->setText(fields.at(2));
            ui->SAT_FIX_04->setText(fields.at(3));
            ui->SAT_FIX_05->setText(fields.at(4));
            ui->SAT_FIX_06->setText(fields.at(5));
            ui->SAT_FIX_07->setText(fields.at(6));
            ui->SAT_FIX_08->setText(fields.at(7));
        }
     }
	//=============================================================================
    read("/tmp/gps.GNGGA.nb_sat");
    if(aTmp.length()>1)
	{
        	ui->vLine_1->setText(" " + aTmp);
            ui->GNGGA_SatInView->setText(" " + aTmp);
            ui->GPGGA_SatInView->setText(" " + aTmp);
	}
    //=============================================================================





    //=============================================================================
    read("/tmp/gps.GNRMC.lat");
    if(aTmp.length()>1)
	{
        	ui->vLine_2->setText(" " + aTmp);
	}

    read("/tmp/gps.GNRMC.lon");
    if(aTmp.length()>1)
	{
        	ui->vLine_3->setText(" " + aTmp);
	}

    read("/tmp/gps.GNRMC.date");
    if(aTmp.length()>1)
	{
        	ui->vLine_4->setText(" " + aTmp);
	}


    read("/tmp/gps.GNRMC.time");
	if(aTmp.length()>1)
	{
	        ui->vLine_5->setText(" " + aTmp);
	}


    read("/tmp/gps.GNVTG.heading");
	if(aTmp.length()>1)
	{
        ui->vLine_6->setText(" " + aTmp);
	}
    else
    {
        ui->vLine_6->setText(" - ");
    }


    read("/tmp/gps.GNRMC.valid");
    if(aTmp.length()>=1)
    {
        ui->vLine_9->setText(" " + aTmp);

        if( (Fix == 0 )  && (aTmp == "A") )
        {
            Fix = 1;
            Label2.sprintf("%02d:%02d ",min%60,sec%60);
            ui->Line_11->setText(Label2);
            system("beep");
        }

        if( (Fix == 1 )  && (aTmp == "V") )
        {
            Fix = 0;
            Label2.sprintf(" - " );
            ui->Line_11->setText(Label2);
            //loop=0;
        }
    }





    read("/tmp/gps.GNRMC");
    if(aTmp.length()>5)
	{
        ui->vLine_7->setText(" " + aTmp);
    }

    read("/tmp/gps.GPGSV");
    if(aTmp.length()>5)
	{
        ui->vLine_8->setText(" " + aTmp);
	}

    /*
    read("/tmp/system.elapsed_time");
    if(aTmp.length()>1)
    {
        ui->Line_10->setText("Elapsed from startup : " + aTmp);
    }
    */





    //============================================================================================================
    //============================================================================================================
    //============================================================================================================
    //============================================================================================================
    //============================================================================================================
    //============================================================================================================
    //============================================================================================================
    //============================================================================================================
    //============================================================================================================


    if( CurrentScreen == SATINFO_2)
    {
        read("/tmp/gps.GLGSV");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            if(fields.at(2).toInt() == 1 )
                ui->GLGVS_LINE_1->setText(aTmp);
            if(fields.at(2).toInt() == 2 )
                ui->GLGVS_LINE_2->setText(aTmp);
            if(fields.at(2).toInt() == 3 )
                ui->GLGVS_LINE_3->setText(aTmp);
            if(fields.at(2).toInt() == 4 )
                ui->GLGVS_LINE_4->setText(aTmp);
        }
        else
        {
            ui->GLGVS_LINE_1->setText("");
            ui->GLGVS_LINE_2->setText("");
            ui->GLGVS_LINE_3->setText("");
            ui->GLGVS_LINE_4->setText("");
            return;
        }




        read("/tmp/gps.GLGSV.sat_01");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_1->setText(fields.at(0));
            ui->progressBar_1->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_1->setText("");
            ui->progressBar_1->setValue(0);
        }

        read("/tmp/gps.GLGSV.sat_02");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_2->setText(fields.at(0));
            ui->progressBar_2->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_2->setText("");
            ui->progressBar_2->setValue(0);
        }

        read("/tmp/gps.GLGSV.sat_03");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_3->setText(fields.at(0));
            ui->progressBar_3->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_3->setText("");
            ui->progressBar_3->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_04");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_4->setText(fields.at(0));
            ui->progressBar_4->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_4->setText("");
            ui->progressBar_4->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_05");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_5->setText(fields.at(0));
            ui->progressBar_5->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_5->setText("");
            ui->progressBar_5->setValue(0);
        }

        read("/tmp/gps.GLGSV.sat_06");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_6->setText(fields.at(0));
            ui->progressBar_6->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_6->setText("");
            ui->progressBar_6->setValue(0);
        }






        read("/tmp/gps.GLGSV.sat_07");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_7->setText(fields.at(0));
            ui->progressBar_7->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_7->setText("");
            ui->progressBar_7->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_08");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_8->setText(fields.at(0));
            ui->progressBar_8->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_8->setText("");
            ui->progressBar_8->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_09");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_9->setText(fields.at(0));
            ui->progressBar_9->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_9->setText("");
            ui->progressBar_9->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_10");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_10->setText(fields.at(0));
            ui->progressBar_10->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_10->setText("");
            ui->progressBar_10->setValue(0);
        }



        read("/tmp/gps.GLGSV.sat_11");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_11->setText(fields.at(0));
            ui->progressBar_11->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_11->setText("");
            ui->progressBar_11->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_12");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_12->setText(fields.at(0));
            ui->progressBar_12->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_12->setText("");
            ui->progressBar_12->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_13");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_13->setText(fields.at(0));
            ui->progressBar_13->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_13->setText("");
            ui->progressBar_13->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_14");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_14->setText(fields.at(0));
            ui->progressBar_14->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_14->setText("");
            ui->progressBar_14->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_15");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_15->setText(fields.at(0));
            ui->progressBar_15->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_15->setText("");
            ui->progressBar_15->setValue(0);
        }


        read("/tmp/gps.GLGSV.sat_16");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->Sat_ID_16->setText(fields.at(0));
            ui->progressBar_16->setValue(fields.at(3).toInt() );
        }
        else
        {
            ui->Sat_ID_16->setText("");
            ui->progressBar_16->setValue(0);
        }


        Label2.sprintf("%03d",(300-loop%300) ) ;
        ui->INFO_2_counter->setText(Label2);

        if(loop%300 == 259)
        {
            system("rm /tmp/gps.*");
            ui->GLGVS_LINE_1->setText("");
            ui->GLGVS_LINE_2->setText("");
            ui->GLGVS_LINE_3->setText("");
            ui->GLGVS_LINE_4->setText("");
        }


    }
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================
    //==============================================================================================================

    if( CurrentScreen == SATINFO_3)
    {
        read("/tmp/gps.GPGSV");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            if(fields.at(2).toInt() == 1 )
            {
                ui->GPGVS_LINE_1->setText(aTmp);
            }
            if(fields.at(2).toInt() == 2 )
            {
                ui->GPGVS_LINE_2->setText(aTmp);
            }

            if(fields.at(2).toInt() == 3 )
            {
                ui->GPGVS_LINE_3->setText(aTmp);
            }
            if(fields.at(2).toInt() == 4 )
            {
                ui->GPGVS_LINE_4->setText(aTmp);
            }
        }
        else
        {
          // ui->GPGVS_LINE_1->setText("");
          // ui->GPGVS_LINE_2->setText("");
          // ui->GPGVS_LINE_3->setText("");
          // QCoreApplication::processEvents();
          //return;
        }

        read("/tmp/gps.GPGSV.sat_01");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_01->setText(fields.at(0));
            ui->progressBar_GPGVS_01->setValue(fields.at(3).toInt() );

            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_01->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_01->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_01->setText("");
            ui->progressBar_GPGVS_01->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_02");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_02->setText(fields.at(0));
            ui->progressBar_GPGVS_02->setValue(fields.at(3).toInt() );
            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_02->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_02->setStyleSheet(styleGreen);
            //-----------------------------------------

        }
        else
        {
            ui->GPGVS_ID_02->setText("");
            ui->progressBar_GPGVS_02->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_03");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_03->setText(fields.at(0));
            ui->progressBar_GPGVS_03->setValue(fields.at(3).toInt() );
            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_03->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_03->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_03->setText("");
            ui->progressBar_GPGVS_03->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_04");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_04->setText(fields.at(0));
            ui->progressBar_GPGVS_04->setValue(fields.at(3).toInt() );
            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_04->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_04->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_04->setText("");
            ui->progressBar_GPGVS_04->setValue(0);
        }



        read("/tmp/gps.GPGSV.sat_05");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_05->setText(fields.at(0));
            ui->progressBar_GPGVS_05->setValue(fields.at(3).toInt() );
            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_05->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_05->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_05->setText("");
            ui->progressBar_GPGVS_05->setValue(0);
        }


        read("/tmp/gps.GPGSV.sat_06");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_06->setText(fields.at(0));
            ui->progressBar_GPGVS_06->setValue(fields.at(3).toInt() );
            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_06->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_06->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_06->setText("");
            ui->progressBar_GPGVS_06->setValue(0);
        }



        read("/tmp/gps.GPGSV.sat_07");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_07->setText(fields.at(0));
            ui->progressBar_GPGVS_07->setValue(fields.at(3).toInt() );
            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_07->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_07->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_07->setText("");
            ui->progressBar_GPGVS_07->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_08");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_08->setText(fields.at(0));
            ui->progressBar_GPGVS_08->setValue(fields.at(3).toInt() );

            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_08->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_08->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_08->setText("");
            ui->progressBar_GPGVS_08->setValue(0);
        }





        read("/tmp/gps.GPGSV.sat_09");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_09->setText(fields.at(0));
            ui->progressBar_GPGVS_09->setValue(fields.at(3).toInt() );

            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_09->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_09->setStyleSheet(styleGreen);

        }
        else
        {
            ui->GPGVS_ID_09->setText("");
            ui->progressBar_GPGVS_09->setValue(0);
        }





        read("/tmp/gps.GPGSV.sat_10");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_10->setText(fields.at(0));
            ui->progressBar_GPGVS_10->setValue(fields.at(3).toInt() );

            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_10->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_10->setStyleSheet(styleGreen);

        }
        else
        {
            ui->GPGVS_ID_10->setText("");
            ui->progressBar_GPGVS_10->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_11");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_11->setText(fields.at(0));
            ui->progressBar_GPGVS_11->setValue(fields.at(3).toInt() );


            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_11->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_11->setStyleSheet(styleGreen);

        }
        else
        {
            ui->GPGVS_ID_11->setText("");
            ui->progressBar_GPGVS_11->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_12");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_12->setText(fields.at(0));
            ui->progressBar_GPGVS_12->setValue(fields.at(3).toInt() );
            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_12->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_12->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_12->setText("");
            ui->progressBar_GPGVS_12->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_13");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_13->setText(fields.at(0));
            ui->progressBar_GPGVS_13->setValue(fields.at(3).toInt() );

            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_13->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_13->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_13->setText("");
            ui->progressBar_GPGVS_13->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_14");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_14->setText(fields.at(0));
            ui->progressBar_GPGVS_14->setValue(fields.at(3).toInt() );

            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_14->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_14->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_14->setText("");
            ui->progressBar_GPGVS_14->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_15");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_15->setText(fields.at(0));
            ui->progressBar_GPGVS_15->setValue(fields.at(3).toInt() );

            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_15->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_15->setStyleSheet(styleGreen);
            //-----------------------------------------
        }
        else
        {
            ui->GPGVS_ID_15->setText("");
            ui->progressBar_GPGVS_15->setValue(0);
        }

        read("/tmp/gps.GPGSV.sat_16");
        if(aTmp.length()>=1)
        {
            fields = aTmp.split(',');
            ui->GPGVS_ID_16->setText(fields.at(0));
            ui->progressBar_GPGVS_16->setValue(fields.at(3).toInt() );

            //-----------------------------------------
            SatId = fields.at(0).toInt();
            if( IsFixSat(SatId) )
                  ui->progressBar_GPGVS_16->setStyleSheet(styleBlue);
            else
                  ui->progressBar_GPGVS_16->setStyleSheet(styleGreen);
            //-----------------------------------------

        }
        else
        {
            ui->GPGVS_ID_16->setText("");
            ui->progressBar_GPGVS_16->setValue(0);
        }

        Label2.sprintf("%03d",(3000-loop%300) ) ;
        ui->INFO_3_counter->setText(Label2);


        if(loop%3000 == 2999)
        {
            system("rm /tmp/gps.*");
            ui->GPGVS_LINE_1->setText("");
            ui->GPGVS_LINE_2->setText("");
            ui->GPGVS_LINE_3->setText("");
            ui->GPGVS_LINE_4->setText("");
        }

        QCoreApplication::processEvents();
    }

    //==============================================================================================================




    QCoreApplication::processEvents();























    /*




    read_barcode("/tmp/rakinda.data");

    if(aTmp_barco.length()<3)
    {
       // loop=0;
       // ui->free_text->setText(aTmp_barco);
    }

    if(aTmp_barco.length()>2)
    {
        refresh=30;

        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
        system("beep &");
        system("echo \"\" > /tmp/rakinda.data");
        system("OpenDoor.sh &");
    }
    //=============================================================================
    if( read_ip == 0)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
     }

    if( loop%10==3)
    {
        system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}'> /tmp/ip.txt");
    }

    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
     }
    //=============================================================================
    read("/tmp/moti.data");
    if(aTmp.length()>5)
    {
        refresh=30;
        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
        system("beep &");
        system("echo \"\" > /tmp/moti.data");
        system("OpenDoor.sh &");
        QCoreApplication::processEvents();
    }
    //=============================================================================

    */
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}


/*
void MainWindow::on_pb_to_satinfo_4_clicked()
{
    ui->stackedWidget->setCurrentIndex(BARCODE_SCREEN);
    CurrentScreen = BARCODE_SCREEN ;
}
*/









void MainWindow::on_pb_to_satinfo_2_clicked()
{
    ui->stackedWidget->setCurrentIndex(SATINFO_2);
    CurrentScreen = SATINFO_2 ;
}

void MainWindow::on_pb_to_satinfo_3_clicked()
{
    ui->stackedWidget->setCurrentIndex(SATINFO_3);
    CurrentScreen = SATINFO_3 ;

}
void MainWindow::on_pb_to_satinfo_1_clicked()
{
    ui->stackedWidget->setCurrentIndex(BARCODE_SCREEN);
    CurrentScreen = BARCODE_SCREEN ;
}
