#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>



typedef struct
{
    int Sat_in_view;
}GPS;




namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void MasterTimer_update();
    void MainTask_loop();

    void on_pb_to_satinfo_2_clicked();

    void on_pb_to_satinfo_1_clicked();

 //   void on_pb_to_satinfo_4_clicked();

    void on_pb_to_satinfo_3_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
