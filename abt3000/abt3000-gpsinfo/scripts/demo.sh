#!/bin/sh


clean_up()
{
	killall Xorg
	killall moti
    killall rakinda
	killall emc-test-mv3000

	rm moti.tmp
	rm rakinda.tmp

	echo "Cleaning_done"
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT 



if [ -d "/usr/lib/fonts" ]
then
	echo "Fonts folder present"
else
	echo "Installing Fonts folder"
	ln -s /usr/share/fonts/ttf/ /usr/lib/fonts
fi


rm moti.tmp
rm rakinda.tmp

moti &
rakinda &


sleep 1

export DISPLAY=:0.0
X &

sleep 1

./emc-test-abt3000


clean_up


