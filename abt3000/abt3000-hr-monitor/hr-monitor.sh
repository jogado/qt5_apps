#!/bin/sh

#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
kill_it()
{
	pid=`pidof $1`
      	if [ "$pid" != "" ]
		then
			echo "killing $1 (pid:$pid)"
        	kill -9 $pid
       	fi
}
killall_it()
{
	while [ 1 ]
	do
		pid=`pidof $1`
      	if [ "$pid" != "" ]
		then
			echo "killing $1 (pid:$pid)"
		    kill -9  $pid
		else
			break
       	fi
	done
}
rm_it()
{
      	if [ -e $1 ]; then

                rm $1
       	fi
}
#------------------------------------------------------------------------------
clean_up()
{
	kill_it abt3000-hr-monitor
	kill_it X
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT

#------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
echo $$ > $TMP_FOLDER/demo_qt5.pid
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# prepare folder for first time
#------------------------------------------------------------------------------
HR_FOLDER="/etc/HR"
HR_ERROR_CNT_COUNTER="hr_error_cnt.log"
HR_GOOD_CNT_COUNTER="hr_good_cnt.log"
HR_TEST_NAME="hr_current_test.log"
if [ ! -d $HR_FOLDER ]
then
	echo "folder do not exist"
	echo "creating folder : $HR_FOLDER"
	mkdir -p $HR_FOLDER
fi

if [ ! -f $HR_FOLDER/$HR_ERROR_CNT_COUNTER ]
then
	echo "creating file   : $HR_FOLDER/$HR_ERROR_CNT_COUNTER"
	echo "0" > $HR_FOLDER/$HR_ERROR_CNT_COUNTER
fi

if [ ! -f $HR_FOLDER/$HR_GOOD_CNT_COUNTER ]
then
	echo "creating file   : $HR_FOLDER/$HR_GOOD_CNT_COUNTER"
	echo "0" > $HR_FOLDER/$HR_GOOD_CNT_COUNTER
fi


if [ ! -f $HR_FOLDER/$HR_TEST_NAME ]
then
	echo "creating file   : $HR_FOLDER/$HR_TEST_NAME"
	echo "-----------" > $HR_FOLDER/$HR_TEST_NAME
fi



#------------------------------------------------------------------------------




echo 0 > /sys/class/graphics/fbcon/cursor_blink
export DISPLAY=:0.0
X -nocursor -s 0 &

abt3000-hr-monitor


clean_up





