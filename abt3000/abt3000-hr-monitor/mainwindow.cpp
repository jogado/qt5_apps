#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>

QString aTmp;
QString aTmp_barco;

/*
QString aTitle;
QString MyTime;
*/


int NbAdult=0;
int NbChild=0;
float Total=0;


#define MAIN_SCREEN       0
#define SPLASH_SCREEN     1

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label2;
QString Label3;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);

    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;

    system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    system("echo \"-----\" > /tmp/text.txt");

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

}

MainWindow::~MainWindow()
{
    delete ui;
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}


int     loop=0;
int     read_ip=0;
int     refresh=0;
char    aBuf[256];
const char *aBuf2;


//=========================================================================================
void MainWindow::barled_OK()
{
    //ui->pb_ValidCard->setVisible(true);
}
//=========================================================================================
void MainWindow::barled_BAD()
{
    //ui->pb_InvalidCard->setVisible(false);
}
//=========================================================================================
void MainWindow::barled_OFF()
{
    //ui->pb_InvalidCard->setVisible(false);
    //ui->pb_ValidCard->setVisible(false);
}
//=========================================================================================




void MainWindow::MainTask_loop()
{
    QString res = "reset";
    //int len;
    char Data[100];
    int test_counter=0;
    int test_error_counter=0;
    int loop_counter=0;
    int loop_error_counter=0;
    int error_counter=0;


    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime   = current.toString("hh:mm:ss");
    QString TheDate   = current.toString("dd/MM/yyyy");


    ui->TheTime->setText(TheTime);
    ui->TheDate->setText(TheDate);

    loop++;
    //=============================================================================


    //=============================================================================
    // DATE TIME
    //=============================================================================
   /*
    sec = loop/10;
    min = sec/60;
    hour = min/60;
    days = hour/24;
    Label2.sprintf("(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
    ui->Counter->setText(Label2);
    */
    //=============================================================================


    //=============================================================================
    if( read_ip == 0)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
     }
    //=============================================================================

    if( loop%10==3)
    {
            system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    }

    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
    }

    //=============================================================================
    read("/etc/HR/hr_test_description.log");
    if(aTmp.length()>=1)
    {
        aBuf2=aTmp.toLatin1().data();
        ui->TestName->setText(aBuf2);
    }
    //=============================================================================




    //=============================================================================
    read("/etc/HR/hr_test_error_counter.log");
    if(aTmp.length()>=1)
    {
        aBuf2=aTmp.toLatin1().data();
        test_error_counter=atoi(aBuf2);
    }

    read("/etc/HR/hr_test_counter.log");
    if(aTmp.length()>=1)
    {
        aBuf2=aTmp.toLatin1().data();
        test_counter=atoi(aBuf2);
    }
    sprintf(Data,"%d/%d",test_error_counter,test_counter);

    if(test_error_counter != 0)
    {
        ui->pushButton->setStyleSheet("background-color:red;color:white");
    }
    else
    {
        ui->pushButton->setStyleSheet("background-color:green;color:white");
    }

    DelayTimer = 1000;

    ui->test_label->setText(Data);
    //=============================================================================




    //=============================================================================
    read("/etc/HR/hr_loop_counter.log");
    if(aTmp.length()>=1)
    {
        aBuf2=aTmp.toLatin1().data();
        loop_counter=atoi(aBuf2);
    }

    read("/etc/HR/hr_loop_error_counter.log");
    if(aTmp.length()>=1)
    {
        aBuf2=aTmp.toLatin1().data();
        loop_error_counter=atoi(aBuf2);
    }


    sprintf(Data,"Loops: %d/%d",loop_error_counter,loop_counter);
    ui->loop_counter->setText(Data);

    //=============================================================================

    //=============================================================================
    read("/etc/HR/hr_error_counter.log");
    if(aTmp.length()>=1)
    {
        aBuf2=aTmp.toLatin1().data();
        error_counter=atoi(aBuf2);
        sprintf(Data,"Error: %d",error_counter);
        ui->error_counter->setText(Data);
    }
    //=============================================================================

    QCoreApplication::processEvents();

}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}





