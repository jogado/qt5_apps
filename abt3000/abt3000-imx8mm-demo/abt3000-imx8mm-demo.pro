#-------------------------------------------------
#
# Project created by QtCreator 2018-07-19T11:34:41
#
#-------------------------------------------------

QT       += core gui

target.path = /home/root
INSTALL += target


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets



TARGET = abt3000-imx8mm-demo
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    myresource.qrc
