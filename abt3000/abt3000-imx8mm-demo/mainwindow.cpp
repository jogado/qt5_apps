#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>


#include <QTimer>

QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;

char aBarco[] = "Barcode input";




#define MAIN_SCREEN       0
#define SPLASH_SCREEN     1
#define INFO_SCREEN       2

int CurrentScreen = SPLASH_SCREEN;
int DelayTimer = 20;

QString Label2;
QString Version;


//=============================================================================================================================
int ReadDataOneLine( char *ifile , char *data , int MaxLen)
{
    FILE *fp;

    fp = fopen(ifile,"r");

    if(fp == NULL)
    {
            perror("Error opening file");
            return(-1);
    }

    if( fgets (data, MaxLen, fp)!=NULL )
    {
        fclose(fp);
        return(strlen(data));
    }

    fclose(fp);
    return(0);
}
//=============================================================================================================================
char completeVersion[50];

char *Build_dateTime(void)
{
    char aTmp[100];
    int Month = 0;
    int Year  = 0;
    int Day   = 0;

    sprintf(aTmp,"%s",__DATE__);

    if(memcmp(aTmp,"Jan",3)==0) Month=1;
    if(memcmp(aTmp,"F"  ,1)==0) Month=2;
    if(memcmp(aTmp,"Mar",3)==0) Month=3;
    if(memcmp(aTmp,"Ap" ,2)==0) Month=4;
    if(memcmp(aTmp,"May",3)==0) Month=5;
    if(memcmp(aTmp,"Jun",3)==0) Month=6;
    if(memcmp(aTmp,"Jul",3)==0) Month=7;
    if(memcmp(aTmp,"Au" ,2)==0) Month=8;
    if(memcmp(aTmp,"S"  ,1)==0) Month=9;
    if(memcmp(aTmp,"O"  ,1)==0) Month=10;
    if(memcmp(aTmp,"N"  ,1)==0) Month=11;
    if(memcmp(aTmp,"D"  ,1)==0) Month=13;


    aTmp[6]=0;
    Day  = atoi(&aTmp[4]);
    Year = atoi(&aTmp[7]);

    sprintf(completeVersion,"%02d/%02d/%04d %s"
        ,Day
        ,Month
        ,Year
        ,__TIME__);

    return(completeVersion);
}
//=============================================================================================================================



QString AppVersion;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);


    ui->Counter->setVisible(true);


    /*
    ui->BarcodeScreen->setStyleSheet ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
    ui->CardScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
    ui->MainScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/mainscreen.jpg\"); background-position: center;" );
    ui->AboutScreen->setStyleSheet   ( "background-image:url(\"/etc/demo/jpg/aboutscreen.jpg\"); background-position: center;" );
    */
    //ui->SplashScreen->setStyleSheet  ( "background-image:url(\"./BG3_320x240.jpg\"); background-position: center;" );
    //ui->MainScreen->setStyleSheet    ( "background-image:url(\"./BG2_320x240.jpg\"); background-position: center;" );

    ui->Counter->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->Head->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);


    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;
    DelayTimer = 20;


    setAttribute(Qt::WA_AcceptTouchEvents, true);


    system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    system("echo \"-----\" > /tmp/text.txt");
    system("echo \"\" > /tmp/moti.data");
    system("echo \"\" > /tmp/rakinda.data");


    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}


int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}





int loop=0;
int read_ip=0;
int refresh=0;


void MainWindow::MainTask_loop()
{

    QString res = "reset";
    int sec,min,hour,days;
    char aBuff[200];

    loop++;



    /*
    Label2.sprintf("%09d",loop/10);
    */
    sec = loop/10;
    min = sec/60;
    hour = min/60;
    days = hour/24;

    Label2.sprintf("(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
    ui->Counter->setText(Label2);


    system("uname -a |cut -d' ' -f3,6-8,11 > /tmp/kernel.txt");
    system("cat /proc/device-tree/model |cut -d' ' -f2  > /tmp/dtb.txt");
    system("fw_printenv  |grep 'partition=' | cut -d'=' -f2 > /tmp/partition.txt");


    AppVersion.sprintf("application : %s",Build_dateTime() );
    ui->info_application->setText(AppVersion);

    /*
    if( ReadDataOneLine((char *)"/proc/device-tree/model",aBuff,200) >0 )
    {
        AppVersion.sprintf("dtb : %s",aBuff );
        ui->info_dtb->setText(AppVersion);
    }
    */
    if( ReadDataOneLine((char *)"/tmp/dtb.txt",aBuff,200) >0 )
    {
        AppVersion.sprintf("DTB : %s",aBuff );
         ui->info_dtb->setText(AppVersion);
    }


    if( ReadDataOneLine((char *)"/tmp/ip.txt",aBuff,200) >0 )
    {
        AppVersion.sprintf("Ip : %s",aBuff );
        ui->info_ip->setText(AppVersion);
    }


    if( ReadDataOneLine((char *)"/tmp/kernel.txt",aBuff,200) >0 )
    {
        AppVersion.sprintf("Kernel : %s",aBuff );
        ui->info_kernel->setText(AppVersion);
    }

    if( ReadDataOneLine((char *)"/tmp/partition.txt",aBuff,200) >0 )
    {
        AppVersion.sprintf("Partition : %s",aBuff );
        ui->info_partition->setText(AppVersion);
    }



    if( refresh )
    {
        refresh--;
        if(refresh==0)
        {
            ui->free_text->setText("");
        }
    }



    if( DelayTimer) DelayTimer--;


    if( (DelayTimer == 0) && (CurrentScreen !=MAIN_SCREEN))
    {
       ui->stackedWidget->setCurrentIndex(0);
       CurrentScreen = MAIN_SCREEN ;
    }



    //=============================================================================

    read_barcode("/tmp/rakinda.data");

    if(aTmp_barco.length()<3)
    {
       // loop=0;
       // ui->free_text->setText(aTmp_barco);
    }

    if(aTmp_barco.length()>2)
    {
        refresh=30;

        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
        system("beep &");
        system("echo \"\" > /tmp/rakinda.data");
        system("OpenDoor.sh &");
    }
    //=============================================================================
    if( read_ip == 0)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
     }


    if(loop%100==10)
    {
        ui->stackedWidget->setCurrentIndex(INFO_SCREEN);
        CurrentScreen = INFO_SCREEN;
        DelayTimer = 50;
    }


    if( loop%10==3)
    {
        system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}'> /tmp/ip.txt");
    }

    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
     }
    //=============================================================================
    read("/tmp/moti.data");
    if(aTmp.length()>5)
    {
        refresh=30;
        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
        system("beep &");
        system("echo \"\" > /tmp/moti.data");
        system("OpenDoor.sh &");
        QCoreApplication::processEvents();
    }
    //=============================================================================
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}

