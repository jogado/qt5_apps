#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>


#include <QTimer>


#include <QDirModel>
#include <QDebug>
#include <QMessageBox>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>
#include <QPainter>
#include <QFile>





QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;

char aBarco[] = "Barcode input";




#define MAIN_SCREEN       0
#define CARD_SCREEN       1
#define SPLASH_SCREEN     2
#define BARCODE_SCREEN    3
#define ABOUT_SCREEN      4

int CurrentScreen = SPLASH_SCREEN;
int DelayTimer = 20;
QString Label2;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);


    ui->Counter->setVisible(true);


    /*
    ui->BarcodeScreen->setStyleSheet ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
    ui->CardScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
    ui->MainScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/mainscreen.jpg\"); background-position: center;" );
    ui->AboutScreen->setStyleSheet   ( "background-image:url(\"/etc/demo/jpg/aboutscreen.jpg\"); background-position: center;" );
    */
    //ui->SplashScreen->setStyleSheet  ( "background-image:url(\"./BG3_320x240.jpg\"); background-position: center;" );
    //ui->MainScreen->setStyleSheet    ( "background-image:url(\"./BG2_320x240.jpg\"); background-position: center;" );

    ui->Counter->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->Head->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);






    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;



    DelayTimer = 20;

    system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    system("echo \"-----\" > /tmp/text.txt");
    system("echo \"\" > /tmp/moti.data");
    system("echo \"\" > /tmp/rakinda.data");


    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

    }

MainWindow::~MainWindow()
{
    delete ui;
}



int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}






int loop=0;
int read_ip=0;
int refresh=0;
void MainWindow::MainTask_loop()
{

    QString res = "reset";
    int sec,min,hour,days;

    loop++;


    /*
    Label2.sprintf("%09d",loop/10);
    */
    sec = loop/10;
    min = sec/60;
    hour = min/60;
    days = hour/24;

    Label2.sprintf("(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);

    ui->Counter->setText(Label2);


    if( refresh )
    {
        refresh--;
        if(refresh==0)
        {
            ui->free_text->setText("");
        }
    }



    if( DelayTimer) DelayTimer--;

    if( (DelayTimer == 0) && (CurrentScreen != BARCODE_SCREEN))
    {
       ui->stackedWidget->setCurrentIndex(0);
       CurrentScreen = BARCODE_SCREEN ;
    }



    //=============================================================================

    read_barcode("/tmp/rakinda.data");

    if(aTmp_barco.length()<3)
    {
       // loop=0;
       // ui->free_text->setText(aTmp_barco);
    }

    if(aTmp_barco.length()>2)
    {
        refresh=30;

        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
        system("beep &");
        system("echo \"\" > /tmp/rakinda.data");
        system("OpenDoor.sh &");
    }
    //=============================================================================
    if( read_ip == 0)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
     }

    if( loop%10==3)
    {
        system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}'> /tmp/ip.txt");
    }

    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
     }
    //=============================================================================
    read("/tmp/moti.data");
    if(aTmp.length()>5)
    {
        refresh=30;
        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
        system("beep &");
        system("echo \"\" > /tmp/moti.data");
        system("OpenDoor.sh &");
        QCoreApplication::processEvents();
    }
    //=============================================================================
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}

void MainWindow::on_pushButton_clicked()
{
    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName("/foobar/nonwritable.pdf");
    QPainter painter;
    if (! painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return;
    }
    painter.drawText(10, 10, "Test");
    if (! printer.newPage()) {
        qWarning("failed in flushing page to disk, disk full?");
        return;
    }
    painter.drawText(10, 10, "Test 2");
    painter.end();
}
