#include "mainwindow.h"
//#include <QApplication>
#include <QtWidgets/QApplication>
#include <QLabel>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCursor cursor(Qt::BlankCursor);
    QApplication::setOverrideCursor(cursor);
    QApplication::changeOverrideCursor(cursor);


    QImage myImage;
  //myImage.scaled(320,240, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    myImage.load(argv[1]);

    QLabel myLabel;
    myLabel.setPixmap(QPixmap::fromImage(myImage) );
//    myLabel.setScaledContents( true );

    myLabel.setGeometry(0,0,320,240);
    myLabel.show();

    return a.exec();
}
