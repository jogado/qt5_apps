/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *SplashScreen;
    QWidget *MainScreen;
    QLabel *Counter;
    QLabel *ip_address;
    QLabel *free_text;
    QPushButton *pushButton;
    QPushButton *pb_adult_plus;
    QPushButton *pb_child_plus;
    QPushButton *pb_adult_minus;
    QPushButton *pb_child_minus;
    QLabel *ChildCount;
    QLabel *AdultCount;
    QLabel *TotalValue;
    QLabel *Counter_2;
    QLabel *TheDate;
    QLabel *TheTime;
    QPushButton *pb_InvalidCard;
    QPushButton *pb_ValidCard;
    QWidget *Service;
    QPushButton *pb_moti;
    QPushButton *pb_reboot;
    QPushButton *pb_barcode;
    QLabel *BuildDate;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(320, 240);
        MainWindow->setStyleSheet(QStringLiteral("color:black;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 320, 240));
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QStringLiteral(""));
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QStringLiteral("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QStringLiteral("image:url(:/bmp/IT-Trans.bmp)"));
        stackedWidget->addWidget(SplashScreen);
        MainScreen = new QWidget();
        MainScreen->setObjectName(QStringLiteral("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QStringLiteral(""));
        Counter = new QLabel(MainScreen);
        Counter->setObjectName(QStringLiteral("Counter"));
        Counter->setEnabled(true);
        Counter->setGeometry(QRect(30, 214, 120, 20));
        QFont font;
        font.setFamily(QStringLiteral("Liberation Sans"));
        font.setPointSize(12);
        font.setItalic(false);
        Counter->setFont(font);
        Counter->setStyleSheet(QLatin1String("color: rgb(29, 60, 118);\n"
"\n"
""));
        Counter->setLineWidth(2);
        Counter->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        ip_address = new QLabel(MainScreen);
        ip_address->setObjectName(QStringLiteral("ip_address"));
        ip_address->setGeometry(QRect(183, 4, 130, 20));
        QFont font1;
        font1.setFamily(QStringLiteral("Liberation Sans"));
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        ip_address->setFont(font1);
        ip_address->setStyleSheet(QLatin1String("color: rgb(29, 60, 118);\n"
"\n"
""));
        ip_address->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        free_text = new QLabel(MainScreen);
        free_text->setObjectName(QStringLiteral("free_text"));
        free_text->setGeometry(QRect(10, 510, 221, 60));
        QFont font2;
        font2.setFamily(QStringLiteral("Liberation Sans"));
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setWeight(75);
        free_text->setFont(font2);
        free_text->setStyleSheet(QLatin1String("color:white;\n"
""));
        free_text->setLineWidth(2);
        free_text->setAlignment(Qt::AlignCenter);
        free_text->setWordWrap(true);
        pushButton = new QPushButton(MainScreen);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setEnabled(false);
        pushButton->setGeometry(QRect(0, 0, 320, 240));
        pushButton->setStyleSheet(QLatin1String("border: none;\n"
"background-image: url(:/bmp/main.bmp);"));
        pb_adult_plus = new QPushButton(MainScreen);
        pb_adult_plus->setObjectName(QStringLiteral("pb_adult_plus"));
        pb_adult_plus->setGeometry(QRect(238, 79, 53, 37));
        pb_adult_plus->setStyleSheet(QLatin1String("background-image: url(:/bmp/plus.bmp);\n"
"border: none;"));
        pb_child_plus = new QPushButton(MainScreen);
        pb_child_plus->setObjectName(QStringLiteral("pb_child_plus"));
        pb_child_plus->setGeometry(QRect(238, 121, 53, 37));
        pb_child_plus->setStyleSheet(QLatin1String("background-image: url(:/bmp/plus.bmp);\n"
"border: none;"));
        pb_adult_minus = new QPushButton(MainScreen);
        pb_adult_minus->setObjectName(QStringLiteral("pb_adult_minus"));
        pb_adult_minus->setGeometry(QRect(40, 79, 53, 37));
        pb_adult_minus->setStyleSheet(QLatin1String("background-image: url(:/bmp/min.bmp);\n"
"border: none;"));
        pb_child_minus = new QPushButton(MainScreen);
        pb_child_minus->setObjectName(QStringLiteral("pb_child_minus"));
        pb_child_minus->setGeometry(QRect(40, 121, 53, 37));
        pb_child_minus->setStyleSheet(QLatin1String("background-image: url(:/bmp/min.bmp);\n"
"border: none;"));
        ChildCount = new QLabel(MainScreen);
        ChildCount->setObjectName(QStringLiteral("ChildCount"));
        ChildCount->setEnabled(true);
        ChildCount->setGeometry(QRect(187, 124, 41, 31));
        QFont font3;
        font3.setFamily(QStringLiteral("Liberation Sans"));
        font3.setPointSize(20);
        font3.setBold(true);
        font3.setItalic(false);
        font3.setWeight(75);
        ChildCount->setFont(font3);
        ChildCount->setStyleSheet(QLatin1String("color:white;\n"
"\n"
""));
        ChildCount->setLineWidth(2);
        ChildCount->setAlignment(Qt::AlignCenter);
        AdultCount = new QLabel(MainScreen);
        AdultCount->setObjectName(QStringLiteral("AdultCount"));
        AdultCount->setEnabled(true);
        AdultCount->setGeometry(QRect(187, 81, 41, 31));
        AdultCount->setFont(font3);
        AdultCount->setStyleSheet(QLatin1String("color:white;\n"
"\n"
""));
        AdultCount->setLineWidth(2);
        AdultCount->setAlignment(Qt::AlignCenter);
        TotalValue = new QLabel(MainScreen);
        TotalValue->setObjectName(QStringLiteral("TotalValue"));
        TotalValue->setEnabled(true);
        TotalValue->setGeometry(QRect(105, 178, 120, 30));
        QFont font4;
        font4.setFamily(QStringLiteral("Liberation Sans"));
        font4.setPointSize(25);
        font4.setBold(true);
        font4.setItalic(false);
        font4.setWeight(75);
        TotalValue->setFont(font4);
        TotalValue->setStyleSheet(QLatin1String("color:white;\n"
"\n"
""));
        TotalValue->setLineWidth(2);
        TotalValue->setAlignment(Qt::AlignCenter);
        Counter_2 = new QLabel(MainScreen);
        Counter_2->setObjectName(QStringLiteral("Counter_2"));
        Counter_2->setEnabled(true);
        Counter_2->setGeometry(QRect(435, 768, 31, 20));
        QFont font5;
        font5.setFamily(QStringLiteral("Liberation Sans"));
        font5.setPointSize(10);
        font5.setItalic(false);
        Counter_2->setFont(font5);
        Counter_2->setStyleSheet(QLatin1String("color:white;\n"
"\n"
""));
        Counter_2->setLineWidth(2);
        Counter_2->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        TheDate = new QLabel(MainScreen);
        TheDate->setObjectName(QStringLiteral("TheDate"));
        TheDate->setEnabled(true);
        TheDate->setGeometry(QRect(370, 14, 100, 20));
        TheDate->setFont(font);
        TheDate->setStyleSheet(QLatin1String("color:white;\n"
""));
        TheDate->setLineWidth(2);
        TheDate->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        TheTime = new QLabel(MainScreen);
        TheTime->setObjectName(QStringLiteral("TheTime"));
        TheTime->setEnabled(true);
        TheTime->setGeometry(QRect(370, 36, 100, 20));
        TheTime->setFont(font);
        TheTime->setStyleSheet(QLatin1String("color:white\n"
"\n"
""));
        TheTime->setLineWidth(2);
        TheTime->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        pb_InvalidCard = new QPushButton(MainScreen);
        pb_InvalidCard->setObjectName(QStringLiteral("pb_InvalidCard"));
        pb_InvalidCard->setGeometry(QRect(3, 161, 314, 51));
        pb_InvalidCard->setStyleSheet(QLatin1String("background-image: url(:/bmp/invalid.bmp);\n"
"border: none;"));
        pb_ValidCard = new QPushButton(MainScreen);
        pb_ValidCard->setObjectName(QStringLiteral("pb_ValidCard"));
        pb_ValidCard->setGeometry(QRect(3, 161, 314, 51));
        pb_ValidCard->setStyleSheet(QLatin1String("background-image: url(:/bmp/valid.bmp);\n"
"border: none;"));
        stackedWidget->addWidget(MainScreen);
        pushButton->raise();
        Counter->raise();
        free_text->raise();
        pb_adult_plus->raise();
        pb_child_plus->raise();
        pb_adult_minus->raise();
        pb_child_minus->raise();
        ChildCount->raise();
        AdultCount->raise();
        TotalValue->raise();
        Counter_2->raise();
        TheDate->raise();
        TheTime->raise();
        pb_InvalidCard->raise();
        pb_ValidCard->raise();
        ip_address->raise();
        Service = new QWidget();
        Service->setObjectName(QStringLiteral("Service"));
        pb_moti = new QPushButton(Service);
        pb_moti->setObjectName(QStringLiteral("pb_moti"));
        pb_moti->setGeometry(QRect(30, 20, 261, 37));
        pb_moti->setStyleSheet(QStringLiteral(""));
        pb_reboot = new QPushButton(Service);
        pb_reboot->setObjectName(QStringLiteral("pb_reboot"));
        pb_reboot->setGeometry(QRect(32, 146, 261, 37));
        pb_reboot->setStyleSheet(QStringLiteral(""));
        pb_barcode = new QPushButton(Service);
        pb_barcode->setObjectName(QStringLiteral("pb_barcode"));
        pb_barcode->setGeometry(QRect(30, 90, 261, 37));
        pb_barcode->setStyleSheet(QStringLiteral(""));
        BuildDate = new QLabel(Service);
        BuildDate->setObjectName(QStringLiteral("BuildDate"));
        BuildDate->setEnabled(true);
        BuildDate->setGeometry(QRect(19, 220, 281, 20));
        BuildDate->setFont(font);
        BuildDate->setStyleSheet(QLatin1String("color: rgb(29, 60, 118);\n"
"\n"
""));
        BuildDate->setLineWidth(2);
        BuildDate->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        stackedWidget->addWidget(Service);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        Counter->setText(QApplication::translate("MainWindow", "0", nullptr));
        ip_address->setText(QApplication::translate("MainWindow", "000.000.000.000", nullptr));
        free_text->setText(QApplication::translate("MainWindow", "---", nullptr));
        pushButton->setText(QString());
        pb_adult_plus->setText(QString());
        pb_child_plus->setText(QString());
        pb_adult_minus->setText(QString());
        pb_child_minus->setText(QString());
        ChildCount->setText(QApplication::translate("MainWindow", "0", nullptr));
        AdultCount->setText(QApplication::translate("MainWindow", "0", nullptr));
        TotalValue->setText(QApplication::translate("MainWindow", "\342\202\254 0.00", nullptr));
        Counter_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        TheDate->setText(QApplication::translate("MainWindow", "0", nullptr));
        TheTime->setText(QApplication::translate("MainWindow", "0", nullptr));
        pb_InvalidCard->setText(QString());
        pb_ValidCard->setText(QString());
        pb_moti->setText(QApplication::translate("MainWindow", "MOTI", nullptr));
        pb_reboot->setText(QApplication::translate("MainWindow", "REBOOT", nullptr));
        pb_barcode->setText(QApplication::translate("MainWindow", "BARCODE", nullptr));
        BuildDate->setText(QApplication::translate("MainWindow", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
