#!/bin/sh

#===================================================
# From mv3000_demo_qt5.sh
#===================================================


# APP="abt3000-uitp-barcelona-pm"
# APP="mv3000l-london"
APP="abt3000-480x800-nt"
#APP="/usr/bin/demo-480x800-tc"

#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
EMV3000_DEVICE="/dev/emv3000"
BARCODE_DEVICE="/dev/barcode"
#----------------------------------------------------------------


#----------------------------------------------------------------
kill_it() {
    # Check if a process name was provided
    if [ -z "$1" ]; then
        echo "Usage: kill_it <process_name>"
        return 1
    fi

    process_name="$1"

    # Use pgrep to find PIDs of the process
    pids=$(pgrep -f "$process_name")


    # Check if any PIDs were found
    if [ -n "$pids" ]; then
        for pid in $pids; do
            # Kill each PID individually
            kill -9 "$pid" && echo "Killed process : $1 ( PID: $pid )"
        done
    else
        echo "No     process : $1 found"
    fi
}
#----------------------------------------------------------------



killall_it()
{
	pid=`pidof $1`
      	if [ "$pid" != "" ]; then
                killall $1
       	fi
}
rm_it()
{
      	if [ -e $1 ]; then

                rm $1
       	fi
}


#------------------------------------------------------------------------------
clean_up()
{
	kill_it moti.sh
	kill_it moti
	kill_it barcode
	kill_it x11vnc
    kill_it $APP
    kill_it demo_qt5.sh
	echo "Cleaning_done"
	exit 0
}
#trap clean_up SIGTERM SIGINT SIGTSTP EXIT
trap clean_up SIGTERM EXIT

#------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
echo $$ > $TMP_FOLDER/demo_qt5.pid
#------------------------------------------------------------------------------

#----------------------------------------------------------------
if [ -d "/usr/lib/fonts" ]
then
	echo "Fonts folder present"
else
	echo "Installing Fonts folder"
	ln -s /usr/share/fonts/ttf/ /usr/lib/fonts
fi
#----------------------------------------------------------------


echo 0 > /sys/class/graphics/fbcon/cursor_blink
export DISPLAY=:0.0
X -nocursor -s 0 &

if [ -f "/usr/bin/x11vnc" ]
then
	echo "VNC server present"
	if [ -d "/etc/vnc" ]
	then
		echo "VNC folder exist"
	else
		echo "VNC init *****"
		mkdir /etc/vnc
		x11vnc -storepasswd emsyscon /etc/vnc/passfile
		sleep 1
	fi
	x11vnc -rfbauth /etc/vnc/passfile &
fi
sleep 1
#=======================================================================

kill_it moti.sh
kill_it moti
kill_it barcode

sleep 1

`$APP`

clean_up

