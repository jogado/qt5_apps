#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>

QString aTmp;
QString aTmp_barco;

int ret=0;
int barleds=0;

int NbAdult=1;
int NbChild=0;
float Total=0;

float fAdult=2.2;
float fChild=1.5;

#define SPLASH_SCREEN     0
#define MAIN_SCREEN       1
#define SERVICE_SCREEN    2

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label1;
QString Label2;
QString Label3;



char aBufOld[256];
char *pBufOld;


int loop=0;
int read_ip=0;
int refresh=0;

int BARLED_IS_ON = 0;
int BARLED_COLOR = 0;

char aBuf[256];
const char *aBuf2;


int BeepIsOn=1;
int init_done=0;
int Timeout=50;


char Currency_char[10];

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

   pBufOld = &aBufOld[0];

    ui->setupUi(this);

    ui->Counter->setAttribute   (Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute (Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;

    ret=system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    ret=system("echo \"-----\" > /tmp/text.txt");
    ret=system("echo \"\" > /tmp/moti.data");
    ret=system("echo \"\" > /tmp/rakinda.data");

    if( barleds )
    {
        ret=system("echo 0 > /sys/class/leds/green1/brightness");
        ret=system("echo 0 > /sys/class/leds/red1/brightness");
        ret=system("echo 0 > /sys/class/leds/green2/brightness");
        ret=system("echo 0 > /sys/class/leds/red2/brightness");
    }



    ui->pb_ValidCard->setVisible(false);
    ui->pb_InvalidCard->setVisible(false);
    ui->pb_waiting->setVisible(false);
    ui->TheDate->setVisible(false);
    ui->free_text->setVisible(false);
    ui->TheTime->setVisible(false);
    ui->Counter_2->setVisible(true);


    ui->label_header_00->setVisible(false);

    Update_Values();

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);


    read_header(0);
    read_header(1);
    read_header(2);
    read_header(3);
    read_header(4);
    read_header(5);
    read_header(6);
    read_header(7);
    read_header(8);
    read_header(9);
}

MainWindow::~MainWindow()
{
    delete ui;
}




//=============================================================================
int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}
//=============================================================================
void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    QCoreApplication::processEvents();
    aTmp = in.readAll();
    file.close();
}
//=============================================================================
void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }
    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}
//=============================================================================



void MainWindow::Update_Values()
{
    char aTmp[100];

    Total=((float)NbAdult * fAdult ) + ((float)NbChild * fChild);


    sprintf(aTmp,"%d",NbAdult);
    ui->AdultCount->setText(aTmp);

    sprintf(aTmp,"%d",NbChild);
    ui->ChildCount->setText(aTmp);


    sprintf(aTmp,"  %4.2f",Total);
    aTmp[0]=Currency_char[0];
    ui->TotalValue->setText(aTmp);

    QCoreApplication::processEvents();
}
//=============================================================================










#define WAITING 0
#define WORKING 1

int STATE=WAITING;




//=========================================================================================
void MainWindow::Led_RED()
{
    STATE=WORKING;

    ui->pb_ValidCard->setVisible(false);
    ui->pb_waiting->setVisible(false);
    ui->pb_InvalidCard->setVisible(true);

    ui->label_header_09->setVisible(false);

    QCoreApplication::processEvents();
    QCoreApplication::processEvents();

    ret=system("edgeled -w 11 -c red -stay_on");

    if( barleds )
    {
        ret=system("echo 0 > /sys/class/leds/green1/brightness");
        ret=system("echo 0 > /sys/class/leds/green2/brightness");
        ret=system("echo 255 > /sys/class/leds/red1/brightness");
        ret=system("echo 255 > /sys/class/leds/red2/brightness");
    }

    if( BeepIsOn )  ret=system("beep -f 100 -l 300 &");
}
//=========================================================================================
void MainWindow::Led_GREEN()
{
    STATE=WORKING;
    ui->pb_waiting->setVisible(false);
    ui->pb_InvalidCard->setVisible(false);
    ui->pb_ValidCard->setVisible(true);

    ui->label_header_09->setVisible(false);


    QCoreApplication::processEvents();
    QCoreApplication::processEvents();

    ret=system("edgeled -w 11 -c green -stay_on ");

    if( barleds )
    {
        ret=system("echo 255 > /sys/class/leds/green1/brightness");
        ret=system("echo 255 > /sys/class/leds/green2/brightness");
        ret=system("echo 0 > /sys/class/leds/red1/brightness");
        ret=system("echo 0 > /sys/class/leds/red2/brightness");
    }

    if( BeepIsOn )  ret=system("beep &");
}
//=========================================================================================
void MainWindow::Led_OFF()
{
    STATE=WAITING;

    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);


    ui->pb_ValidCard->setVisible(false);
    ui->pb_InvalidCard->setVisible(false);
    ui->pb_waiting->setVisible(true);

    ui->label_header_09->setVisible(true);

    QCoreApplication::processEvents();
    QCoreApplication::processEvents();



    if( barleds )
    {
        ret=system("echo 0 > /sys/class/leds/green1/brightness");
        ret=system("echo 0 > /sys/class/leds/green2/brightness");
        ret=system("echo 0 > /sys/class/leds/red1/brightness");
        ret=system("echo 0 > /sys/class/leds/red2/brightness");
    }

    QCoreApplication::processEvents();

}
//=========================================================================================







//=========================================================================================

void MainWindow::read_header(int header)
{
    QLabel *l = nullptr;
    QString Empty = "****";
    char aBuffer[20];


    if(header==0)
    {
        read("/etc//demo/header_00.txt");
        sprintf(Currency_char,"%s",aTmp.toLatin1().data());
        return;
    }

    if(header== 0) { read("/etc/demo/header_00.txt"); l=ui->label_header_00;}
    if(header== 1) { read("/etc/demo/header_01.txt"); l=ui->label_header_01;}
    if(header== 2) { read("/etc/demo/header_02.txt"); l=ui->label_header_02;}
    if(header== 3) { read("/etc/demo/header_03.txt"); l=ui->label_header_03;}
    if(header== 4) { read("/etc/demo/header_04.txt"); l=ui->label_header_04;}
    if(header== 5) { read("/etc/demo/header_05.txt"); l=ui->label_header_05;}
    if(header== 6) { read("/etc/demo/header_06.txt"); l=ui->label_header_06;}
    if(header== 7) { read("/etc/demo/header_07.txt"); l=ui->label_header_07;}
    if(header== 8) { read("/etc/demo/header_08.txt"); l=ui->label_header_08;}
    if(header== 9) { read("/etc/demo/header_09.txt"); l=ui->label_header_09;}



    if( header == 5  )
    {
        fAdult = aTmp.toFloat();
    }

    if( header == 7  )
    {
        fChild = aTmp.toFloat();
    }


    if (l == nullptr)
    {
        return;
    }


    if( header == 5 || header == 7 )
    {
        sprintf(aBuffer,"  %s",aTmp.toLatin1().data());
        aBuffer[0]=Currency_char[0];
        l->setText(aBuffer);
        return;
    }

    if(aTmp.length()>0)
    {

        l->setText(aTmp);
    }
    else
    {
         l->setText(Empty);
    }
}
//=========================================================================================



char aTmp2[256];
int Card_Present=0;
int Toggle=0;


void MainWindow::MainTask_loop()
{
    QString res = "reset";
    int sec,min,hour,days;
    int len;
    unsigned char c;
    char aBuf[100];


    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime   = current.toString("hh:mm:ss");
    QString TheDate   = current.toString("dd/MM/yyyy");

    ui->TheTime->setText(TheTime);
    ui->TheDate->setText(TheDate);

    loop++;



    //=============================================================================
     if( loop%40==1 )
     {
         ret=system("/usr/bin/start_moti.sh &");
     }
     //=============================================================================


     //=============================================================================
     if(loop%10==5 )
     {
         if(Toggle)
         {
            Toggle=0;
            ui->pb_waiting->setVisible(false);
         }
         else
         {
             if(STATE==WAITING)
             {
                Toggle=1;
                ui->pb_waiting->setVisible(true);
             }
         }
     }
     //=============================================================================


     //=========================================================================
    if( DelayTimer)
    {
        DelayTimer--;
        sprintf(aBuf,"%d",DelayTimer);
        ui->Counter_2->setText(aBuf);

        sprintf(aBuf,"%d",DelayTimer/10);
        ui->free_text->setText(aBuf);

        if( DelayTimer == 0 )
        {
            NbAdult=1;
            NbChild=0;
            Update_Values();
        }
    }
    //=========================================================================

    //=========================================================================
    if( DelayTimer == 0 && CurrentScreen != MAIN_SCREEN)
    {
        CurrentScreen = MAIN_SCREEN;
        ui->stackedWidget->setCurrentIndex(CurrentScreen);
        NbAdult=1;
        NbChild=0;
        Update_Values();
    }
    //=========================================================================



    //=========================================================================
    if(loop%10==0)
    {
        sec = loop/10;
        min = sec/60;
        hour = min/60;
        days = hour/24;
        sprintf(aBuf,"(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
        ui->Counter->setText(aBuf);
    }
    //=========================================================================


    //=============================================================================
    if( refresh > 0)
    {
        refresh--;
        if(refresh==0)
        {
            ui->free_text->setText("");
        }
    }
    //=============================================================================

    //=============================================================================
    if( loop%10==5)
    {
        ret=system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
    }
    //=============================================================================


    //=============================================================================
    read("/tmp/moti.data");
    if(aTmp.length()>10 )
    {
        Card_Present = 1;
    }
    else
    {
        Card_Present = 0;
        if(loop%10==5)
        {
            sprintf(pBufOld,"%s","");
        }
    }
    //=============================================================================


    //=============================================================================
    if( (DelayTimer%30) == 15 )
    {
        ret=system("echo \"\" > /tmp/moti.data");
        ret=system("echo \"Clearing moti.data ....\n\r\" ");
    }
    //=============================================================================

    if(Card_Present )
    {
        refresh=30;
        len=aTmp.length();
        aBuf2=aTmp.toLatin1().data();
        c = aBuf2[len-4];

        ret=system("echo \"\" > /tmp/moti.data");


        if( strstr ( aBuf2,"[04 35 C4 8A 68 67 80 ]")!= NULL  ||  strstr ( aBuf2,"[04 BB 34 8A 68 67 84 ]") != NULL )
        {
            Clear_Service_buttons();
            CurrentScreen = SERVICE_SCREEN;
            ui->stackedWidget->setCurrentIndex(CurrentScreen);
            DelayTimer = 50;
            return;
        }

        if( strcmp ( aBuf2,pBufOld) == 0 )
        {
            sprintf(aTmp2,"echo \"EQUAL: <%s><%s>\n\r\"",aBuf2,pBufOld );
            ret=system(aTmp2);
            return;
        }
        else
        {
            sprintf(aTmp2,"echo \"DIFFERENT: <%s><%s>\n\r\"",aBuf2,pBufOld );
            ret=system(aTmp2);
            sprintf(pBufOld,"%s",aBuf2);
            QCoreApplication::processEvents();
        }

        DelayTimer = 30;

        if(NbAdult || NbChild )
        {
            NbAdult=1;
            NbChild=0;

            Update_Values();

            if ( c%2==0 ) Led_RED();
            if ( c%2==1 ) Led_GREEN();
        }

        BARLED_IS_ON = 7; // (7 x 100 ms)
        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
    }
    QCoreApplication::processEvents();
    //=============================================================================





    //=============================================================================
    read_barcode("/tmp/rakinda.data");
    if(aTmp_barco.length()>5)
    {
        refresh=30;

        len=aTmp_barco.length(); QCoreApplication::processEvents();
        aBuf2=aTmp_barco.toLatin1().data();
        c = aBuf2[len-2];
        ret=system("echo \"\" > /tmp/rakinda.data");

        if( strstr ( aBuf2,"ESC_SERVICECARD1")  )
        {
            ret=system("beep &");
            Clear_Service_buttons();
            CurrentScreen = SERVICE_SCREEN;
            ui->stackedWidget->setCurrentIndex(CurrentScreen);
            DelayTimer = 50;
            return;
        }

        DelayTimer = 30;

        if(NbAdult || NbChild )
        {
            NbAdult=1;
            NbChild=0;

            Update_Values();

            if ( c%2==0 ) Led_RED();
            if ( c%2==1 ) Led_GREEN();
        }
        BARLED_IS_ON = 7; // (7 x 100 ms)

        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
        //ret=system("echo \"\" > /tmp/rakinda.data");
        //QCoreApplication::processEvents();
    }
    //=============================================================================


   //=============================================================================
    if( BARLED_IS_ON > 0 )
    {
         BARLED_IS_ON--;
         if( BARLED_IS_ON == 0 )
         {
              Led_OFF();
         }
    }
    //=============================================================================
    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::on_pb_adult_minus_clicked()
{
    DelayTimer = 50;

    if(NbAdult<=1) return ;
    NbAdult--;

    Update_Values();

    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_adult_plus_clicked()
{
    DelayTimer = 50;

    if(NbAdult>=9) return;

    NbAdult++;

    Update_Values();

    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_child_minus_clicked()
{
    DelayTimer = 50;

    if(NbChild<=0) return;

    NbChild--;

    Update_Values();

    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_child_plus_clicked()
{
    DelayTimer = 50;

    if(NbChild>=9) return;

    NbChild++;

    Update_Values();

    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================


void MainWindow::Clear_Service_buttons()
{
    ui->pb_moti->setStyleSheet("background-color: white;\nborder-color: rgb(32, 74, 135);\ncolor: rgb(20, 60, 118);\nborder: 3px solid rgb(32, 74, 135);");
    ui->pb_barcode->setStyleSheet("background-color: white;\nborder-color: rgb(32, 74, 135);\ncolor: rgb(20, 60, 118);\nborder: 3px solid rgb(32, 74, 135);");
    ui->pb_reboot->setStyleSheet("background-color: white;\nborder-color: rgb(32, 74, 135);\ncolor: rgb(20, 60, 118);\nborder: 3px solid rgb(32, 74, 135);");
}



//=========================================================================================
void MainWindow::on_pb_moti_clicked()
{
    ui->pb_moti->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("beep &");
    DelayTimer = 5;
    ret=system("echo \"Killall moti\"");
    ret=system("killall moti");
    QCoreApplication::processEvents();
}
//=========================================================================================

//=========================================================================================
void MainWindow::on_pb_barcode_clicked()
{
    ui->pb_barcode->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("beep &");
    DelayTimer = 5;
    ret=system("echo \"Killall barcode\"");
    ret=system("killall barcode");
    QCoreApplication::processEvents();
}

//=========================================================================================
void MainWindow::on_pb_reboot_clicked()
{
    ui->pb_reboot->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");

    Led_OFF();
    ret=system("beep &");
    ret=system("reboot &");
    QCoreApplication::processEvents();
}
//=========================================================================================
