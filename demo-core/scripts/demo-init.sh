#!/bin/sh


CONFIG_FILE=/etc/demo/demo.config


if [ ! -f $CONFIG_FILE ]
then
	echo "demo-init.sh: No demo config file <$CONFIG_FILE>" > /dev/kmsg
    exit
fi

RET=`cat $CONFIG_FILE | grep -i AUTO-START |cut -c21-`

if [ "$RET" == "YES" ]
then
	echo "demo-init.sh: Autostart demo enabled" > /dev/kmsg
else
	echo "demo-init.sh: Autostart demo disable" > /dev/kmsg
    exit
fi

echo "demo-init.sh: starting demo_qt5.sh" > /dev/kmsg
/usr/bin/demo_qt5.sh &
