#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTouchEvent>
#include <QTimer>

QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;

char aBarco[] = "Barcode input";


int inactivity=0;
int State=1;


#define MAIN_SCREEN       0
#define CARD_SCREEN       1
#define SPLASH_SCREEN     2
#define BARCODE_SCREEN    3
#define ABOUT_SCREEN      4

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label2;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    system("echo 255 > /sys/class/backlight/lcd_backlight/brightness");

    ui->setupUi(this);


    ui->Counter->setVisible(true);


    ui->time_date->setVisible(false);
    ui->time_text->setVisible(false);
    //ui->Exit_button->setVisible(false);


   // ui->BarcodeScreen->setStyleSheet ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
   // ui->CardScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
   // ui->MainScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/mainscreen.jpg\"); background-position: center;" );
   // ui->SplashScreen->setStyleSheet  ( "background-image:url(\"/etc/demo/jpg/splashscreen.jpg\"); background-position: center;" );
   // ui->AboutScreen->setStyleSheet   ( "background-image:url(\"/etc/demo/jpg/aboutscreen.jpg\"); background-position: center;" );

   // ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
   // CurrentScreen = SPLASH_SCREEN;
   // DelayTimer = 50;

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;

    setAttribute(Qt::WA_AcceptTouchEvents, true);


    system("ifconfig eth0 |grep 'inet addr' |cut -c 21-34 > ip.txt");
    system("echo \"-----\" > text.txt");

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));


    MainTask->start(100);



    // system("./mute.sh");
}

MainWindow::~MainWindow()
{
    delete ui;
}


bool MainWindow::event(QEvent* event)
{
    char aTmp[100];

//    sprintf(aTmp,"echo Event = %03X ",event->type());
//    system(aTmp);

    switch (event->type())
    {
        case QEvent::TouchBegin:
            inactivity  = 0;
            State       = 1;
            system("echo 255 > /sys/class/backlight/lcd_backlight/brightness");
            break;
        default:
            break;
            // call base implementation
    }
   return QMainWindow::event(event);
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}







int loop=0;

int read_ip=0;
void MainWindow::MainTask_loop()
{


//   QString text = "Hour:" + QTime::currentTime().toString();

    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime = current.toString("hh:mm:ss");
    QString TheDate = current.toString("dd/MM/yyyy");
    QString res = "reset";
    QString CountDown;

    loop++;
    inactivity++;
    if(inactivity > 100 && State==1 )
    {
        inactivity = 0;
        State=0;
        system("echo 5 > /sys/class/backlight/lcd_backlight/brightness");
    }

    if(State==1 )
    {
        CountDown.sprintf("Countdown: %02d",10 - (inactivity/10));
        ui->label_3->setText(CountDown);
    }



    Label2.sprintf("%09d",loop/10);
    ui->Counter->setText(Label2);


    ui->time_text->setText(TheTime);
    ui->time_date->setText(TheDate);

    if( DelayTimer) DelayTimer--;

    if( (DelayTimer == 0) && (CurrentScreen != MAIN_SCREEN))
    {
       ui->stackedWidget->setCurrentIndex(0);
       CurrentScreen = MAIN_SCREEN ;
    }



    if(loop%600 == 0 )
    {
        system("echo O >  /sys/class/graphics/fb0/blank");
    }



    //=============================================================================
    read_barcode("text.txt");

    if(aTmp_barco.length()<3)
    {
        loop=0;
        ui->free_text->setText(aTmp_barco);
    }

    if(aTmp_barco.length()>2)
    {
        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
    }
    //=============================================================================
    if( read_ip == 0)
    {
        read("ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
    }
    //=============================================================================
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}





void MainWindow::on_bt_1_clicked()
{
    ui->textBrowser->setStyleSheet ( "background-image: url(:/jpg/citroen.jpg) 0 0 0 0 stretch stretch;" );
    system("beep");
}

void MainWindow::on_bt_2_clicked()
{
    ui->textBrowser->setStyleSheet ( "background-image: url(:/jpg/ferrari.jpg) 0 0 0 0 stretch stretch;" );
    system("beep");
}

void MainWindow::on_bt_3_clicked()
{
    ui->textBrowser->setStyleSheet ( "background-image: url(:/jpg/Lamborghini.jpg) contain;");
      system("beep");
}

void MainWindow::on_bt_4_clicked()
{
    ui->textBrowser->setStyleSheet ( "background-image: url(:/jpg/mercedes.jpg) 0 0 0 0 stretch stretch;" );
      system("beep");
}

void MainWindow::on_bt_5_clicked()
{
    ui->textBrowser->setStyleSheet ( "background-image: url(:/jpg/porche.jpg) 0 0 0 0 stretch stretch;" );
      system("beep");
}

void MainWindow::on_bt_6_clicked()
{
    ui->textBrowser->setStyleSheet ( "background-image: url(:/jpg/rolls.jpg) 0 0 0 0 stretch stretch;" );
      system("beep");
}
