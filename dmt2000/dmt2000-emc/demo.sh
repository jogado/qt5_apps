#!/bin/sh


clean_up()
{
	killall Xorg
	killall smd-test-abt3000.sh
	echo "Cleaning_done"
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT 



if [ -d "/usr/lib/fonts" ]
then
	echo "Fonts folder present"
else
	echo "Installing Fonts folder"
	ln -s /usr/share/fonts/ttf/ /usr/lib/fonts
fi



export DISPLAY=:0.0
X &

sleep 1

./emc-test-dmt2000


clean_up

sleep 1


