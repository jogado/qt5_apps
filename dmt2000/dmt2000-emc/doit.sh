#!/bin/sh

. /opt/poky-emsyscon/2.4.1/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
make clean
qmake
make

ARG1=$1
FILE="emc-test-dmt2000"
if echo $1 | grep -q "192.168"; then

	echo "scp $FILE root@$1:/usr/bin"
	scp $FILE root@$1:/usr/bin
fi

