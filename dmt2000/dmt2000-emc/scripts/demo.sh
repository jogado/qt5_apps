#!/bin/sh


clean_up()
{
	killall Xorg
	killall moti
	killall test2
	rm moti.tmp
	echo "Cleaning_done"
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT 



if [ -d "/usr/lib/fonts" ]
then
	echo "Fonts folder present"
else
	echo "Installing Fonts folder"
	ln -s /usr/share/fonts/ttf/ /usr/lib/fonts
fi


rm moti.tmp

moti &
sleep 1

export DISPLAY=:0.0
X &

sleep 1

./test2


clean_up

sleep 1

