#!/bin/sh


env_imx6ul()
{
	echo "env : imx6ul"
	export ARCH=arm
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	. /opt/poky-emsyscon/3.1.3/cortexa7t2hf/environment-setup-cortexa7t2hf-neon-poky-linux-gnueabi
}

env_imx6s()
{
	echo "env : imx6s"
	export ARCH=arm
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	. /opt/poky-emsyscon/3.1.3/cortexa9t2hf/environment-setup-cortexa9t2hf-neon-poky-linux-gnueabi
}



if echo $1 | grep -q "imx6s"; then
	env_imx6s
elif  echo $1 | grep -q "imx6ul"; then
	env_imx6ul 
else
	echo "syntax doit [ imx6s | imx6ul ] < 192.168.0.xxx >"
	exit
fi

make clean
qmake
make

ARG1=$2
FILE="dmt2000-ittrans"
if echo $2 | grep -q "192.168"; then

	scp $FILE root@$2:/usr/bin/$FILE
	scp $FILE root@$2:/usr/bin/TheDemo
fi
