#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTouchEvent>
#include <QTimer>

QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;


int inactivity=0;
int State=1;


#define MAIN_SCREEN       0

#define ABOUT_SCREEN      1
#define ABT3000_SCREEN    2
#define MV3000_SCREEN     3
#define MG3000_SCREEN     4
#define EMV3000_SCREEN    5
#define DMT2000_SCREEN    6
#define DMT750_SCREEN     7

int CurrentScreen = ABOUT_SCREEN;
QString Label2;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

     ui->label_2->setVisible(false);
     ui->Counter->setVisible(false);


//    ui->time_date->setVisible(false);
//    ui->time_text->setVisible(false);

    //ui->Exit_button->setVisible(false);
   // ui->BarcodeScreen->setStyleSheet ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
   // ui->CardScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
   // ui->MainScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/mainscreen.jpg\"); background-position: center;" );
   // ui->SplashScreen->setStyleSheet  ( "background-image:url(\"/etc/demo/jpg/splashscreen.jpg\"); background-position: center;" );
   //ui->AboutScreen->setStyleSheet   ( "background-image:url(\"/etc/demo/jpg/aboutscreen.jpg\"); background-position: center;" );

    ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt750->setStyleSheet  ( "background-image: url(:/bmp/DMT750_OFF.bmp) 0 0 0 0 stretch stretch;" );





    ui->stackedWidget->setCurrentIndex(ABOUT_SCREEN);
    CurrentScreen = ABOUT_SCREEN;

    setAttribute(Qt::WA_AcceptTouchEvents, true);


    system("ifconfig eth0 |grep 'inet addr' |cut -c 21-34 > ip.txt");
    system("echo \"-----\" > text.txt");

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));


    MainTask->start(100);



    // system("./mute.sh");
}

MainWindow::~MainWindow()
{
    delete ui;
}


bool MainWindow::event(QEvent* event)
{
    char aTmp[100];

//    sprintf(aTmp,"echo Event = %03X ",event->type());
//    system(aTmp);

    switch (event->type())
    {
        case QEvent::TouchBegin:
            inactivity  = 0;
            State       = 1;
           // system("echo 255 > /sys/class/backlight/lcd_backlight/brightness");
            break;
        default:
            break;
            // call base implementation
    }
   return QMainWindow::event(event);
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}




int loop=0;

int read_ip=0;
void MainWindow::MainTask_loop()
{


//   QString text = "Hour:" + QTime::currentTime().toString();

    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime   = current.toString("hh:mm:ss");
    QString TheDate   = current.toString("dd/MM/yyyy");
    QString res = "reset";
    QString CountDown;

    loop++;

    inactivity++;


    if(inactivity > 300 && State==1 )
    {
        inactivity = 0;
        State=0;
        ui->pb_main->setStyleSheet    ( "background-image: url(:/bmp/Main.bmp) 0 0 0 0 stretch stretch;" );
        CurrentScreen = ABOUT_SCREEN ;

        ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_dmt750->setStyleSheet  ( "background-image: url(:/bmp/DMT750_OFF.bmp) 0 0 0 0 stretch stretch;" );
    }





    if(State==1 )
    {
        CountDown.sprintf("Count     %02d",30 - (inactivity/10));
        CountDown.sprintf("%02d",30 - (inactivity/10));
        ui->label_3->setText(CountDown);
    }


    Label2.sprintf("%09d",loop/10);
    ui->Counter->setText(Label2);


    ui->time_text->setText(TheTime);
    ui->time_date->setText(TheDate);


    //=============================================================================
    if( read_ip == 0)
    {
        read("ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
    }
    //=============================================================================
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}



void MainWindow::on_pb_main_clicked()
{

}

//========================================================================================
void MainWindow::on_pb_abt3000_clicked()
{
    ui->pb_main->setStyleSheet ( "background-image: url(:/bmp/ABT3000.bmp) 0 0 0 0 stretch stretch;" );

    ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_ON.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt750->setStyleSheet  ( "background-image: url(:/bmp/DMT750_OFF.bmp) 0 0 0 0 stretch stretch;" );

    inactivity=0;
    State=1;
    system("beep &");
}
//========================================================================================
void MainWindow::on_pb_mv3000_clicked()
{
    ui->pb_main->setStyleSheet ( "background-image: url(:/bmp/MV3000.bmp) 0 0 0 0 stretch stretch;" );

    ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_ON.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt750->setStyleSheet  ( "background-image: url(:/bmp/DMT750_OFF.bmp) 0 0 0 0 stretch stretch;" );

    inactivity=0;
    State=1;
    system("beep &");
}
//========================================================================================
void MainWindow::on_pb_mg3000_clicked()
{
   ui->pb_main->setStyleSheet ( "background-image: url(:/bmp/MG3000.bmp) 0 0 0 0 stretch stretch;" );

   ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
   ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
   ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_ON.bmp) 0 0 0 0 stretch stretch;" );
   ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
   ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
   ui->pb_dmt750->setStyleSheet  ( "background-image: url(:/bmp/DMT750_OFF.bmp) 0 0 0 0 stretch stretch;" );

   inactivity=0;
   State=1;
   system("beep &");
}

void MainWindow::on_pb_emv3000_clicked()
{
    ui->pb_main->setStyleSheet ( "background-image: url(:/bmp/EMV3000.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_ON.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt750->setStyleSheet  ( "background-image: url(:/bmp/DMT750_OFF.bmp) 0 0 0 0 stretch stretch;" );

    inactivity=0;
    State=1;
    system("beep &");
}
void MainWindow::on_pb_dmt2000_clicked()
{
     ui->pb_main->setStyleSheet ( "background-image: url(:/bmp/DMT2000.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_ON.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_dmt750->setStyleSheet  ( "background-image: url(:/bmp/DMT750_OFF.bmp) 0 0 0 0 stretch stretch;" );

     inactivity=0;
     State=1;
     system("beep &");
}
void MainWindow::on_pb_dmt750_clicked(bool checked)
{
     ui->pb_main->setStyleSheet ( "background-image: url(:/bmp/DMT750.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
     ui->pb_dmt750->setStyleSheet  ( "background-image: url(:/bmp/DMT750_ON.bmp) 0 0 0 0 stretch stretch;" );

    inactivity=0;
    State=1;
    system("beep &");
}
