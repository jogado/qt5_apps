#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTouchEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void MasterTimer_update();
    void MainTask_loop();
//    void on_bt_1_clicked();
//    void on_bt_2_clicked();
//    void on_bt_3_clicked();
//    void on_bt_4_clicked();
//    void on_bt_5_clicked();
//    void on_bt_6_clicked();

//    void on_BT_1_clicked();

//    void on_pushButton_2_clicked();

    void on_pb_abt3000_clicked();

//    void on_pb_abt3000_2_clicked();

    void on_pb_mv3000_clicked();

    void on_pb_main_clicked();

    void on_pb_mg3000_clicked();

    void on_pb_emv3000_clicked();

    void on_pb_dmt2000_clicked();

    void on_pb_dmt750_clicked(bool checked);

private:
    Ui::MainWindow *ui;

protected:
   // void touchEvent(QTouchEvent *event);
    bool event(QEvent* event);

};

#endif // MAINWINDOW_H
