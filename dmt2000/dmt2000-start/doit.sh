#!/bin/sh

if  echo $3 | grep -q "nobuild"; then

	echo "********************"
	echo "***   No build   ***"
	echo "********************"

else
	ARG1=$1
	if echo $1 | grep -q "imx6s"; then
		export ARCH=arm
		export CROSS_COMPILE=arm-poky-linux-gnueabi-
		. /opt/poky-emsyscon/3.1.3/cortexa9t2hf/environment-setup-cortexa9t2hf-neon-poky-linux-gnueabi

        make clean
        qmake
        make

	elif  echo $1 | grep -q "imx6ul"; then
		export ARCH=arm
		export CROSS_COMPILE=arm-poky-linux-gnueabi-
		. /opt/poky-emsyscon/3.1.3/cortexa7t2hf/environment-setup-cortexa7t2hf-neon-poky-linux-gnueabi

	elif  echo $1 | grep -q "imx8mm"; then
		export ARCH=arm64
		export CROSS_COMPILE=aarch64-linux-gnu-
		. /opt/poky-emsyscon/3.1.3/aarch64/environment-setup-aarch64-poky-linux 

	else
		echo "syntax doit [ imx6s | imx6ul |imx8mm ] < 192.168.0.xxx > <nobuild> <boot>"
		exit
	fi

fi












ARG1=$2
FILE="dmt2000-start"
if echo $2 | grep -q "192.168"; then
	scp $FILE root@$2:/usr/bin/$FILE
fi
