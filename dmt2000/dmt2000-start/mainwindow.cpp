#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTouchEvent>
#include <QTimer>

QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;

char aBarco[] = "Barcode input";


int inactivity=0;
int State=1;


#define MAIN_SCREEN       0
#define CARD_SCREEN       1
#define SPLASH_SCREEN     2
#define BARCODE_SCREEN    3
#define ABOUT_SCREEN      4

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 600;
QString Label2;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QDateTime current = QDateTime::currentDateTime();
    QString formattedTime = current.toString("hh:mm:ss");
    QString CountDown;

    system("echo 255 > /sys/class/backlight/lcd_backlight/brightness");

    ui->setupUi(this);

        /*
    ui->Counter->setVisible(true);
    ui->time_date->setVisible(true);
    ui->time_text->setVisible(true);
        */

    //ui->Exit_button->setVisible(false);


   // ui->BarcodeScreen->setStyleSheet ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
   // ui->CardScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
   // ui->MainScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/mainscreen.jpg\"); background-position: center;" );
   // ui->SplashScreen->setStyleSheet  ( "background-image:url(\"/etc/demo/jpg/splashscreen.jpg\"); background-position: center;" );
   // ui->AboutScreen->setStyleSheet   ( "background-image:url(\"/etc/demo/jpg/aboutscreen.jpg\"); background-position: center;" );

   // ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
   // CurrentScreen = SPLASH_SCREEN;
   // DelayTimer = 50;

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;

    setAttribute(Qt::WA_AcceptTouchEvents, true);


    system("ifconfig eth0 |grep 'inet addr' |cut -c 21-34 > ip.txt");
    system("echo \"-----\" > text.txt");

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));




    MainTask->start(100);



    // system("./mute.sh");
}

MainWindow::~MainWindow()
{
    delete ui;
}


//----------------------------------------------------------------------------------------------
bool MainWindow::event(QEvent* event)
{
 // char aTmp[100];
 // sprintf(aTmp,"echo Event = %03X ",event->type());
 // system(aTmp);

    switch (event->type())
    {
        case QEvent::TouchBegin:
            inactivity  = 0;
            State       = 1;
            system("echo 255 > /sys/class/backlight/lcd_backlight/brightness");
            break;
        default:
            break;
            // call base implementation
    }
   return QMainWindow::event(event);
}
//----------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------
int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}
//----------------------------------------------------------------------------------------------



void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}







int loop=0;

int read_ip=0;
void MainWindow::MainTask_loop()
{


//   QString text = "Hour:" + QTime::currentTime().toString();

    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime = current.toString("hh:mm:ss");
    QString TheDate = current.toString("dd/MM/yyyy");

    QString res = "reset";
    QString CountDown;

    loop++;
    inactivity++;
    if( ( inactivity > DelayTimer) && State==1 )
    {
        inactivity = 0;
        State=0;
        system("echo 5 > /sys/class/backlight/lcd_backlight/brightness");
    }

    if(State==1 )
    {
        CountDown.sprintf("%02d",(DelayTimer - inactivity)/10);
        ui->label_3->setText(CountDown);
    }


    ui->time_text->setText(TheTime);
    ui->time_date->setText(TheDate);

 //    qDebug() << "Local time is:" << formattedTime;


    /*
    if( DelayTimer) DelayTimer--;

    if( (DelayTimer == 0) && (CurrentScreen != MAIN_SCREEN))
    {
       ui->stackedWidget->setCurrentIndex(0);
       CurrentScreen = MAIN_SCREEN ;
    }
    */


    if(loop%600 == 0 )
    {
        system("echo O >  /sys/class/graphics/fb0/blank");
    }

    //=============================================================================
    if( read_ip == 0)
    {
        read("ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
    }
    //=============================================================================
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}




void MainWindow::on_pushButton_clicked()
{
    system("beep");
}
