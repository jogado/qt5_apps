#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTouchEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void MasterTimer_update();
    void MainTask_loop();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;

protected:
   // void touchEvent(QTouchEvent *event);
    bool event(QEvent* event);

};

#endif // MAINWINDOW_H
