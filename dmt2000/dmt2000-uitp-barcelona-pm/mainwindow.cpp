#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTouchEvent>
#include <QTimer>

QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;


int inactivity=0;
int State=1;


#define MAIN_SCREEN       0

#define ABOUT_SCREEN      1
#define ABT3000_SCREEN    2
#define MV3000_SCREEN     3
#define MG3000_SCREEN     4
#define EMV3000_SCREEN    5
#define DMT2000_SCREEN    6
#define PV3000_SCREEN     7

int CurrentScreen = ABOUT_SCREEN;
QString Label2;

int ret=0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    ui->Counter->setVisible(true);

    ui->time_date->setVisible(false);
    ui->time_text->setVisible(false);


    all_buttons_off();

    ui->stackedWidget->setCurrentIndex(ABOUT_SCREEN);
    CurrentScreen = ABOUT_SCREEN;

    setAttribute(Qt::WA_AcceptTouchEvents, true);


    ret=system("ifconfig eth0 |grep 'inet addr' |cut -c 21-34 > ip.txt");
    ret=system("echo \"-----\" > text.txt");

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));

    MainTask->start(100);

    // system("./mute.sh");
}

MainWindow::~MainWindow()
{
    delete ui;
}


bool MainWindow::event(QEvent* event)
{
//    char aTmp[100];
//    sprintf(aTmp,"echo Event = %03X ",event->type());
//    system(aTmp);

    switch (event->type())
    {
        case QEvent::TouchBegin:
            inactivity  = 0;
            State       = 1;
           // system("echo 255 > /sys/class/backlight/lcd_backlight/brightness");
            break;
        default:
            break;
            // call base implementation
    }
   return QMainWindow::event(event);
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}




int loop=0;

int read_ip=0;
void MainWindow::MainTask_loop()
{
    QString res = "reset";
    int sec,min,hour,days;
    //int len;
    //unsigned char c;
    char aBuf[100];

    //=============================================================================
    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime   = current.toString("hh:mm:ss");
    QString TheDate   = current.toString("dd/MM/yyyy");

    QString CountDown;

    ui->time_text->setText(TheTime);
    ui->time_date->setText(TheDate);

    loop++;
    //=============================================================================

    //=============================================================================
    sec = loop/10;
    min = sec/60;
    hour = min/60;
    days = hour/24;

 //   Label2.sprintf("(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
 //   ui->Counter->setText(Label2);
     sprintf(aBuf,"(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
     ui->Counter->setText(aBuf);

    //=============================================================================





    inactivity++;


    if(inactivity > 200 && State==1 )
    {
        inactivity = 0;
        State=0;
        ui->pb_main->setStyleSheet    ( "border:none;background-image: url(:/bmp/Demo_Main_PM.bmp) 0 0 0 0 stretch stretch;" );
        CurrentScreen = ABOUT_SCREEN ;

        all_buttons_off();
        /*
        ui->pb_abt3000->setStyleSheet ( "background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_mv3000->setStyleSheet  ( "background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_mg3000->setStyleSheet  ( "background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_emv3000->setStyleSheet ( "background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_dmt2000->setStyleSheet ( "background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        ui->pb_pv3000->setStyleSheet  ( "background-image: url(:/bmp/PV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
        */
    }


    if(State==1 )
    {
        //CountDown.sprintf("Count     %02d",30 - (inactivity/10));
        //CountDown.sprintf("%02d",30 - (inactivity/10));
        //ui->label_3->setText(CountDown);
        sprintf(aBuf,"%02d",20 - (inactivity/10));
        ui->label_3->setText(aBuf);
    }


  //  Label2.sprintf("%09d",loop/10);
   // ui->Counter->setText(Label2);




    //=============================================================================
    if( read_ip == 0)
    {
        read("ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
    }
    //=============================================================================
    if( loop%10==3)
    {
            ret=system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    }
    //=============================================================================
    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
    }
    //=============================================================================


    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}


void MainWindow::all_buttons_off()
{
    ui->pb_abt3000->setStyleSheet ( "border:none;background-image: url(:/bmp/ABT3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mv3000->setStyleSheet  ( "border:none;background-image: url(:/bmp/MV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_mg3000->setStyleSheet  ( "border:none;background-image: url(:/bmp/MG3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_emv3000->setStyleSheet ( "border:none;background-image: url(:/bmp/EMV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_dmt2000->setStyleSheet ( "border:none;background-image: url(:/bmp/DMT2000_OFF.bmp) 0 0 0 0 stretch stretch;" );
    ui->pb_pv3000->setStyleSheet  ( "border:none;background-image: url(:/bmp/PV3000_OFF.bmp) 0 0 0 0 stretch stretch;" );
}


//========================================================================================
void MainWindow::on_pb_abt3000_clicked()
{
    ui->pb_main->setStyleSheet ( "border:none;background-image: url(:/bmp/ABT3000.bmp) 0 0 0 0 stretch stretch;" );
    all_buttons_off();
    ui->pb_abt3000->setStyleSheet ( "border:none;background-image: url(:/bmp/ABT3000_ON.bmp) 0 0 0 0 stretch stretch;" );

    inactivity=0;
    State=1;
    ret=system("beep &");
}
//========================================================================================
void MainWindow::on_pb_mv3000_clicked()
{
    ui->pb_main->setStyleSheet ( "border:none;background-image: url(:/bmp/MV3000.bmp) 0 0 0 0 stretch stretch;" );
    all_buttons_off();
    ui->pb_mv3000->setStyleSheet  ( "border:none;background-image: url(:/bmp/MV3000_ON.bmp) 0 0 0 0 stretch stretch;" );

    inactivity=0;
    State=1;
    ret=system("beep &");
}
//========================================================================================
void MainWindow::on_pb_mg3000_clicked()
{
   ui->pb_main->setStyleSheet ( "border:none;background-image: url(:/bmp/MG3000.bmp) 0 0 0 0 stretch stretch;" );
   all_buttons_off();
   ui->pb_mg3000->setStyleSheet  ( "border:none;background-image: url(:/bmp/MG3000_ON.bmp) 0 0 0 0 stretch stretch;" );

   inactivity=0;
   State=1;
   ret=system("beep &");
}

void MainWindow::on_pb_emv3000_clicked()
{
    ui->pb_main->setStyleSheet ( "border:none;background-image: url(:/bmp/EMV3000.bmp) 0 0 0 0 stretch stretch;" );

    all_buttons_off();
    ui->pb_emv3000->setStyleSheet ( "border:none;background-image: url(:/bmp/EMV3000_ON.bmp) 0 0 0 0 stretch stretch;" );

    inactivity=0;
    State=1;
    ret=system("beep &");
}
void MainWindow::on_pb_dmt2000_clicked()
{
     ui->pb_main->setStyleSheet ( "border:none;background-image: url(:/bmp/DMT2000.bmp) 0 0 0 0 stretch stretch;" );

     all_buttons_off();

     ui->pb_dmt2000->setStyleSheet ( "border:none;background-image: url(:/bmp/DMT2000_ON.bmp) 0 0 0 0 stretch stretch;" );

     inactivity=0;
     State=1;
     ret=system("beep &");
}


void MainWindow::on_pb_pv3000_clicked()
{
    ui->pb_main->setStyleSheet ( "border:none;background-image: url(:/bmp/PV3000.bmp) 0 0 0 0 stretch stretch;" );

    all_buttons_off();
    ui->pb_pv3000->setStyleSheet  ( "border:none;background-image: url(:/bmp/PV3000_ON.bmp) 0 0 0 0 stretch stretch;" );

   inactivity=0;
   State=1;
   ret=system("beep &");

}
