#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTouchEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void all_buttons_off();


private slots:
    void MasterTimer_update();
    void MainTask_loop();

    void on_pb_abt3000_clicked();
    void on_pb_mv3000_clicked();
    void on_pb_mg3000_clicked();
    void on_pb_emv3000_clicked();
    void on_pb_dmt2000_clicked();
    void on_pb_pv3000_clicked();

private:
    Ui::MainWindow *ui;

protected:
   // void touchEvent(QTouchEvent *event);
    bool event(QEvent* event);

};

#endif // MAINWINDOW_H
