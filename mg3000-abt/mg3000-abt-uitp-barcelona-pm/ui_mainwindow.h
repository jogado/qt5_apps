/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *SplashScreen;
    QPushButton *pushButton_2;
    QWidget *MainScreen;
    QLabel *Counter;
    QLabel *ip_address;
    QLabel *free_text;
    QPushButton *pushButton;
    QLabel *AdultCount;
    QLabel *TheDate;
    QLabel *TheTime;
    QPushButton *pb_InvalidCard;
    QPushButton *pb_ValidCard;
    QPushButton *pb_waiting;
    QWidget *Service;
    QPushButton *pb_barcode;
    QPushButton *pb_reboot;
    QPushButton *pb_header;
    QPushButton *pb_moti;
    QLabel *Counter_2;
    QPushButton *pb_open;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(320, 240);
        MainWindow->setStyleSheet(QStringLiteral("color:black;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 320, 240));
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QStringLiteral(""));
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QStringLiteral("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QStringLiteral("image:url(:/bmp/IT-Trans.bmp)"));
        pushButton_2 = new QPushButton(SplashScreen);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setEnabled(false);
        pushButton_2->setGeometry(QRect(0, 0, 320, 240));
        pushButton_2->setStyleSheet(QLatin1String("border: none;\n"
"background-image: url(:/bmp/main.bmp);"));
        stackedWidget->addWidget(SplashScreen);
        MainScreen = new QWidget();
        MainScreen->setObjectName(QStringLiteral("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QStringLiteral(""));
        Counter = new QLabel(MainScreen);
        Counter->setObjectName(QStringLiteral("Counter"));
        Counter->setEnabled(true);
        Counter->setGeometry(QRect(214, 13, 100, 10));
        QFont font;
        font.setFamily(QStringLiteral("Liberation Sans"));
        font.setPointSize(8);
        font.setBold(true);
        font.setItalic(false);
        font.setWeight(75);
        Counter->setFont(font);
        Counter->setStyleSheet(QLatin1String("color: rgb(29, 60, 118);\n"
"\n"
""));
        Counter->setLineWidth(2);
        Counter->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        ip_address = new QLabel(MainScreen);
        ip_address->setObjectName(QStringLiteral("ip_address"));
        ip_address->setGeometry(QRect(211, 3, 100, 10));
        ip_address->setFont(font);
        ip_address->setStyleSheet(QStringLiteral("color: rgb(29, 60, 118);"));
        ip_address->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        free_text = new QLabel(MainScreen);
        free_text->setObjectName(QStringLiteral("free_text"));
        free_text->setGeometry(QRect(6, 162, 310, 20));
        QFont font1;
        font1.setFamily(QStringLiteral("Liberation Sans"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        free_text->setFont(font1);
        free_text->setStyleSheet(QLatin1String("color:white;\n"
""));
        free_text->setLineWidth(2);
        free_text->setAlignment(Qt::AlignCenter);
        free_text->setWordWrap(true);
        pushButton = new QPushButton(MainScreen);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setEnabled(false);
        pushButton->setGeometry(QRect(0, 0, 320, 240));
        pushButton->setStyleSheet(QLatin1String("border: none;\n"
"background-image: url(:/bmp/main.bmp);"));
        AdultCount = new QLabel(MainScreen);
        AdultCount->setObjectName(QStringLiteral("AdultCount"));
        AdultCount->setEnabled(true);
        AdultCount->setGeometry(QRect(187, 81, 41, 31));
        QFont font2;
        font2.setFamily(QStringLiteral("Liberation Sans"));
        font2.setPointSize(20);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setWeight(75);
        AdultCount->setFont(font2);
        AdultCount->setStyleSheet(QLatin1String("color:white;\n"
"\n"
""));
        AdultCount->setLineWidth(2);
        AdultCount->setAlignment(Qt::AlignCenter);
        TheDate = new QLabel(MainScreen);
        TheDate->setObjectName(QStringLiteral("TheDate"));
        TheDate->setEnabled(true);
        TheDate->setGeometry(QRect(370, 14, 100, 20));
        QFont font3;
        font3.setFamily(QStringLiteral("Liberation Sans"));
        font3.setPointSize(12);
        font3.setItalic(false);
        TheDate->setFont(font3);
        TheDate->setStyleSheet(QLatin1String("color:white;\n"
""));
        TheDate->setLineWidth(2);
        TheDate->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        TheTime = new QLabel(MainScreen);
        TheTime->setObjectName(QStringLiteral("TheTime"));
        TheTime->setEnabled(true);
        TheTime->setGeometry(QRect(370, 36, 100, 20));
        TheTime->setFont(font3);
        TheTime->setStyleSheet(QLatin1String("color:white\n"
"\n"
""));
        TheTime->setLineWidth(2);
        TheTime->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        pb_InvalidCard = new QPushButton(MainScreen);
        pb_InvalidCard->setObjectName(QStringLiteral("pb_InvalidCard"));
        pb_InvalidCard->setGeometry(QRect(5, 187, 311, 48));
        pb_InvalidCard->setStyleSheet(QLatin1String("background-image: url(:/bmp/invalid.bmp);\n"
"border: none;"));
        pb_ValidCard = new QPushButton(MainScreen);
        pb_ValidCard->setObjectName(QStringLiteral("pb_ValidCard"));
        pb_ValidCard->setGeometry(QRect(5, 187, 311, 48));
        pb_ValidCard->setStyleSheet(QLatin1String("background-image: url(:/bmp/valid.bmp);\n"
"border: none;"));
        pb_waiting = new QPushButton(MainScreen);
        pb_waiting->setObjectName(QStringLiteral("pb_waiting"));
        pb_waiting->setGeometry(QRect(5, 187, 311, 48));
        pb_waiting->setStyleSheet(QLatin1String("background-image: url(:/bmp/arrow.bmp);\n"
"border: none;"));
        stackedWidget->addWidget(MainScreen);
        pushButton->raise();
        Counter->raise();
        free_text->raise();
        AdultCount->raise();
        TheDate->raise();
        TheTime->raise();
        ip_address->raise();
        pb_ValidCard->raise();
        pb_InvalidCard->raise();
        pb_waiting->raise();
        Service = new QWidget();
        Service->setObjectName(QStringLiteral("Service"));
        pb_barcode = new QPushButton(Service);
        pb_barcode->setObjectName(QStringLiteral("pb_barcode"));
        pb_barcode->setGeometry(QRect(10, 100, 300, 40));
        QFont font4;
        font4.setFamily(QStringLiteral("Liberation Sans"));
        font4.setPointSize(18);
        font4.setBold(true);
        font4.setWeight(75);
        pb_barcode->setFont(font4);
        pb_barcode->setStyleSheet(QLatin1String("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_reboot = new QPushButton(Service);
        pb_reboot->setObjectName(QStringLiteral("pb_reboot"));
        pb_reboot->setGeometry(QRect(10, 198, 300, 40));
        pb_reboot->setFont(font4);
        pb_reboot->setStyleSheet(QLatin1String("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_header = new QPushButton(Service);
        pb_header->setObjectName(QStringLiteral("pb_header"));
        pb_header->setGeometry(QRect(10, 5, 300, 40));
        pb_header->setFont(font4);
        pb_header->setStyleSheet(QLatin1String("background-color: rgb(20, 60, 118);\n"
"border: none;\n"
"color:white;\n"
"\n"
""));
        pb_moti = new QPushButton(Service);
        pb_moti->setObjectName(QStringLiteral("pb_moti"));
        pb_moti->setGeometry(QRect(10, 51, 300, 40));
        pb_moti->setFont(font4);
        pb_moti->setStyleSheet(QLatin1String("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_moti->setFlat(false);
        Counter_2 = new QLabel(Service);
        Counter_2->setObjectName(QStringLiteral("Counter_2"));
        Counter_2->setEnabled(true);
        Counter_2->setGeometry(QRect(275, 25, 30, 20));
        QFont font5;
        font5.setFamily(QStringLiteral("Liberation Sans"));
        font5.setPointSize(10);
        font5.setItalic(false);
        Counter_2->setFont(font5);
        Counter_2->setStyleSheet(QLatin1String("color:white;\n"
"\n"
""));
        Counter_2->setLineWidth(2);
        Counter_2->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        pb_open = new QPushButton(Service);
        pb_open->setObjectName(QStringLiteral("pb_open"));
        pb_open->setGeometry(QRect(10, 150, 300, 40));
        pb_open->setFont(font4);
        pb_open->setStyleSheet(QLatin1String("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        stackedWidget->addWidget(Service);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        pushButton_2->setText(QString());
        Counter->setText(QApplication::translate("MainWindow", "0", nullptr));
        ip_address->setText(QApplication::translate("MainWindow", "000.000.000.000", nullptr));
        free_text->setText(QApplication::translate("MainWindow", "---", nullptr));
        pushButton->setText(QString());
        AdultCount->setText(QApplication::translate("MainWindow", "0", nullptr));
        TheDate->setText(QApplication::translate("MainWindow", "0", nullptr));
        TheTime->setText(QApplication::translate("MainWindow", "0", nullptr));
        pb_InvalidCard->setText(QString());
        pb_ValidCard->setText(QString());
        pb_waiting->setText(QString());
        pb_barcode->setText(QApplication::translate("MainWindow", "Restart Barcode", nullptr));
        pb_reboot->setText(QApplication::translate("MainWindow", "Reboot Device", nullptr));
        pb_header->setText(QApplication::translate("MainWindow", "SERVICE SCREEN", nullptr));
        pb_moti->setText(QApplication::translate("MainWindow", "Restart Moti", nullptr));
        Counter_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        pb_open->setText(QApplication::translate("MainWindow", "Open Door", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
