/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *MainScreen;
    QLabel *label;
    QPushButton *pb_mouse;
    QPushButton *pb_cat;
    QPushButton *pb_lion;
    QPushButton *pb_dragon;
    QWidget *SplashScreen;
    QPushButton *pb_red;
    QPushButton *pb_green;
    QPushButton *pb_blue;
    QPushButton *pb_yellow;
    QPushButton *pb_background;
    QPushButton *pb_center;
    QPushButton *pb_start;
    QPushButton *pb_up;
    QPushButton *pb_down;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(480, 640);
        MainWindow->setStyleSheet(QString::fromUtf8("color:black;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 480, 640));
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QString::fromUtf8(""));
        MainScreen = new QWidget();
        MainScreen->setObjectName(QString::fromUtf8("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QString::fromUtf8("background-color:black"));
        label = new QLabel(MainScreen);
        label->setObjectName(QString::fromUtf8("label"));
        label->setEnabled(true);
        label->setGeometry(QRect(0, 19, 480, 81));
        QFont font;
        font.setFamily(QString::fromUtf8("Liberation Serif"));
        font.setPointSize(30);
        font.setBold(true);
        font.setItalic(false);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("color:RED;"));
        label->setLineWidth(2);
        label->setAlignment(Qt::AlignCenter);
        pb_mouse = new QPushButton(MainScreen);
        pb_mouse->setObjectName(QString::fromUtf8("pb_mouse"));
        pb_mouse->setGeometry(QRect(20, 140, 440, 80));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Liberation Serif"));
        font1.setPointSize(30);
        font1.setBold(true);
        font1.setWeight(75);
        pb_mouse->setFont(font1);
        pb_mouse->setStyleSheet(QString::fromUtf8("color:Yellow;\n"
"background-color: black;\n"
"border:1px solid #FF0000;\n"
""));
        pb_cat = new QPushButton(MainScreen);
        pb_cat->setObjectName(QString::fromUtf8("pb_cat"));
        pb_cat->setGeometry(QRect(20, 240, 440, 80));
        pb_cat->setFont(font1);
        pb_cat->setStyleSheet(QString::fromUtf8("color:Yellow;\n"
"background-color: black;\n"
"border:1px solid #FF0000;\n"
""));
        pb_lion = new QPushButton(MainScreen);
        pb_lion->setObjectName(QString::fromUtf8("pb_lion"));
        pb_lion->setGeometry(QRect(20, 340, 440, 80));
        pb_lion->setFont(font1);
        pb_lion->setStyleSheet(QString::fromUtf8("color:Yellow;\n"
"background-color: black;\n"
"border:1px solid #FF0000;\n"
""));
        pb_dragon = new QPushButton(MainScreen);
        pb_dragon->setObjectName(QString::fromUtf8("pb_dragon"));
        pb_dragon->setGeometry(QRect(20, 440, 440, 80));
        pb_dragon->setFont(font1);
        pb_dragon->setStyleSheet(QString::fromUtf8("color:Yellow;\n"
"background-color: black;\n"
"border:1px solid #FF0000;\n"
""));
        stackedWidget->addWidget(MainScreen);
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QString::fromUtf8("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QString::fromUtf8(""));
        pb_red = new QPushButton(SplashScreen);
        pb_red->setObjectName(QString::fromUtf8("pb_red"));
        pb_red->setGeometry(QRect(10, 90, 225, 225));
        pb_red->setStyleSheet(QString::fromUtf8("background-color:  rgb(150,0,0);"));
        pb_green = new QPushButton(SplashScreen);
        pb_green->setObjectName(QString::fromUtf8("pb_green"));
        pb_green->setGeometry(QRect(245, 90, 225, 225));
        pb_green->setStyleSheet(QString::fromUtf8("background-color:green"));
        pb_blue = new QPushButton(SplashScreen);
        pb_blue->setObjectName(QString::fromUtf8("pb_blue"));
        pb_blue->setGeometry(QRect(10, 325, 225, 225));
        pb_blue->setStyleSheet(QString::fromUtf8("background-color:rgb(21, 10, 124)\n"
""));
        pb_yellow = new QPushButton(SplashScreen);
        pb_yellow->setObjectName(QString::fromUtf8("pb_yellow"));
        pb_yellow->setGeometry(QRect(245, 325, 225, 225));
        pb_yellow->setStyleSheet(QString::fromUtf8("background-color:yellow\n"
""));
        pb_background = new QPushButton(SplashScreen);
        pb_background->setObjectName(QString::fromUtf8("pb_background"));
        pb_background->setGeometry(QRect(0, 0, 480, 640));
        pb_background->setStyleSheet(QString::fromUtf8("background-color: white\n"
""));
        pb_center = new QPushButton(SplashScreen);
        pb_center->setObjectName(QString::fromUtf8("pb_center"));
        pb_center->setGeometry(QRect(128, 208, 225, 225));
        pb_center->setFont(font1);
        pb_center->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_center->setStyleSheet(QString::fromUtf8("background-color: rgb(208, 250, 247);\n"
"border:10px solid #FFFFFF;\n"
"background-color: rgb(117, 80, 123);\n"
"border-radius:112px;\n"
"color:white"));
        pb_start = new QPushButton(SplashScreen);
        pb_start->setObjectName(QString::fromUtf8("pb_start"));
        pb_start->setGeometry(QRect(128, 208, 225, 225));
        pb_start->setFont(font1);
        pb_start->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_start->setStyleSheet(QString::fromUtf8("background-color: rgb(255,0,0);\n"
"border:10px solid #FFFFFF;\n"
"border-radius:112px;\n"
"color:white"));
        pb_up = new QPushButton(SplashScreen);
        pb_up->setObjectName(QString::fromUtf8("pb_up"));
        pb_up->setGeometry(QRect(10, 10, 460, 70));
        pb_up->setStyleSheet(QString::fromUtf8("background-color:rgb(193, 125, 17)"));
        pb_down = new QPushButton(SplashScreen);
        pb_down->setObjectName(QString::fromUtf8("pb_down"));
        pb_down->setGeometry(QRect(10, 560, 460, 70));
        pb_down->setStyleSheet(QString::fromUtf8("background-color: rgb(162, 220, 88);"));
        stackedWidget->addWidget(SplashScreen);
        pb_background->raise();
        pb_red->raise();
        pb_green->raise();
        pb_blue->raise();
        pb_yellow->raise();
        pb_center->raise();
        pb_start->raise();
        pb_up->raise();
        pb_down->raise();
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "Ooh, you are a ...", nullptr));
        pb_mouse->setText(QApplication::translate("MainWindow", "Mouse", nullptr));
        pb_cat->setText(QApplication::translate("MainWindow", "Cat", nullptr));
        pb_lion->setText(QApplication::translate("MainWindow", "Lion", nullptr));
        pb_dragon->setText(QApplication::translate("MainWindow", "Dragon", nullptr));
        pb_red->setText(QString());
        pb_green->setText(QString());
        pb_blue->setText(QString());
        pb_yellow->setText(QString());
        pb_background->setText(QString());
        pb_center->setText(QString());
        pb_start->setText(QApplication::translate("MainWindow", "Start", nullptr));
        pb_up->setText(QString());
        pb_down->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
