#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>
#include <QRandomGenerator>
#include <QCommonStyle>

typedef enum {
    STATE_INIT = 1 ,
    STATE_WAIT_START   ,
    STATE_PROFILE   ,
    STATE_SHOW_SCREEN_1 ,
    STATE_SHOW_SCREEN_2 ,
    STATE_PREPARE_SCREEN  ,
    STATE_SHOW_ROUND,
    STATE_PLAY_ROUND,
    STATE_SHOW    ,
    STATE_ROUND    ,
    STATE_SHOW_YOUR_TURN,
    STATE_PLAY    ,
    STATE_GOOD_NEXT_ROUND,
    STATE_GAME_OVER,
    STATE_WINNER,
    STATE_THE_WALL,

} STATES;



#define MAIN_SCREEN       0
#define SPLASH_SCREEN     1
#define NAME_SCREEN       3
#define FAME_SCREEN       2

int LED_1=1;
int LED_2=1;
int LED_3=1;
int LED_4=1;
int LED_5=1;
int LED_6=1;
int LED_7=1;

int ret=0;

int MOUSE=0;
int CAT=0;
int LION=0;
int DRAGON=0;

char aName[]="*ABCDEFGHIJKLMNOPQRSTUVWXYZ-_.";
int name_1=0;
int name_2=0;
int name_3=0;


QString aScore;

int len;

QDateTime current;
QString TheTime;
QString TheDate;




int DATA_LIVE_TIME=10;
int SONG_SHOW   = 0 ;
int SONG_ROUND  = 0 ;
int SONG_STEP   = 0 ;
int PLAY_STEP  = 0;
char aNote[100];
int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label1;
QString Label2;
QString Label3;
int timer_simon=0;
#define T_NOTE 3

int Sequence[100+1];



void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite | QFile::Text))
    {
        file.close();
        return;
    }
    aScore.clear();
    aScore = in.readAll();
    file.close();
}



//=====================================================================================================================
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;

    ui->pb_red->setVisible(false);
    ui->pb_green->setVisible(false);
    ui->pb_blue->setVisible(false);
    ui->pb_yellow->setVisible(false);
    ui->pb_center->setVisible(false);

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

}
//=====================================================================================================================
MainWindow::~MainWindow()
{
    delete ui;
}
//=====================================================================================================================




int loop=0;
int refresh=0;
char aBuf[256];
const char *aBuf2;
int BeepIsOn=1;
int init_done=0;
int Timeout=50;




int STATE = STATE_INIT;

char aTmp[100];

int PLAY_TIMER = 0;



int SLEEP_TIMEOUT=0;
int TIMER_1=0;
int TIMER_2=0;
int TIMER_3=0;
int TIMER_4=0;
int TIMER_5=0;




void MainWindow::GenerateSong()
{
    int i;
    int old_val;
    int val;
    int low=1;
    int high=5;

    if(MOUSE==1)
    {
        high=4;
    }

    if(DRAGON==1)
    {
        high=7;
    }


    i=0;
    old_val=-1;
    while ( i < 50 )
    {
        val = QRandomGenerator::global()->generate() % ((high + 1) - low) + low ;
        if(val == old_val) continue;
        old_val=val;

        Sequence[i] = val;
        i++;
    }

    aTmp[0]=0;
    sprintf(aTmp,"echo '");
    for ( i = 0 ; i < 30 ; i ++)
    {
       sprintf(&aTmp[strlen(aTmp)],"%d ",Sequence[i]);
    }
    sprintf(&aTmp[strlen(aTmp)],"'");
    system(aTmp);

    Sequence[50]=0;

}



void MainWindow::ShowState()
{
        /*
    switch(STATE)
    {
        case STATE_INIT         : ret=system("echo 'State : STATE_INIT' "       );   break;
        case STATE_WAIT_START   : ret=system("echo 'State : STATE_WAIT_START' " );   break;
        case STATE_SHOW         : ret=system("echo 'State : STATE_SHOW' "       );   break;
        case STATE_ROUND        : ret=system("echo 'State : STATE_ROUND' "      );   break;
        case STATE_PLAY         : ret=system("echo 'State : STATE_PLAY' "       );   break;
        default                 : ret=system("echo 'State : default' "          );   break;
    }
    */
}


int party_time=0;


void MainWindow::MainTask_loop()
{

    //-----------------------------------------
    loop++;             // 100 ms steps
    if(STATE==STATE_PLAY) party_time++;
    //-----------------------------------------

    //-----------------------------------------
    if(TIMER_1) TIMER_1 --;
    if(TIMER_2) TIMER_2 --;
    if(TIMER_3) TIMER_3 --;
    if(TIMER_4) TIMER_4 --;
    if(TIMER_5) TIMER_5 --;
    //-----------------------------------------


    //-----------------------------------------
    if( SLEEP_TIMEOUT )
    {
        SLEEP_TIMEOUT--;
        QCoreApplication::processEvents();
        return;
    }
    //-----------------------------------------



   if( PLAY_TIMER )
   {
        PLAY_TIMER--;

        if(PLAY_TIMER==0)
        {
            STATE = STATE_INIT;
        }
   }


   //if(loop%10 != 5) return;




    switch(STATE)
    {
        case STATE_INIT:
                    ShowState();
                    GenerateSong();
                    Simon_Start();

                    STATE=STATE_WAIT_START;

                    sprintf(aTmp," ");
                    //ui->pb_message->setText(aTmp);
                    //ui->info_round->setText(aTmp);

                    return;

        case STATE_WAIT_START:
                    sprintf(aTmp,"START");
                    ui->pb_start->setText(aTmp);

                    if(loop%20<10)
                    {
                        ui->pb_start->setStyleSheet("background-color: rgb(255,0,0);\nborder:10px solid #FFFFFF;\nborder-radius:112px;\ncolor:white");
                    }
                    else
                    {
                        ui->pb_start->setStyleSheet("background-color: rgb(255,0,0);\nborder:10px solid #FFFFFF;\nborder-radius:112px;\ncolor:rgb(255,0,0)");
                    }
                    return;

        case STATE_PROFILE:
                    return;



        case  STATE_SHOW_SCREEN_1:
                    ui->pb_background->setStyleSheet("background-color:white");
                    ui->pb_red->setStyleSheet("background-color:rgb(255,0,0);");
                    ui->pb_green->setStyleSheet("background-color:rgb(0,255,0);");
                    ui->pb_blue->setStyleSheet("background-color:rgb(0,0,255);");
                    ui->pb_yellow->setStyleSheet("background-color:rgb(255,255,0);");
                    ui->pb_center->setStyleSheet("border:10px solid #FFFFFF;background-color: rgb(117, 80, 123);\nborder-radius:112px;color:black");
                    ui->pb_red->setVisible(true);
                    ui->pb_green->setVisible(true);
                    ui->pb_blue->setVisible(true);
                    ui->pb_yellow->setVisible(true);
                    ui->pb_center->setVisible(true);
                    QCoreApplication::processEvents();

                    //ui->pb_message->setVisible(true);
                    //ui->info_round->setVisible(true);

                    //SLEEP_TIMEOUT=5;
                    STATE=STATE_SHOW_SCREEN_2;
                    return;

        case  STATE_SHOW_SCREEN_2:
                    ui->pb_red->setStyleSheet   ("background-color:black;");
                    ui->pb_green->setStyleSheet ("background-color:black;");
                    ui->pb_blue->setStyleSheet  ("background-color:black;");
                    ui->pb_yellow->setStyleSheet("background-color:black;");
                    ui->pb_center->setStyleSheet("border:10px solid #FFFFFF;background-color:black;\nborder-radius:112px;color:white");
                    QCoreApplication::processEvents();
                    //SLEEP_TIMEOUT=5;
                    STATE=STATE_PREPARE_SCREEN;
                    return;


        case  STATE_PREPARE_SCREEN:
                    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
                    CurrentScreen = SPLASH_SCREEN;

                    GenerateSong();
                    SONG_ROUND    = 0;
                    SLEEP_TIMEOUT = 5;
                    STATE=STATE_SHOW_ROUND;
                    return;


        case  STATE_SHOW_ROUND:
                    if( LION==1 || DRAGON==1) GenerateSong();

                    ui->pb_background->setStyleSheet("background-color:black");
                    ui->pb_red->setVisible(true);
                    ui->pb_green->setVisible(true);
                    ui->pb_blue->setVisible(true);
                    ui->pb_yellow->setVisible(true);
                    ui->pb_center->setVisible(true);
                    ui->pb_up->setVisible(true);
                    ui->pb_down->setVisible(true);


                    SONG_ROUND++ ;
                    Clear_Leds();
                    sprintf(aTmp,"echo 'STATE_SHOW_ROUND\n\r' ");
                    ret=system(aTmp);

                    sprintf(aTmp,"LEVEL %d",SONG_ROUND);
                    ui->pb_start->setStyleSheet("background-color:green;color:white;\nborder-radius:112px");
                    ui->pb_start->setText(aTmp);
                    ui->pb_start->setVisible(true);

                    QCoreApplication::processEvents();
                    SONG_STEP=0;
                    SLEEP_TIMEOUT=10;
                    STATE=STATE_PLAY_ROUND;
                    return;

        case  STATE_PLAY_ROUND:
                    ui->pb_start->setVisible(false);

                    ui->pb_background->setStyleSheet("background-color:black");

                    ui->pb_red->setVisible(true);
                    ui->pb_green->setVisible(true);
                    ui->pb_blue->setVisible(true);
                    ui->pb_yellow->setVisible(true);
                    ui->pb_center->setVisible(true);
                    //ui->pb_message->setVisible(true);
                    //ui->info_round->setVisible(true);

                    ui->pb_red->setStyleSheet   ("background-color:black;");
                    ui->pb_green->setStyleSheet ("background-color:black;");
                    ui->pb_blue->setStyleSheet  ("background-color:black;");
                    ui->pb_yellow->setStyleSheet("background-color:black;");
                    ui->pb_center->setStyleSheet("background-color:black;\nborder-radius:112px;color:white");


                    PlayRound();
                    return;


        case STATE_SHOW_YOUR_TURN:
                Clear_Leds();
                  ui->pb_background->setStyleSheet("background-color:white");
                  ui->pb_center->setStyleSheet("border:10px solid #FFFFFF;background-color:black;\nborder-radius:112px;color:white");

                /*
                sprintf(aTmp,"YOUR TURN");
                ui->info_round->setText(aTmp);
                */
                //ui->pb_background->setStyleSheet("background-color:black");

                //sprintf(aTmp,"YOUR\nTURN");
                //ui->pb_start->setStyleSheet("background-color:green;color:white;\nborder-radius:112px");
                //ui->pb_start->setText(aTmp);
                //ui->pb_start->setVisible(true);


                SLEEP_TIMEOUT=5;
                PLAY_STEP   = 0;
                STATE=STATE_PLAY;
                sprintf(aTmp,"echo 'STATE_PLAY\n\r' ");
                ret=system(aTmp);
                return;

        case STATE_PLAY:
                //ui->pb_background->setStyleSheet("background-color:white");
                ui->pb_start->setVisible(false);
                return;
        case STATE_GOOD_NEXT_ROUND:
                Clear_Leds();
                //sprintf(aTmp,"GOOD! , Next level");
                //ui->info_round->setText(aTmp);
                SLEEP_TIMEOUT=2;
                STATE=STATE_SHOW_ROUND;
                return;

        case STATE_GAME_OVER:
                ui->pb_start->setStyleSheet("background-color:red;color:white;\nborder-radius:112px");
                sprintf(aTmp,"GAME\nOVER");
                ui->pb_start->setText(aTmp);
                ui->pb_start->setVisible(true);
                SLEEP_TIMEOUT=20;
                STATE=STATE_WINNER;
                return;

        case STATE_WINNER:
                if(CurrentScreen != NAME_SCREEN )
                {

                    sprintf(aTmp,"%c",aName[name_1]);
                    ui->pb_val_1->setText(aTmp);

                    sprintf(aTmp,"%c",aName[name_2]);
                    ui->pb_val_2->setText(aTmp);

                    sprintf(aTmp,"%c",aName[name_3]);
                    ui->pb_val_3->setText(aTmp);

                    ui->stackedWidget->setCurrentIndex(NAME_SCREEN);
                    CurrentScreen = NAME_SCREEN;
                    QCoreApplication::processEvents();





                }
                 return;
            case STATE_THE_WALL:
                if(TIMER_1==0)
                {
                    ui->stackedWidget->setCurrentIndex(FAME_SCREEN);
                    CurrentScreen = FAME_SCREEN;
                    STATE=STATE_THE_WALL;

                    read("/home/root/score.txt");
                    len=aScore.length();
                    QCoreApplication::processEvents();

                    sprintf(aTmp,"echo 'len=%03d\n' ",len);
                    ret=system(aTmp);

                    ui->label_wall->setText(aScore);
                    QCoreApplication::processEvents();
                    TIMER_1=300;
                    return;
                }
                if(TIMER_1 == 1)
                {
                    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
                    CurrentScreen = SPLASH_SCREEN;
                    STATE=STATE_INIT;
                    return;
                }

        return;

            default:
                ShowState();
                break;
    }

    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}
//=============================================================================



#define n_DO             2093

#define n_DO_diese       2217
#define n_RE_bemol       2217

#define n_RE             2349

#define n_RE_diese       2489
#define n_MI_bemol       2489

#define n_MI             2637

#define n_FA             2794

#define n_FA_diese       2960
#define n_SOL_bemol      2960

#define n_SOL            3136

#define n_SOL_diese      3322
#define n_LA_bemol       3322

#define n_LA             3520

#define n_LA_diese       3729
#define n_SI_bemol       3729

#define n_SI             3951



#define n_C      n_DO

#define n_Cd     n_DO_diese
#define n_Db     n_RE_bemol

#define n_D      n_RE

#define n_Dd     n_RE_diese
#define n_Eb     n_MI_bemol

#define n_E      n_MI
#define n_F      n_FA

#define n_Fd      n_FA_diese
#define n_Gb      n_SOL_bemol

#define n_G      n_SOL
#define n_Gd     n_SOL_diese
#define n_Ab     n_LA_bemol


#define n_A      n_LA

#define n_Ad      n_LA_diese
#define n_Bb      n_SI_bemol


#define n_B      n_SI






void MainWindow::Simon_Start()
{
    LED_1=LED_2=LED_3=LED_4=LED_5=0;
    party_time=0;
    ret=system("echo 'Simon_Start()'");

    ui->pb_background->setStyleSheet("background-color:black");

    ui->pb_red->setStyleSheet   ("background-color:rgb(0,0,0);");
    ui->pb_green->setStyleSheet ("background-color:rgb(0,0,0);");
    ui->pb_blue->setStyleSheet  ("background-color:rgb(0,0,0);");
    ui->pb_yellow->setStyleSheet("background-color:rgb(0,0,0);");
    ui->pb_center->setStyleSheet("background-color:rgb(0,0,0);");

    ui->pb_up->setStyleSheet    ("background-color:rgb(0,0,0);");
    ui->pb_down->setStyleSheet  ("background-color:rgb(0,0,0);");

    ui->pb_red->setVisible      (false);
    ui->pb_green->setVisible    (false);
    ui->pb_blue->setVisible     (false);
    ui->pb_yellow->setVisible   (false);
    ui->pb_center->setVisible   (false);
    ui->pb_up->setVisible       (false);
    ui->pb_down->setVisible     (false);

    ui->pb_start->setVisible     (true);

    QCoreApplication::processEvents();
}



void MainWindow::on_pb_red_clicked()
{

    if( STATE != STATE_PLAY )   return;
    if( SLEEP_TIMEOUT )         return;

    sprintf(aTmp,"echo 'on_pb_red_clicked(%d/%d)'",PLAY_STEP,SONG_ROUND);
    ret=system(aTmp);
    QCoreApplication::processEvents();


    Clear_Leds();
    PLAY_TIMER =100;

    //--------------------------------
    LED_1=1;
    //--------------------------------
    sprintf(aNote,"beep -f %d &",n_DO);
    ret=system(aNote);
    //--------------------------------
    ui->pb_red->setStyleSheet("background-color:rgb(255,0,0);");
    timer_simon=T_NOTE;
    //--------------------------------
    if( Sequence[PLAY_STEP]!=1)
    {
         STATE=STATE_GAME_OVER;
         return;
    }
    else
    {
         sprintf(aTmp,"    echo 'STEP[%d]= 1 : OK'",PLAY_STEP);
         ret=system(aTmp);
    }
    //--------------------------------



    PLAY_STEP++;
    PLAY_TIMER =100;

    if(PLAY_STEP >= SONG_ROUND )

    {
       SONG_STEP =0;
       STATE=STATE_GOOD_NEXT_ROUND;
       ret=system("echo OK");
    }
    SLEEP_TIMEOUT=3;

}

void MainWindow::on_pb_green_clicked()
{
    if( STATE != STATE_PLAY )   return;
    if( SLEEP_TIMEOUT )         return;

    sprintf(aTmp,"echo 'on_pb_green_clicked(%d/%d)'",PLAY_STEP,SONG_ROUND);
    ret=system(aTmp);
    QCoreApplication::processEvents();

    Clear_Leds();

    LED_2=1;
    sprintf(aNote,"beep -f %d &",n_RE);
    ret=system(aNote);
    ui->pb_green->setStyleSheet("background-color:rgb(0,255,0);");
    timer_simon=T_NOTE;

    if(STATE == STATE_SHOW) return;

    if( Sequence[PLAY_STEP]!=2)
    {
         STATE=STATE_GAME_OVER;
         return;
    }
    else
    {
         sprintf(aTmp,"    echo 'STEP[%d]= 2 : OK'",PLAY_STEP);
         ret=system(aTmp);
    }


    PLAY_STEP++;
    PLAY_TIMER =100;

    if(PLAY_STEP >= SONG_ROUND)
    {
       SONG_STEP =0;
       STATE=STATE_GOOD_NEXT_ROUND;
       ret=system("echo OK");
    }
    SLEEP_TIMEOUT=3;

}

void MainWindow::on_pb_blue_clicked()
{
    if( STATE != STATE_PLAY )   return;
    if( SLEEP_TIMEOUT )         return;

    sprintf(aTmp,"echo 'on_pb_blueclicked(%d/%d)'",PLAY_STEP,SONG_ROUND);
    ret=system(aTmp);
    QCoreApplication::processEvents();

    Clear_Leds();

    LED_3=1;
    sprintf(aNote,"beep -f %d &",n_MI);
    ret=system(aNote);
    ui->pb_blue->setStyleSheet("background-color:rgb(0,0,255);");
    timer_simon=T_NOTE;

    if(STATE == STATE_SHOW) return;

    if( Sequence[PLAY_STEP]!=3)
    {
         STATE=STATE_GAME_OVER;
         return;
    }
    else
    {
         sprintf(aTmp,"    echo 'STEP[%d]= 3 : OK'",PLAY_STEP);
         ret=system(aTmp);
    }


    PLAY_STEP++;
    PLAY_TIMER =100;

    if(PLAY_STEP >= SONG_ROUND)

    {
       SONG_STEP =0;
       STATE=STATE_GOOD_NEXT_ROUND;
       ret=system("echo OK");
    }
    SLEEP_TIMEOUT=3;

}

void MainWindow::on_pb_yellow_clicked()
{
    if( STATE != STATE_PLAY )   return;
    if( SLEEP_TIMEOUT )         return;

    sprintf(aTmp,"echo 'on_pb_yellow_clicked(%d/%d)'",PLAY_STEP,SONG_ROUND);
    ret=system(aTmp);
    QCoreApplication::processEvents();




    Clear_Leds();

    LED_4=1;
    sprintf(aNote,"beep -f %d &",n_FA);
    ret=system(aNote);
    ui->pb_yellow->setStyleSheet("background-color:rgb(255,255,0);");
    timer_simon=T_NOTE;

    if(STATE == STATE_SHOW) return;

    if( Sequence[PLAY_STEP]!=4)
    {
         STATE=STATE_GAME_OVER;
         return;
    }
    else
    {
         sprintf(aTmp,"echo '    STEP[%d]= 4 : OK'",PLAY_STEP);
         ret=system(aTmp);
    }


    PLAY_STEP++;
    PLAY_TIMER =100;

    if(PLAY_STEP >= SONG_ROUND)
    {
       SONG_STEP =0;
       STATE=STATE_GOOD_NEXT_ROUND;
       ret=system("echo OK");
    }

    SLEEP_TIMEOUT=3;

}



void MainWindow::on_pb_center_clicked()
{
    if( STATE != STATE_PLAY )   return;
    if( SLEEP_TIMEOUT )         return;

    sprintf(aTmp,"echo 'on_pb_center_clicked(%d/%d)'",PLAY_STEP,SONG_ROUND);
    ret=system(aTmp);
    QCoreApplication::processEvents();



    Clear_Leds();

    LED_5=1;
    sprintf(aNote,"beep -f %d &",n_SOL);
    ret=system(aNote);
    ui->pb_center->setStyleSheet("border:10px solid #FFFFFF;\nbackground-color: rgb(117, 80, 123);\nborder-radius:112px;color:white");
    timer_simon=T_NOTE;


    if(STATE == STATE_SHOW) return;


    if( Sequence[PLAY_STEP]!=5)
    {
         STATE=STATE_GAME_OVER;
         return;
    }
    else
    {
         sprintf(aTmp,"echo 'STEP[%d]= 5 : OK'",PLAY_STEP);
         ret=system(aTmp);
    }


    PLAY_STEP++;
    PLAY_TIMER =100;

    if(PLAY_STEP >= SONG_ROUND)
    {
       SONG_STEP =0;
       STATE=STATE_GOOD_NEXT_ROUND;
       ret=system("echo OK");
    }
    SLEEP_TIMEOUT=3;

}



void MainWindow::on_pb_up_clicked()
{
    if( STATE != STATE_PLAY )   return;
    if( SLEEP_TIMEOUT )         return;

    sprintf(aTmp,"echo 'on_pb_up_clicked(%d/%d)'",PLAY_STEP,SONG_ROUND);
    ret=system(aTmp);
    QCoreApplication::processEvents();


    Clear_Leds();

    LED_6=1;
    sprintf(aNote,"beep -f %d &",n_LA);
    ret=system(aNote);
    ui->pb_up->setStyleSheet("background-color: rgb(193, 125, 17)");
    timer_simon=T_NOTE;


    if(STATE == STATE_SHOW) return;


    if( Sequence[PLAY_STEP]!=6)
    {
         STATE=STATE_GAME_OVER;
         return;
    }
    else
    {
         sprintf(aTmp,"echo 'STEP[%d]= 6 : OK'",PLAY_STEP);
         ret=system(aTmp);
    }


    PLAY_STEP++;
    PLAY_TIMER =100;

    if(PLAY_STEP >= SONG_ROUND)
    {
       SONG_STEP =0;
       STATE=STATE_GOOD_NEXT_ROUND;
       ret=system("echo OK");
    }
    SLEEP_TIMEOUT=3;

}

void MainWindow::on_pb_down_clicked()
{
    if( STATE != STATE_PLAY )   return;
    if( SLEEP_TIMEOUT )         return;

    sprintf(aTmp,"echo 'on_pb_center_clicked(%d/%d)'",PLAY_STEP,SONG_ROUND);
    ret=system(aTmp);
    QCoreApplication::processEvents();



    Clear_Leds();

    LED_7=1;
    sprintf(aNote,"beep -f %d &",n_SI);
    ret=system(aNote);
    ui->pb_down->setStyleSheet("background-color: rgb(162, 220, 88);");
    timer_simon=T_NOTE;


    if(STATE == STATE_SHOW) return;


    if( Sequence[PLAY_STEP]!=7)
    {
         STATE=STATE_GAME_OVER;
         return;
    }
    else
    {
         sprintf(aTmp,"echo 'STEP[%d]= 7 : OK'",PLAY_STEP);
         ret=system(aTmp);
    }


    PLAY_STEP++;
    PLAY_TIMER =100;

    if(PLAY_STEP >= SONG_ROUND)
    {
       SONG_STEP =0;
       STATE=STATE_GOOD_NEXT_ROUND;
       ret=system("echo OK");
    }
    SLEEP_TIMEOUT=3;

}















void MainWindow::ShowSong()
{
}

void MainWindow::PlaySong()
{
}



void MainWindow::Clear_Leds()
{
    if( LED_1) ui->pb_red->setStyleSheet   ("background-color:rgb(0,0,0);");
    if( LED_2) ui->pb_green->setStyleSheet ("background-color:rgb(0,0,0);");
    if( LED_3) ui->pb_blue->setStyleSheet  ("background-color:rgb(0,0,0);");
    if( LED_4) ui->pb_yellow->setStyleSheet("background-color:rgb(0,0,0);");
    if( LED_5)
    {
       ui->pb_center->setStyleSheet("border:10px solid #FFFFFF;\nbackground-color: rgb(0,0,0);\nborder-radius:112px;color:white");
    }
    if( LED_6) ui->pb_up->setStyleSheet  ("background-color:rgb(0,0,0);");
    if( LED_7) ui->pb_down->setStyleSheet("background-color:rgb(0,0,0);");
    LED_1=LED_2=LED_3=LED_4=LED_5=LED_6=LED_7=0;
}

void MainWindow::Clear_Leds2()
{
    if( LED_1) ui->pb_red->setStyleSheet   ("background-color:rgb(0,0,0);");
    if( LED_2) ui->pb_green->setStyleSheet ("background-color:rgb(0,0,0);");
    if( LED_3) ui->pb_blue->setStyleSheet  ("background-color:rgb(0,0,0);");
    if( LED_4) ui->pb_yellow->setStyleSheet("background-color:rgb(0,0,0);");
    if( LED_5)
    {
       ui->pb_center->setStyleSheet("background-color: rgb(0,0,0);\nborder-radius:112px;color:white");
    }
    if( LED_6) ui->pb_up->setStyleSheet  ("background-color:rgb(0,0,0);");
    if( LED_7) ui->pb_down->setStyleSheet("background-color:rgb(0,0,0);");
    LED_1=LED_2=LED_3=LED_4=LED_5=LED_6=LED_7=0;
}





void MainWindow::PlayRound()
{
    int note;

    Clear_Leds2();

    note = Sequence[SONG_STEP];

    //sprintf(aTmp,"echo '    PlayRound(%d/%d)' ",SONG_STEP,SONG_ROUND);
    //ret=system(aTmp);


    switch(note)
    {
        case 1: show_red   (); break;
        case 2: show_green (); break;
        case 3: show_blue  (); break;
        case 4: show_yellow(); break;
        case 5: show_center(); break;
        case 6: show_up(); break;
        case 7: show_down(); break;
        default:  STATE=STATE_INIT    ; break;
    }
    QCoreApplication::processEvents();

    SONG_STEP++;

    if(SONG_STEP >= SONG_ROUND )
    {
       //SONG_ROUND++ ;
       SLEEP_TIMEOUT=10;
       STATE=STATE_SHOW_YOUR_TURN;
    }
    else
    {
        if(MOUSE==1)
            SLEEP_TIMEOUT=10;
        else
            SLEEP_TIMEOUT=3;
    }
}





void MainWindow::show_red()
{
    sprintf(aTmp,"echo 'LED_1'");system(aTmp);
    LED_1=1;
    ui->pb_red->setStyleSheet("background-color:rgb(255,0,0);");
    timer_simon=T_NOTE;

    sprintf(aNote,"beep -f %d &",n_DO);
    ret=system(aNote);
}

void MainWindow::show_green()
{
        sprintf(aTmp,"echo 'LED_2'");system(aTmp);
    LED_2=1;

    ui->pb_green->setStyleSheet("background-color:rgb(0,255,0);");
    timer_simon=T_NOTE;

    sprintf(aNote,"beep -f %d &",n_RE);
    ret=system(aNote);
}

void MainWindow::show_blue()
{
        sprintf(aTmp,"echo 'LED_3'");system(aTmp);
    LED_3=1;

    ui->pb_blue->setStyleSheet("background-color:rgb(0,0,255);");
    timer_simon=T_NOTE;

    sprintf(aNote,"beep -f %d &",n_MI);
    ret=system(aNote);
}


void MainWindow::show_yellow()
{
        sprintf(aTmp,"echo 'LED_4'");system(aTmp);
    LED_4=1;
    ui->pb_yellow->setStyleSheet("background-color:rgb(255,255,0);");
    timer_simon=T_NOTE;

    sprintf(aNote,"beep -f %d &",n_FA);
    ret=system(aNote);
}

void MainWindow::show_center()
{
    LED_5=1;

    sprintf(aTmp,"echo 'LED_5'");system(aTmp);

//  ui->pb_center->setStyleSheet("border:10px solid #FFFFFF;\nbackground-color: rgb(117, 80, 123);\nborder-radius:112px;color:white");
    ui->pb_center->setStyleSheet("background-color: rgb(117, 80, 123);\nborder-radius:112px;color:white");
    timer_simon=T_NOTE;

    sprintf(aNote,"beep -f %d &",n_SOL);
    ret=system(aNote);
}


void MainWindow::show_up()
{
    LED_6=1;

    sprintf(aTmp,"echo 'LED_6'");system(aTmp);

    ui->pb_up->setStyleSheet("background-color: rgb(193, 125, 17);\nborder-radius:112px;color:white");

    timer_simon=T_NOTE;

    sprintf(aNote,"beep -f %d &",n_LA);
    ret=system(aNote);
}

void MainWindow::show_down()
{
    LED_7=1;

    sprintf(aTmp,"echo 'LED_7'");system(aTmp);

    ui->pb_down->setStyleSheet("background-color: rgb(162, 220, 88);\nborder-radius:112px;color:white");
    timer_simon=T_NOTE;

    sprintf(aNote,"beep -f %d &",n_SI);
    ret=system(aNote);
}











void MainWindow::on_pb_start_clicked()
{
    ui->pb_start->setVisible     (false);

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;

    SLEEP_TIMEOUT=10;
    STATE = STATE_PROFILE;
}





char aLetter;
int TheLen=strlen(aName)-1;

void MainWindow::on_pb_up_1_clicked()
{
    if   ( name_1 < TheLen ) name_1++;
    else   name_1=0;

    aLetter=aName[name_1];
    sprintf(aTmp,"%c",aLetter);
    ui->pb_val_1->setText(aTmp);

}
void MainWindow::on_pb_up_2_clicked()
{
    if   ( name_2 < TheLen ) name_2++;
    else   name_2=0;

    aLetter=aName[name_2];
    sprintf(aTmp,"%c",aLetter);
    ui->pb_val_2->setText(aTmp);
}
void MainWindow::on_pb_up_3_clicked()
{
    if   ( name_3 < TheLen ) name_3++;
    else   name_3=0;

    aLetter=aName[name_3];
    sprintf(aTmp,"%c",aLetter);
    ui->pb_val_3->setText(aTmp);
}


void MainWindow::on_pb_dw_1_clicked()
{
    if ( name_1 > 0 )    name_1--;
    else  name_1=TheLen;

    aLetter=aName[name_1];
    sprintf(aTmp,"%c",aLetter);
    ui->pb_val_1->setText(aTmp);
}


void MainWindow::on_pb_dw_2_clicked()
{
    if ( name_2 > 0 )    name_2--;
    else  name_2=TheLen;

    aLetter=aName[name_2];
    sprintf(aTmp,"%c",aLetter);
    ui->pb_val_2->setText(aTmp);
}

void MainWindow::on_pb_dw_3_clicked()
{
    if ( name_3 > 0 )    name_3--;
    else  name_3=TheLen;

    aLetter=aName[name_3];
    sprintf(aTmp,"%c",aLetter);
    ui->pb_val_3->setText(aTmp);
}



char aLog[512];

void MainWindow::on_pb_ok_clicked()
{
   current   = QDateTime::currentDateTime();
   TheTime   = current.toString("hh:mm:ss");
   TheDate   = current.toString("yyyy/MM/dd");


   aLog[0]=0;

   sprintf(aLog,"%s-%s ,",
           TheDate.toLatin1().data(),
           TheTime.toLatin1().data()
   ) ;


    if(DRAGON==1) sprintf(&aLog[strlen(aLog)],"DRAGON,");
    if(LION==1  ) sprintf(&aLog[strlen(aLog)],"LION  ,");
    if(CAT==1   ) sprintf(&aLog[strlen(aLog)],"CAT   ,");
    if(MOUSE==1 ) sprintf(&aLog[strlen(aLog)],"MOUSE ,");

    sprintf(&aLog[strlen(aLog)],"%02d,",SONG_ROUND-1);
    sprintf(&aLog[strlen(aLog)],"%05d,",party_time);
    sprintf(&aLog[strlen(aLog)],"%c%c%c",aName[name_1],aName[name_2],aName[name_3]);
    sprintf(aTmp,"echo '%s' >> ~/score.txt",aLog);system(aTmp);

    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;
    STATE=STATE_THE_WALL;
}

void MainWindow::on_pb_cancel_clicked()
{
    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;
    STATE=STATE_THE_WALL;
}
//=============================================================================
void MainWindow::on_pb_quit_clicked()
{
    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;
    STATE=STATE_INIT;
    TIMER_1=0;
    return;
}
//=============================================================================


























void MainWindow::on_pb_mouse_clicked()
{
   MOUSE=1; CAT=0; LION=0; DRAGON=0;
//   ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
//   CurrentScreen = SPLASH_SCREEN;
   SLEEP_TIMEOUT=10;
   STATE = STATE_PREPARE_SCREEN;
}

void MainWindow::on_pb_cat_clicked()
{
    MOUSE=0; CAT=1; LION=0; DRAGON=0;
//    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
//    CurrentScreen = SPLASH_SCREEN;
    SLEEP_TIMEOUT=10;
    STATE = STATE_PREPARE_SCREEN;
}
void MainWindow::on_pb_lion_clicked()
{
    MOUSE=0; CAT=0; LION=1; DRAGON=0;
//    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
//    CurrentScreen = SPLASH_SCREEN;
    SLEEP_TIMEOUT=10;
    STATE = STATE_PREPARE_SCREEN;
}
void MainWindow::on_pb_dragon_clicked()
{
    MOUSE=0; CAT=0; LION=0; DRAGON=1;
//    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
//    CurrentScreen = SPLASH_SCREEN;
    SLEEP_TIMEOUT=10;
    STATE = STATE_PREPARE_SCREEN;
}
