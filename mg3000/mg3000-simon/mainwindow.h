#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Update_Values();

private slots:
    void MasterTimer_update();
    void MainTask_loop();
    void ShowState();


    void on_pb_red_clicked();
    void on_pb_green_clicked();
    void on_pb_blue_clicked();
    void on_pb_yellow_clicked();
    void on_pb_center_clicked();


    void Simon_Start();
    //void Simon_Clear();

    void GenerateSong();
    void PlaySong();
    void ShowSong();

    void PlayRound();
    void Clear_Leds();
    void Clear_Leds2();




    void show_red();
    void show_green();
    void show_blue();
    void show_yellow();
    void show_center();
    void show_up();
    void show_down();

    void on_pb_start_clicked();
    void on_pb_mouse_clicked();
    void on_pb_cat_clicked();
    void on_pb_lion_clicked();
    void on_pb_dragon_clicked();

    void on_pb_up_clicked();
    void on_pb_down_clicked();

    void on_pb_up_1_clicked();
    void on_pb_dw_1_clicked();
    void on_pb_up_2_clicked();
    void on_pb_dw_2_clicked();
    void on_pb_up_3_clicked();
    void on_pb_dw_3_clicked();
    void on_pb_ok_clicked();
    void on_pb_cancel_clicked();
    void on_pb_quit_clicked();
    void UpdateWallOfFame();
    void UpdateBoard(int Animal,int level,int time,char* initials );

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
