/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[47];
    char stringdata0[669];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 18), // "MasterTimer_update"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 13), // "MainTask_loop"
QT_MOC_LITERAL(4, 45, 9), // "ShowState"
QT_MOC_LITERAL(5, 55, 17), // "on_pb_red_clicked"
QT_MOC_LITERAL(6, 73, 19), // "on_pb_green_clicked"
QT_MOC_LITERAL(7, 93, 18), // "on_pb_blue_clicked"
QT_MOC_LITERAL(8, 112, 20), // "on_pb_yellow_clicked"
QT_MOC_LITERAL(9, 133, 20), // "on_pb_center_clicked"
QT_MOC_LITERAL(10, 154, 11), // "Simon_Start"
QT_MOC_LITERAL(11, 166, 12), // "GenerateSong"
QT_MOC_LITERAL(12, 179, 8), // "PlaySong"
QT_MOC_LITERAL(13, 188, 8), // "ShowSong"
QT_MOC_LITERAL(14, 197, 9), // "PlayRound"
QT_MOC_LITERAL(15, 207, 10), // "Clear_Leds"
QT_MOC_LITERAL(16, 218, 11), // "Clear_Leds2"
QT_MOC_LITERAL(17, 230, 8), // "show_red"
QT_MOC_LITERAL(18, 239, 10), // "show_green"
QT_MOC_LITERAL(19, 250, 9), // "show_blue"
QT_MOC_LITERAL(20, 260, 11), // "show_yellow"
QT_MOC_LITERAL(21, 272, 11), // "show_center"
QT_MOC_LITERAL(22, 284, 7), // "show_up"
QT_MOC_LITERAL(23, 292, 9), // "show_down"
QT_MOC_LITERAL(24, 302, 19), // "on_pb_start_clicked"
QT_MOC_LITERAL(25, 322, 19), // "on_pb_mouse_clicked"
QT_MOC_LITERAL(26, 342, 17), // "on_pb_cat_clicked"
QT_MOC_LITERAL(27, 360, 18), // "on_pb_lion_clicked"
QT_MOC_LITERAL(28, 379, 20), // "on_pb_dragon_clicked"
QT_MOC_LITERAL(29, 400, 16), // "on_pb_up_clicked"
QT_MOC_LITERAL(30, 417, 18), // "on_pb_down_clicked"
QT_MOC_LITERAL(31, 436, 18), // "on_pb_up_1_clicked"
QT_MOC_LITERAL(32, 455, 18), // "on_pb_dw_1_clicked"
QT_MOC_LITERAL(33, 474, 18), // "on_pb_up_2_clicked"
QT_MOC_LITERAL(34, 493, 18), // "on_pb_dw_2_clicked"
QT_MOC_LITERAL(35, 512, 18), // "on_pb_up_3_clicked"
QT_MOC_LITERAL(36, 531, 18), // "on_pb_dw_3_clicked"
QT_MOC_LITERAL(37, 550, 16), // "on_pb_ok_clicked"
QT_MOC_LITERAL(38, 567, 20), // "on_pb_cancel_clicked"
QT_MOC_LITERAL(39, 588, 18), // "on_pb_quit_clicked"
QT_MOC_LITERAL(40, 607, 16), // "UpdateWallOfFame"
QT_MOC_LITERAL(41, 624, 11), // "UpdateBoard"
QT_MOC_LITERAL(42, 636, 6), // "Animal"
QT_MOC_LITERAL(43, 643, 5), // "level"
QT_MOC_LITERAL(44, 649, 4), // "time"
QT_MOC_LITERAL(45, 654, 5), // "char*"
QT_MOC_LITERAL(46, 660, 8) // "initials"

    },
    "MainWindow\0MasterTimer_update\0\0"
    "MainTask_loop\0ShowState\0on_pb_red_clicked\0"
    "on_pb_green_clicked\0on_pb_blue_clicked\0"
    "on_pb_yellow_clicked\0on_pb_center_clicked\0"
    "Simon_Start\0GenerateSong\0PlaySong\0"
    "ShowSong\0PlayRound\0Clear_Leds\0Clear_Leds2\0"
    "show_red\0show_green\0show_blue\0show_yellow\0"
    "show_center\0show_up\0show_down\0"
    "on_pb_start_clicked\0on_pb_mouse_clicked\0"
    "on_pb_cat_clicked\0on_pb_lion_clicked\0"
    "on_pb_dragon_clicked\0on_pb_up_clicked\0"
    "on_pb_down_clicked\0on_pb_up_1_clicked\0"
    "on_pb_dw_1_clicked\0on_pb_up_2_clicked\0"
    "on_pb_dw_2_clicked\0on_pb_up_3_clicked\0"
    "on_pb_dw_3_clicked\0on_pb_ok_clicked\0"
    "on_pb_cancel_clicked\0on_pb_quit_clicked\0"
    "UpdateWallOfFame\0UpdateBoard\0Animal\0"
    "level\0time\0char*\0initials"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      40,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  214,    2, 0x08 /* Private */,
       3,    0,  215,    2, 0x08 /* Private */,
       4,    0,  216,    2, 0x08 /* Private */,
       5,    0,  217,    2, 0x08 /* Private */,
       6,    0,  218,    2, 0x08 /* Private */,
       7,    0,  219,    2, 0x08 /* Private */,
       8,    0,  220,    2, 0x08 /* Private */,
       9,    0,  221,    2, 0x08 /* Private */,
      10,    0,  222,    2, 0x08 /* Private */,
      11,    0,  223,    2, 0x08 /* Private */,
      12,    0,  224,    2, 0x08 /* Private */,
      13,    0,  225,    2, 0x08 /* Private */,
      14,    0,  226,    2, 0x08 /* Private */,
      15,    0,  227,    2, 0x08 /* Private */,
      16,    0,  228,    2, 0x08 /* Private */,
      17,    0,  229,    2, 0x08 /* Private */,
      18,    0,  230,    2, 0x08 /* Private */,
      19,    0,  231,    2, 0x08 /* Private */,
      20,    0,  232,    2, 0x08 /* Private */,
      21,    0,  233,    2, 0x08 /* Private */,
      22,    0,  234,    2, 0x08 /* Private */,
      23,    0,  235,    2, 0x08 /* Private */,
      24,    0,  236,    2, 0x08 /* Private */,
      25,    0,  237,    2, 0x08 /* Private */,
      26,    0,  238,    2, 0x08 /* Private */,
      27,    0,  239,    2, 0x08 /* Private */,
      28,    0,  240,    2, 0x08 /* Private */,
      29,    0,  241,    2, 0x08 /* Private */,
      30,    0,  242,    2, 0x08 /* Private */,
      31,    0,  243,    2, 0x08 /* Private */,
      32,    0,  244,    2, 0x08 /* Private */,
      33,    0,  245,    2, 0x08 /* Private */,
      34,    0,  246,    2, 0x08 /* Private */,
      35,    0,  247,    2, 0x08 /* Private */,
      36,    0,  248,    2, 0x08 /* Private */,
      37,    0,  249,    2, 0x08 /* Private */,
      38,    0,  250,    2, 0x08 /* Private */,
      39,    0,  251,    2, 0x08 /* Private */,
      40,    0,  252,    2, 0x08 /* Private */,
      41,    4,  253,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 45,   42,   43,   44,   46,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->MasterTimer_update(); break;
        case 1: _t->MainTask_loop(); break;
        case 2: _t->ShowState(); break;
        case 3: _t->on_pb_red_clicked(); break;
        case 4: _t->on_pb_green_clicked(); break;
        case 5: _t->on_pb_blue_clicked(); break;
        case 6: _t->on_pb_yellow_clicked(); break;
        case 7: _t->on_pb_center_clicked(); break;
        case 8: _t->Simon_Start(); break;
        case 9: _t->GenerateSong(); break;
        case 10: _t->PlaySong(); break;
        case 11: _t->ShowSong(); break;
        case 12: _t->PlayRound(); break;
        case 13: _t->Clear_Leds(); break;
        case 14: _t->Clear_Leds2(); break;
        case 15: _t->show_red(); break;
        case 16: _t->show_green(); break;
        case 17: _t->show_blue(); break;
        case 18: _t->show_yellow(); break;
        case 19: _t->show_center(); break;
        case 20: _t->show_up(); break;
        case 21: _t->show_down(); break;
        case 22: _t->on_pb_start_clicked(); break;
        case 23: _t->on_pb_mouse_clicked(); break;
        case 24: _t->on_pb_cat_clicked(); break;
        case 25: _t->on_pb_lion_clicked(); break;
        case 26: _t->on_pb_dragon_clicked(); break;
        case 27: _t->on_pb_up_clicked(); break;
        case 28: _t->on_pb_down_clicked(); break;
        case 29: _t->on_pb_up_1_clicked(); break;
        case 30: _t->on_pb_dw_1_clicked(); break;
        case 31: _t->on_pb_up_2_clicked(); break;
        case 32: _t->on_pb_dw_2_clicked(); break;
        case 33: _t->on_pb_up_3_clicked(); break;
        case 34: _t->on_pb_dw_3_clicked(); break;
        case 35: _t->on_pb_ok_clicked(); break;
        case 36: _t->on_pb_cancel_clicked(); break;
        case 37: _t->on_pb_quit_clicked(); break;
        case 38: _t->UpdateWallOfFame(); break;
        case 39: _t->UpdateBoard((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< char*(*)>(_a[4]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 40)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 40;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 40)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 40;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
