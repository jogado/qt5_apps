/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *MainScreen;
    QLabel *label;
    QPushButton *pb_mouse;
    QPushButton *pb_cat;
    QPushButton *pb_lion;
    QPushButton *pb_dragon;
    QWidget *SplashScreen;
    QPushButton *pb_red;
    QPushButton *pb_green;
    QPushButton *pb_blue;
    QPushButton *pb_yellow;
    QPushButton *pb_background;
    QPushButton *pb_center;
    QPushButton *pb_start;
    QPushButton *pb_up;
    QPushButton *pb_down;
    QWidget *RecordScreen;
    QLabel *label_Header;
    QLabel *label_wall;
    QPushButton *pb_quit;
    QLabel *label_dragon;
    QLabel *label_dragon_level;
    QLabel *label_dragon_name;
    QLabel *label_lion;
    QLabel *label_lion_level;
    QLabel *label_lion_name;
    QLabel *label_cat;
    QLabel *label_cat_level;
    QLabel *label_cat_name;
    QLabel *label_mouse;
    QLabel *label_mouse_level;
    QLabel *label_mouse_name;
    QLabel *label_mouse_time;
    QLabel *label_lion_time;
    QLabel *label_dragon_time;
    QLabel *label_cat_time;
    QWidget *NameScreen;
    QLabel *label_2;
    QPushButton *pb_val_1;
    QPushButton *pb_val_2;
    QPushButton *pb_val_3;
    QPushButton *pb_ok;
    QPushButton *pb_cancel;
    QPushButton *pb_up_3;
    QPushButton *pb_up_1;
    QPushButton *pb_up_2;
    QPushButton *pb_dw_3;
    QPushButton *pb_dw_1;
    QPushButton *pb_dw_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(480, 640);
        MainWindow->setStyleSheet(QString::fromUtf8("color:black;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 480, 640));
        QFont font;
        font.setFamily(QString::fromUtf8("Liberation Sans Narrow"));
        font.setPointSize(30);
        font.setBold(true);
        font.setWeight(75);
        stackedWidget->setFont(font);
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setContextMenuPolicy(Qt::PreventContextMenu);
        stackedWidget->setStyleSheet(QString::fromUtf8("background-color:black;\n"
"color:yellow"));
        MainScreen = new QWidget();
        MainScreen->setObjectName(QString::fromUtf8("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QString::fromUtf8("background-color:black"));
        label = new QLabel(MainScreen);
        label->setObjectName(QString::fromUtf8("label"));
        label->setEnabled(true);
        label->setGeometry(QRect(0, 19, 480, 81));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Liberation Serif"));
        font1.setPointSize(30);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        label->setFont(font1);
        label->setStyleSheet(QString::fromUtf8("color:RED;"));
        label->setLineWidth(2);
        label->setAlignment(Qt::AlignCenter);
        pb_mouse = new QPushButton(MainScreen);
        pb_mouse->setObjectName(QString::fromUtf8("pb_mouse"));
        pb_mouse->setGeometry(QRect(20, 140, 440, 80));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Liberation Serif"));
        font2.setPointSize(30);
        font2.setBold(true);
        font2.setWeight(75);
        pb_mouse->setFont(font2);
        pb_mouse->setStyleSheet(QString::fromUtf8("color:Yellow;\n"
"background-color: black;\n"
"border:1px solid #FF0000;\n"
""));
        pb_cat = new QPushButton(MainScreen);
        pb_cat->setObjectName(QString::fromUtf8("pb_cat"));
        pb_cat->setGeometry(QRect(20, 240, 440, 80));
        pb_cat->setFont(font2);
        pb_cat->setStyleSheet(QString::fromUtf8("color:Yellow;\n"
"background-color: black;\n"
"border:1px solid #FF0000;\n"
""));
        pb_lion = new QPushButton(MainScreen);
        pb_lion->setObjectName(QString::fromUtf8("pb_lion"));
        pb_lion->setGeometry(QRect(20, 340, 440, 80));
        pb_lion->setFont(font2);
        pb_lion->setStyleSheet(QString::fromUtf8("color:Yellow;\n"
"background-color: black;\n"
"border:1px solid #FF0000;\n"
""));
        pb_dragon = new QPushButton(MainScreen);
        pb_dragon->setObjectName(QString::fromUtf8("pb_dragon"));
        pb_dragon->setGeometry(QRect(20, 440, 440, 80));
        pb_dragon->setFont(font2);
        pb_dragon->setStyleSheet(QString::fromUtf8("color:Yellow;\n"
"background-color: black;\n"
"border:1px solid #FF0000;\n"
""));
        stackedWidget->addWidget(MainScreen);
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QString::fromUtf8("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QString::fromUtf8(""));
        pb_red = new QPushButton(SplashScreen);
        pb_red->setObjectName(QString::fromUtf8("pb_red"));
        pb_red->setGeometry(QRect(10, 90, 225, 225));
        pb_red->setStyleSheet(QString::fromUtf8("background-color:  rgb(150,0,0);"));
        pb_green = new QPushButton(SplashScreen);
        pb_green->setObjectName(QString::fromUtf8("pb_green"));
        pb_green->setGeometry(QRect(245, 90, 225, 225));
        pb_green->setStyleSheet(QString::fromUtf8("background-color:green"));
        pb_blue = new QPushButton(SplashScreen);
        pb_blue->setObjectName(QString::fromUtf8("pb_blue"));
        pb_blue->setGeometry(QRect(10, 325, 225, 225));
        pb_blue->setStyleSheet(QString::fromUtf8("background-color:rgb(21, 10, 124)\n"
""));
        pb_yellow = new QPushButton(SplashScreen);
        pb_yellow->setObjectName(QString::fromUtf8("pb_yellow"));
        pb_yellow->setGeometry(QRect(245, 325, 225, 225));
        pb_yellow->setStyleSheet(QString::fromUtf8("background-color:yellow\n"
""));
        pb_background = new QPushButton(SplashScreen);
        pb_background->setObjectName(QString::fromUtf8("pb_background"));
        pb_background->setGeometry(QRect(0, 0, 480, 640));
        pb_background->setStyleSheet(QString::fromUtf8("background-color: white\n"
""));
        pb_center = new QPushButton(SplashScreen);
        pb_center->setObjectName(QString::fromUtf8("pb_center"));
        pb_center->setGeometry(QRect(128, 208, 225, 225));
        pb_center->setFont(font2);
        pb_center->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_center->setStyleSheet(QString::fromUtf8("background-color: rgb(208, 250, 247);\n"
"border:10px solid #FFFFFF;\n"
"background-color: rgb(117, 80, 123);\n"
"border-radius:112px;\n"
"color:white"));
        pb_start = new QPushButton(SplashScreen);
        pb_start->setObjectName(QString::fromUtf8("pb_start"));
        pb_start->setGeometry(QRect(128, 208, 225, 225));
        pb_start->setFont(font2);
        pb_start->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_start->setStyleSheet(QString::fromUtf8("background-color: rgb(255,0,0);\n"
"border:10px solid #FFFFFF;\n"
"border-radius:112px;\n"
"color:white"));
        pb_up = new QPushButton(SplashScreen);
        pb_up->setObjectName(QString::fromUtf8("pb_up"));
        pb_up->setGeometry(QRect(10, 10, 460, 70));
        pb_up->setStyleSheet(QString::fromUtf8("background-color:rgb(193, 125, 17)"));
        pb_down = new QPushButton(SplashScreen);
        pb_down->setObjectName(QString::fromUtf8("pb_down"));
        pb_down->setGeometry(QRect(10, 560, 460, 70));
        pb_down->setStyleSheet(QString::fromUtf8("background-color: rgb(162, 220, 88);"));
        stackedWidget->addWidget(SplashScreen);
        pb_background->raise();
        pb_red->raise();
        pb_green->raise();
        pb_blue->raise();
        pb_yellow->raise();
        pb_center->raise();
        pb_start->raise();
        pb_up->raise();
        pb_down->raise();
        RecordScreen = new QWidget();
        RecordScreen->setObjectName(QString::fromUtf8("RecordScreen"));
        label_Header = new QLabel(RecordScreen);
        label_Header->setObjectName(QString::fromUtf8("label_Header"));
        label_Header->setEnabled(true);
        label_Header->setGeometry(QRect(0, 0, 480, 81));
        label_Header->setFont(font1);
        label_Header->setStyleSheet(QString::fromUtf8("color:RED;"));
        label_Header->setLineWidth(2);
        label_Header->setAlignment(Qt::AlignCenter);
        label_wall = new QLabel(RecordScreen);
        label_wall->setObjectName(QString::fromUtf8("label_wall"));
        label_wall->setEnabled(true);
        label_wall->setGeometry(QRect(10, 258, 460, 281));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Liberation Serif"));
        font3.setPointSize(15);
        font3.setBold(true);
        font3.setItalic(false);
        font3.setWeight(75);
        label_wall->setFont(font3);
        label_wall->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_wall->setLineWidth(2);
        label_wall->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        pb_quit = new QPushButton(RecordScreen);
        pb_quit->setObjectName(QString::fromUtf8("pb_quit"));
        pb_quit->setGeometry(QRect(10, 559, 461, 51));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Liberation Serif"));
        font4.setPointSize(30);
        font4.setBold(true);
        font4.setItalic(true);
        font4.setWeight(75);
        pb_quit->setFont(font4);
        pb_quit->setLayoutDirection(Qt::LeftToRight);
        pb_quit->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color:blue;"));
        label_dragon = new QLabel(RecordScreen);
        label_dragon->setObjectName(QString::fromUtf8("label_dragon"));
        label_dragon->setEnabled(true);
        label_dragon->setGeometry(QRect(104, 90, 120, 30));
        label_dragon->setFont(font3);
        label_dragon->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_dragon->setLineWidth(2);
        label_dragon->setAlignment(Qt::AlignCenter);
        label_dragon_level = new QLabel(RecordScreen);
        label_dragon_level->setObjectName(QString::fromUtf8("label_dragon_level"));
        label_dragon_level->setEnabled(true);
        label_dragon_level->setGeometry(QRect(233, 90, 31, 30));
        label_dragon_level->setFont(font3);
        label_dragon_level->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_dragon_level->setFrameShape(QFrame::NoFrame);
        label_dragon_level->setLineWidth(2);
        label_dragon_level->setAlignment(Qt::AlignCenter);
        label_dragon_name = new QLabel(RecordScreen);
        label_dragon_name->setObjectName(QString::fromUtf8("label_dragon_name"));
        label_dragon_name->setEnabled(true);
        label_dragon_name->setGeometry(QRect(358, 90, 71, 30));
        label_dragon_name->setFont(font3);
        label_dragon_name->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_dragon_name->setLineWidth(2);
        label_dragon_name->setAlignment(Qt::AlignCenter);
        label_lion = new QLabel(RecordScreen);
        label_lion->setObjectName(QString::fromUtf8("label_lion"));
        label_lion->setEnabled(true);
        label_lion->setGeometry(QRect(104, 128, 120, 30));
        label_lion->setFont(font3);
        label_lion->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_lion->setLineWidth(2);
        label_lion->setAlignment(Qt::AlignCenter);
        label_lion_level = new QLabel(RecordScreen);
        label_lion_level->setObjectName(QString::fromUtf8("label_lion_level"));
        label_lion_level->setEnabled(true);
        label_lion_level->setGeometry(QRect(233, 128, 31, 30));
        label_lion_level->setFont(font3);
        label_lion_level->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_lion_level->setFrameShape(QFrame::NoFrame);
        label_lion_level->setLineWidth(2);
        label_lion_level->setAlignment(Qt::AlignCenter);
        label_lion_name = new QLabel(RecordScreen);
        label_lion_name->setObjectName(QString::fromUtf8("label_lion_name"));
        label_lion_name->setEnabled(true);
        label_lion_name->setGeometry(QRect(358, 128, 71, 30));
        label_lion_name->setFont(font3);
        label_lion_name->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_lion_name->setLineWidth(2);
        label_lion_name->setAlignment(Qt::AlignCenter);
        label_cat = new QLabel(RecordScreen);
        label_cat->setObjectName(QString::fromUtf8("label_cat"));
        label_cat->setEnabled(true);
        label_cat->setGeometry(QRect(104, 168, 120, 30));
        label_cat->setFont(font3);
        label_cat->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_cat->setLineWidth(2);
        label_cat->setAlignment(Qt::AlignCenter);
        label_cat_level = new QLabel(RecordScreen);
        label_cat_level->setObjectName(QString::fromUtf8("label_cat_level"));
        label_cat_level->setEnabled(true);
        label_cat_level->setGeometry(QRect(233, 168, 31, 30));
        label_cat_level->setFont(font3);
        label_cat_level->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_cat_level->setFrameShape(QFrame::NoFrame);
        label_cat_level->setLineWidth(2);
        label_cat_level->setAlignment(Qt::AlignCenter);
        label_cat_name = new QLabel(RecordScreen);
        label_cat_name->setObjectName(QString::fromUtf8("label_cat_name"));
        label_cat_name->setEnabled(true);
        label_cat_name->setGeometry(QRect(358, 168, 71, 30));
        label_cat_name->setFont(font3);
        label_cat_name->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_cat_name->setLineWidth(2);
        label_cat_name->setAlignment(Qt::AlignCenter);
        label_mouse = new QLabel(RecordScreen);
        label_mouse->setObjectName(QString::fromUtf8("label_mouse"));
        label_mouse->setEnabled(true);
        label_mouse->setGeometry(QRect(104, 208, 120, 30));
        label_mouse->setFont(font3);
        label_mouse->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_mouse->setLineWidth(2);
        label_mouse->setAlignment(Qt::AlignCenter);
        label_mouse_level = new QLabel(RecordScreen);
        label_mouse_level->setObjectName(QString::fromUtf8("label_mouse_level"));
        label_mouse_level->setEnabled(true);
        label_mouse_level->setGeometry(QRect(233, 208, 31, 30));
        label_mouse_level->setFont(font3);
        label_mouse_level->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_mouse_level->setFrameShape(QFrame::NoFrame);
        label_mouse_level->setLineWidth(2);
        label_mouse_level->setAlignment(Qt::AlignCenter);
        label_mouse_name = new QLabel(RecordScreen);
        label_mouse_name->setObjectName(QString::fromUtf8("label_mouse_name"));
        label_mouse_name->setEnabled(true);
        label_mouse_name->setGeometry(QRect(358, 208, 71, 30));
        label_mouse_name->setFont(font3);
        label_mouse_name->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_mouse_name->setLineWidth(2);
        label_mouse_name->setAlignment(Qt::AlignCenter);
        label_mouse_time = new QLabel(RecordScreen);
        label_mouse_time->setObjectName(QString::fromUtf8("label_mouse_time"));
        label_mouse_time->setEnabled(true);
        label_mouse_time->setGeometry(QRect(270, 210, 71, 30));
        label_mouse_time->setFont(font3);
        label_mouse_time->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_mouse_time->setLineWidth(2);
        label_mouse_time->setAlignment(Qt::AlignCenter);
        label_lion_time = new QLabel(RecordScreen);
        label_lion_time->setObjectName(QString::fromUtf8("label_lion_time"));
        label_lion_time->setEnabled(true);
        label_lion_time->setGeometry(QRect(270, 130, 71, 30));
        label_lion_time->setFont(font3);
        label_lion_time->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_lion_time->setLineWidth(2);
        label_lion_time->setAlignment(Qt::AlignCenter);
        label_dragon_time = new QLabel(RecordScreen);
        label_dragon_time->setObjectName(QString::fromUtf8("label_dragon_time"));
        label_dragon_time->setEnabled(true);
        label_dragon_time->setGeometry(QRect(270, 92, 71, 30));
        label_dragon_time->setFont(font3);
        label_dragon_time->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_dragon_time->setLineWidth(2);
        label_dragon_time->setAlignment(Qt::AlignCenter);
        label_cat_time = new QLabel(RecordScreen);
        label_cat_time->setObjectName(QString::fromUtf8("label_cat_time"));
        label_cat_time->setEnabled(true);
        label_cat_time->setGeometry(QRect(270, 170, 71, 30));
        label_cat_time->setFont(font3);
        label_cat_time->setStyleSheet(QString::fromUtf8("color:yellow;\n"
"background-color: green\n"
"\n"
""));
        label_cat_time->setLineWidth(2);
        label_cat_time->setAlignment(Qt::AlignCenter);
        stackedWidget->addWidget(RecordScreen);
        NameScreen = new QWidget();
        NameScreen->setObjectName(QString::fromUtf8("NameScreen"));
        label_2 = new QLabel(NameScreen);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setEnabled(true);
        label_2->setGeometry(QRect(0, 0, 480, 81));
        label_2->setFont(font1);
        label_2->setStyleSheet(QString::fromUtf8("color:RED;"));
        label_2->setLineWidth(2);
        label_2->setAlignment(Qt::AlignCenter);
        pb_val_1 = new QPushButton(NameScreen);
        pb_val_1->setObjectName(QString::fromUtf8("pb_val_1"));
        pb_val_1->setGeometry(QRect(80, 210, 81, 70));
        pb_val_1->setFont(font);
        pb_val_1->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_val_1->setStyleSheet(QString::fromUtf8("background-color:black;\n"
"border:5px solid #FF0000;\n"
"color:white;"));
        pb_val_2 = new QPushButton(NameScreen);
        pb_val_2->setObjectName(QString::fromUtf8("pb_val_2"));
        pb_val_2->setGeometry(QRect(180, 210, 81, 70));
        pb_val_2->setFont(font);
        pb_val_2->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_val_2->setStyleSheet(QString::fromUtf8("background-color:black;border:5px solid #FF0000;color:white;"));
        pb_val_3 = new QPushButton(NameScreen);
        pb_val_3->setObjectName(QString::fromUtf8("pb_val_3"));
        pb_val_3->setGeometry(QRect(280, 210, 81, 70));
        pb_val_3->setFont(font);
        pb_val_3->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_val_3->setStyleSheet(QString::fromUtf8("background-color:black;border:5px solid #FF0000;color:white;"));
        pb_ok = new QPushButton(NameScreen);
        pb_ok->setObjectName(QString::fromUtf8("pb_ok"));
        pb_ok->setGeometry(QRect(80, 410, 281, 111));
        pb_ok->setFont(font4);
        pb_ok->setStyleSheet(QString::fromUtf8("background-color:green;color:white"));
        pb_cancel = new QPushButton(NameScreen);
        pb_cancel->setObjectName(QString::fromUtf8("pb_cancel"));
        pb_cancel->setGeometry(QRect(160, 529, 311, 101));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Liberation Mono"));
        font5.setPointSize(30);
        font5.setBold(true);
        font5.setWeight(75);
        pb_cancel->setFont(font5);
        pb_cancel->setStyleSheet(QString::fromUtf8("background-color:red;color:white"));
        pb_up_3 = new QPushButton(NameScreen);
        pb_up_3->setObjectName(QString::fromUtf8("pb_up_3"));
        pb_up_3->setGeometry(QRect(280, 120, 80, 80));
        pb_up_3->setFont(font);
        pb_up_3->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_up_3->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/up_yellow.bmp);"));
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("SP_ArrowLeft");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        pb_up_3->setIcon(icon);
        pb_up_1 = new QPushButton(NameScreen);
        pb_up_1->setObjectName(QString::fromUtf8("pb_up_1"));
        pb_up_1->setGeometry(QRect(80, 120, 80, 80));
        pb_up_1->setFont(font);
        pb_up_1->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_up_1->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/up_yellow.bmp);"));
        pb_up_2 = new QPushButton(NameScreen);
        pb_up_2->setObjectName(QString::fromUtf8("pb_up_2"));
        pb_up_2->setGeometry(QRect(180, 120, 80, 80));
        pb_up_2->setFont(font);
        pb_up_2->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_up_2->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/up_yellow.bmp);"));
        pb_dw_3 = new QPushButton(NameScreen);
        pb_dw_3->setObjectName(QString::fromUtf8("pb_dw_3"));
        pb_dw_3->setGeometry(QRect(280, 286, 80, 80));
        pb_dw_3->setFont(font);
        pb_dw_3->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_dw_3->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/down_yellow.bmp);"));
        pb_dw_1 = new QPushButton(NameScreen);
        pb_dw_1->setObjectName(QString::fromUtf8("pb_dw_1"));
        pb_dw_1->setGeometry(QRect(80, 286, 80, 80));
        pb_dw_1->setFont(font);
        pb_dw_1->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_dw_1->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/down_yellow.bmp);"));
        pb_dw_2 = new QPushButton(NameScreen);
        pb_dw_2->setObjectName(QString::fromUtf8("pb_dw_2"));
        pb_dw_2->setGeometry(QRect(180, 286, 80, 80));
        pb_dw_2->setFont(font);
        pb_dw_2->setContextMenuPolicy(Qt::PreventContextMenu);
        pb_dw_2->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/down_yellow.bmp);"));
        stackedWidget->addWidget(NameScreen);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Ooh, you are a ...", nullptr));
        pb_mouse->setText(QCoreApplication::translate("MainWindow", "Mouse", nullptr));
        pb_cat->setText(QCoreApplication::translate("MainWindow", "Cat", nullptr));
        pb_lion->setText(QCoreApplication::translate("MainWindow", "Lion", nullptr));
        pb_dragon->setText(QCoreApplication::translate("MainWindow", "Dragon", nullptr));
        pb_red->setText(QString());
        pb_green->setText(QString());
        pb_blue->setText(QString());
        pb_yellow->setText(QString());
        pb_background->setText(QString());
        pb_center->setText(QString());
        pb_start->setText(QCoreApplication::translate("MainWindow", "Start", nullptr));
        pb_up->setText(QString());
        pb_down->setText(QString());
        label_Header->setText(QCoreApplication::translate("MainWindow", "Glorious Champions", nullptr));
        label_wall->setText(QCoreApplication::translate("MainWindow", "TEST", nullptr));
        pb_quit->setText(QCoreApplication::translate("MainWindow", "Quit", nullptr));
        label_dragon->setText(QCoreApplication::translate("MainWindow", "DRAGON", nullptr));
        label_dragon_level->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_dragon_name->setText(QString());
        label_lion->setText(QCoreApplication::translate("MainWindow", "LION", nullptr));
        label_lion_level->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_lion_name->setText(QString());
        label_cat->setText(QCoreApplication::translate("MainWindow", "CAT", nullptr));
        label_cat_level->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_cat_name->setText(QString());
        label_mouse->setText(QCoreApplication::translate("MainWindow", "MOUSE", nullptr));
        label_mouse_level->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_mouse_name->setText(QString());
        label_mouse_time->setText(QString());
        label_lion_time->setText(QString());
        label_dragon_time->setText(QString());
        label_cat_time->setText(QString());
        label_2->setText(QCoreApplication::translate("MainWindow", "We have a winner !!!", nullptr));
        pb_val_1->setText(QCoreApplication::translate("MainWindow", "*", nullptr));
        pb_val_2->setText(QCoreApplication::translate("MainWindow", "*", nullptr));
        pb_val_3->setText(QCoreApplication::translate("MainWindow", "*", nullptr));
        pb_ok->setText(QCoreApplication::translate("MainWindow", "I'm :-)", nullptr));
        pb_cancel->setText(QCoreApplication::translate("MainWindow", "Forget me !", nullptr));
        pb_up_3->setText(QString());
        pb_up_1->setText(QString());
        pb_up_2->setText(QString());
        pb_dw_3->setText(QString());
        pb_dw_1->setText(QString());
        pb_dw_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
