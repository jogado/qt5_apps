/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *SplashScreen;
    QPushButton *pb_header_2;
    QWidget *MainScreen;
    QLabel *Counter;
    QLabel *ip_address;
    QLabel *free_text;
    QPushButton *pushButton;
    QLabel *AdultCount;
    QLabel *TheDate;
    QLabel *TheTime;
    QPushButton *pb_ValidCard;
    QPushButton *pb_InvalidCard;
    QPushButton *pb_waiting;
    QWidget *Service;
    QPushButton *pb_header;
    QPushButton *pb_moti;
    QPushButton *pb_barcode;
    QPushButton *pb_reboot;
    QLabel *Counter_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(480, 640);
        MainWindow->setStyleSheet(QString::fromUtf8("color:black;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 480, 640));
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QString::fromUtf8(""));
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QString::fromUtf8("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QString::fromUtf8("image:url(:/bmp/IT-Trans.bmp)"));
        pb_header_2 = new QPushButton(SplashScreen);
        pb_header_2->setObjectName(QString::fromUtf8("pb_header_2"));
        pb_header_2->setGeometry(QRect(10, 20, 460, 76));
        QFont font;
        font.setFamily(QString::fromUtf8("Liberation Sans"));
        font.setPointSize(23);
        font.setBold(true);
        font.setWeight(75);
        pb_header_2->setFont(font);
        pb_header_2->setStyleSheet(QString::fromUtf8("background-color: rgb(20, 60, 118);\n"
"border: none;\n"
"color:white;\n"
"\n"
""));
        stackedWidget->addWidget(SplashScreen);
        MainScreen = new QWidget();
        MainScreen->setObjectName(QString::fromUtf8("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QString::fromUtf8(""));
        Counter = new QLabel(MainScreen);
        Counter->setObjectName(QString::fromUtf8("Counter"));
        Counter->setEnabled(true);
        Counter->setGeometry(QRect(324, 36, 141, 20));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Liberation Sans"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        Counter->setFont(font1);
        Counter->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)"));
        Counter->setLineWidth(1);
        Counter->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        ip_address = new QLabel(MainScreen);
        ip_address->setObjectName(QString::fromUtf8("ip_address"));
        ip_address->setGeometry(QRect(340, 15, 121, 20));
        ip_address->setFont(font1);
        ip_address->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);"));
        ip_address->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        free_text = new QLabel(MainScreen);
        free_text->setObjectName(QString::fromUtf8("free_text"));
        free_text->setGeometry(QRect(80, 380, 31, 21));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Liberation Sans"));
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        free_text->setFont(font2);
        free_text->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
""));
        free_text->setLineWidth(2);
        free_text->setWordWrap(true);
        pushButton = new QPushButton(MainScreen);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setEnabled(false);
        pushButton->setGeometry(QRect(0, 0, 480, 640));
        pushButton->setStyleSheet(QString::fromUtf8("border: none;\n"
"background-image: url(:/bmp/MG3000_Main2.bmp) ;"));
        AdultCount = new QLabel(MainScreen);
        AdultCount->setObjectName(QString::fromUtf8("AdultCount"));
        AdultCount->setEnabled(true);
        AdultCount->setGeometry(QRect(263, 212, 80, 60));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Liberation Sans"));
        font3.setPointSize(35);
        font3.setBold(true);
        font3.setItalic(false);
        font3.setWeight(75);
        AdultCount->setFont(font3);
        AdultCount->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        AdultCount->setLineWidth(2);
        AdultCount->setAlignment(Qt::AlignCenter);
        TheDate = new QLabel(MainScreen);
        TheDate->setObjectName(QString::fromUtf8("TheDate"));
        TheDate->setEnabled(true);
        TheDate->setGeometry(QRect(170, 20, 100, 20));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Liberation Sans"));
        font4.setPointSize(12);
        font4.setItalic(false);
        TheDate->setFont(font4);
        TheDate->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
""));
        TheDate->setLineWidth(2);
        TheDate->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        TheTime = new QLabel(MainScreen);
        TheTime->setObjectName(QString::fromUtf8("TheTime"));
        TheTime->setEnabled(true);
        TheTime->setGeometry(QRect(170, 38, 100, 20));
        TheTime->setFont(font4);
        TheTime->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        TheTime->setLineWidth(2);
        TheTime->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        pb_ValidCard = new QPushButton(MainScreen);
        pb_ValidCard->setObjectName(QString::fromUtf8("pb_ValidCard"));
        pb_ValidCard->setGeometry(QRect(11, 554, 460, 76));
        pb_ValidCard->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/ValidCard.bmp);\n"
"border: none;"));
        pb_InvalidCard = new QPushButton(MainScreen);
        pb_InvalidCard->setObjectName(QString::fromUtf8("pb_InvalidCard"));
        pb_InvalidCard->setGeometry(QRect(11, 554, 460, 76));
        pb_InvalidCard->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/InvalidCard.bmp);\n"
"border: none;"));
        pb_waiting = new QPushButton(MainScreen);
        pb_waiting->setObjectName(QString::fromUtf8("pb_waiting"));
        pb_waiting->setGeometry(QRect(11, 554, 460, 76));
        pb_waiting->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/MG_Arrow.bmp);\n"
"border: none;"));
        stackedWidget->addWidget(MainScreen);
        pushButton->raise();
        Counter->raise();
        free_text->raise();
        ip_address->raise();
        AdultCount->raise();
        TheDate->raise();
        TheTime->raise();
        pb_ValidCard->raise();
        pb_InvalidCard->raise();
        pb_waiting->raise();
        Service = new QWidget();
        Service->setObjectName(QString::fromUtf8("Service"));
        pb_header = new QPushButton(Service);
        pb_header->setObjectName(QString::fromUtf8("pb_header"));
        pb_header->setGeometry(QRect(10, 20, 460, 76));
        pb_header->setFont(font);
        pb_header->setStyleSheet(QString::fromUtf8("background-color: rgb(20, 60, 118);\n"
"border: none;\n"
"color:white;\n"
"\n"
""));
        pb_moti = new QPushButton(Service);
        pb_moti->setObjectName(QString::fromUtf8("pb_moti"));
        pb_moti->setGeometry(QRect(10, 170, 451, 76));
        pb_moti->setFont(font);
        pb_moti->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_moti->setFlat(false);
        pb_barcode = new QPushButton(Service);
        pb_barcode->setObjectName(QString::fromUtf8("pb_barcode"));
        pb_barcode->setGeometry(QRect(10, 250, 451, 76));
        pb_barcode->setFont(font);
        pb_barcode->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_reboot = new QPushButton(Service);
        pb_reboot->setObjectName(QString::fromUtf8("pb_reboot"));
        pb_reboot->setGeometry(QRect(10, 330, 451, 76));
        pb_reboot->setFont(font);
        pb_reboot->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        Counter_2 = new QLabel(Service);
        Counter_2->setObjectName(QString::fromUtf8("Counter_2"));
        Counter_2->setEnabled(true);
        Counter_2->setGeometry(QRect(425, 70, 31, 20));
        Counter_2->setFont(font1);
        Counter_2->setStyleSheet(QString::fromUtf8("color:white"));
        Counter_2->setLineWidth(2);
        Counter_2->setAlignment(Qt::AlignCenter);
        stackedWidget->addWidget(Service);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        pb_header_2->setText(QCoreApplication::translate("MainWindow", "SPLASH", nullptr));
        Counter->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        ip_address->setText(QCoreApplication::translate("MainWindow", "000.000.000.000", nullptr));
        free_text->setText(QCoreApplication::translate("MainWindow", "---", nullptr));
        pushButton->setText(QString());
        AdultCount->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TheDate->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TheTime->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        pb_ValidCard->setText(QString());
        pb_InvalidCard->setText(QString());
        pb_waiting->setText(QString());
        pb_header->setText(QCoreApplication::translate("MainWindow", "SERVICE SCREEN", nullptr));
        pb_moti->setText(QCoreApplication::translate("MainWindow", "Restart Moti", nullptr));
        pb_barcode->setText(QCoreApplication::translate("MainWindow", "Restart Barcode", nullptr));
        pb_reboot->setText(QCoreApplication::translate("MainWindow", "Reboot Device", nullptr));
        Counter_2->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
