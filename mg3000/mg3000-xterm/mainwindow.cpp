#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QTimer>
#include <QPushButton>

/*
screen-test -m "Cover light sensor" -b 0000ff -f 200 &

/usr/bin/screen-test -b 404040 -t 1 -m $TAIL
/usr/bin/screen-test -b 404040 -t $timeout -m "Press Button if ${test} test is OK!" &
/usr/bin/screen-test -q Yes,No -b 404040 -t $timeout -m "Is the ${test} test OK?"
/usr/bin/screen-test -b 404040 -t 10 -m "Press Button within 10 seconds!" &
/usr/bin/screen-test -m "Waiting for GPS fix ..." -b 404040 &
/usr/bin/screen-test -m "Testing hardware watchdog ..." -b 404040 &
/usr/bin/screen-test -m "Backlight test" -b 0000ff -t $TIMEOUT &
/usr/bin/screen-test -m "Present card" -b 0000ff -f 30 &
/usr/bin/screen-test -m $DATA -b 0000ff -f 30 &
/usr/bin/screen-test -m "Cover light sensor" -b 0000ff -f 200 &
/usr/bin/screen-test -t 10 -p2 -m "Press Button if the colors are OK!" &
/usr/bin/screen-test -t 10 -p2 -m "Are the colors OK?" -b 404040 -q Yes,No
/usr/bin/screen-test -t 10 -m "Are the colors on the HDMI screen OK?" -q Yes,No -b 404040

/usr/bin/screen-test -t $TIMEOUT -p2 -m "Color test" &
/usr/bin/screen-test -m "Scan barcode" -b 0000ff -f 300 &
/usr/bin/screen-test -q "Yes,No" -t 10 -b 404040 -m "Was the 2nd M2000 eject OK?"

xterm
  -m  : "Message string"
  -t  : timeout          ( 0  : no timeout )
  -b  : background color ( not implemented )
  -f  : font size_t      ( not implemented )
  -q  : response box     ( not implemented )
  -p2 : ???              ( not implemented )
*/

QString aTmp;
QString MyTime;

int TIMEOUT = 10 ;


extern int Value ;

QString aType    ;
QString aTitle   ;
QString aMessage ;
QString aTimeout ;
QString aBeep    ;
QString aBlink   ;

QLabel *c;
QLabel *t;
QLabel *m;


int     iBeepMode;
int     iBlink;
int     iType;

#define PAGE_INFO           0
#define PAGE_YESNO          1
#define PAGE_TOUCHSCREEN    2
#define PAGE_DISPLAY        3
#define PAGE_CARD_READER    4

int CurrentScreen = PAGE_INFO;


int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}


void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    int count;

    count=QApplication::arguments().count();
    if( count != 7)
    {
        printf("abt3000-xterm - v.2019.05.07\n\r\n\r");
        printf("xterm <iType> <aTitle> <aMessage> <iTimeout> <iBeep> <iBlink>\n\r");
        printf("iType  :\n\r");
        printf("    1: PAGE_INFO        \n\r");
        printf("    2: PAGE_YESNO       \n\r");
        printf("    3: PAGE_TOUCHSCREEN \n\r");
        printf("    4: PAGE_DISPLAY     \n\r");
        printf("aTitle\n\r");
        printf("    <String>\n\r");
        printf("aMessage\n\r");
        printf("    <String>\n\r");
        printf("iTimeout:\n\r");
        printf("    <time in seconds>\n\r");
        printf("iBeep  :\n\r");
        printf("    0: No beep      \n\r");
        printf("    1: Start        \n\r");
        printf("    2: Remainder    \n\r");
        printf("iBlink :            \n\r");
        printf("    0: No blink     \n\r");
        printf("    1: Message blink\n\r");
        printf("Return:\n\r");
        printf("    0: Test ok      \n\r");
        printf("    1: Test failed  \n\r");
        printf("  255: timeout      \n\r");
        exit(0);
    }


    ui->setupUi(this);
    ui->MyTitle_4->setVisible(false);
    ui->MyText_3->setVisible(false);
    populate();

    system("echo "" > /tmp/xterm.txt");



    aType    = QApplication::arguments()[1]; // Type
    aTitle   = QApplication::arguments()[2]; // Title
    aMessage = QApplication::arguments()[3]; // Message
    aTimeout = QApplication::arguments()[4]; // timeout
    aBeep    = QApplication::arguments()[5]; //  0: No beep 1:Start 2:Remainder
    aBlink   = QApplication::arguments()[6]; //  0: No blink 1:Message blink


    iBeepMode = aBeep.toInt();
    iBlink    = aBlink.toInt();
    iType     = aType.toInt();


    switch(iType)
    {
        case 1:
                CurrentScreen = PAGE_INFO;
                c=ui->MyCounter_1;
                t=ui->MyTitle_1;
                m=ui->MyText_1;

                break;
        case 2:
                CurrentScreen = PAGE_YESNO;
                c=ui->MyCounter_2;
                t=ui->MyTitle_2;
                m=ui->MyText_2;
                break;
        case 3:
                CurrentScreen = PAGE_TOUCHSCREEN;
                c=ui->MyCounter_4;
                t=ui->MyTitle_4;
                m=ui->MyText_4;
                break;
        case 4:
                CurrentScreen = PAGE_DISPLAY;
                c=ui->MyCounter_3;
                t=ui->MyTitle_3;
                m=ui->MyText_3;
                break;
        case 5:
                CurrentScreen = PAGE_INFO;
                c=ui->MyCounter_1;
                t=ui->MyTitle_1;
                m=ui->MyText_1;
                break;
        default:
                CurrentScreen = PAGE_INFO;
                c=ui->MyCounter_1;
                t=ui->MyTitle_1;
                m=ui->MyText_1;
                break;
    }
    ui->stackedWidget->setCurrentIndex(CurrentScreen);

    if( iBeepMode == 1) system("beep");

    t->setText(aTitle);
    m->setText(aMessage);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update()
{
    static int count = -2;

    if(count == -2) count=aTimeout.toInt()*10;


    MyTime.sprintf("%03d",count/10);
    c->setText(MyTime);

    if(count <=0)
    {
        exit(-1);
        QCoreApplication::quit();
    }


    //=======================================================================================
    // Blink event
    //=======================================================================================
    if(count%20 == 10)
    {
        if( iBlink && iType != 4)
        {
            m->setVisible(true);
        }
        if( iBeepMode == 2) system("beep");
    }

    if(count%20 == 0)
    {
        if( iBlink && iType != 4)
        {
            m->setVisible(false);
        }
    }
    //=======================================================================================
    if (iType == 5)
    {
        read("/tmp/moti.tmp");
        if(aTmp.length()>2)
        {
            aTmp.replace("CARD_B:00,"   ,"");
            aTmp.replace("CARD_F:00,"   ,"");
            aTmp.replace("CARD_M:00,"   ,"");
            aTmp.replace("CARD_X:00"    ,"");
            aTmp.replace("READ_CARD_OK" ,"");
            aTmp.replace("," ,"\n");
            m->setText(aTmp);
        }
    }
	//=======================================================================================
	if (iType == 1)
	{
		read("/tmp/xterm.txt");
		if(aTmp.length()>2)
		{
			m->setText(aTmp);
		}
		system("echo "" > /tmp/xterm.txt");
	}
	//=======================================================================================



    count--;
    QCoreApplication::processEvents();
}


void MainWindow::on_pb_display_yes_pressed()
{
     exit(0);
}

void MainWindow::on_pb_display_no_pressed()
{
     exit(1);
}

void MainWindow::on_pb_response_yes_pressed()
{
    exit(0);
}

void MainWindow::on_pb_response_nos_pressed()
{
    exit(1);
}


//=====================================================================================================================
void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
//=====================================================================================================================
int ul =0;
int ur =0;
int dl =0;
int dr =0;

int NB_COL=4;
int NB_ROW=4;

int PressedButtons[100];
int Started=1;

void MainWindow::clickedSlot()
{
    QPushButton *bt;
    int index;
    int count;
    int i;

    bt = ((QPushButton*)sender());

    if(Started)
    {
        Started = 0;
        for( i= 0; i < (NB_COL*NB_ROW) ; i++)
        {
            PressedButtons[i] = 0;
        }
    }

    index = bt->text().toInt() - 1;
    PressedButtons[index]=1;


    if(bt->text().length()!=1 )
    {
        bt->setStyleSheet("color:white;background-color:rgb(0,255,0)");
        bt->setText("OK");
    }

    count=0;
    for( i= 0; i < (NB_COL*NB_ROW) ; i++)
    {
        if(PressedButtons[i]) count ++;
    }

    if(count >= (NB_COL*NB_ROW))
    {
        exit(0);
    }

    QCoreApplication::processEvents();

};
//=====================================================================================================================
QPushButton *bt ;

void MainWindow::populate()
{
    char aTmp[10];
    int index = 0;

    QGridLayout *grid = ui->gridLayout;

    for (int row = 0; row < NB_ROW; ++row)
    {
            for (int column = 0; column < NB_COL; ++column)
            {
                index = row *NB_COL +column +1;
                aTmp[0]= '0'+index/10;
                aTmp[1]= '0'+index%10;
                aTmp[2]=  0;

                bt = &allBt[index];

                bt->setText(aTmp);
                bt->setStyleSheet("color: white;background-color:blue");
                bt->setFont(QFont("Times", 20));
                bt->setFlat(0);
                bt->setFocusPolicy(Qt::NoFocus);
                bt->setMaximumHeight((640 / NB_COL) -12);
                bt->setMinimumWidth((480 / NB_ROW) -4);
                connect(bt, SIGNAL(pressed()),this, SLOT(clickedSlot()));
                grid->addWidget(bt, row, column);
             }
        }
}
//=====================================================================================================================


