#!/bin/sh



#####################################
#  SWITCH OFF ETHERNET
#####################################
if ip link show eth0 | grep -q "state UP"
then
    #   ip link set eth0 down
    #   echo "eth0 is now down."
    ifconfig eth0 down
    echo "DoClear: ifconfig eth0 down" > /dev/kmsg
fi



#####################################
# Remove barcode initialization
#####################################
if [ -f "/etc/rc5.d/S31barcode-init.sh" ]
then
    echo "DoClear: /etc/rc5.d/S31barcode-init.sh : removing link" > /dev/kmsg
    mv  /etc/rc5.d/S31barcode-init.sh /etc/rc5.d/R31barcode-init.sh
fi


#####################################
# GSM POWER OFF
#####################################
if [ "$(cat /sys/class/leds/gsm_pwr/brightness)" -eq 1 ]; then
    echo 0 > /sys/class/leds/gsm_pwr/brightness
    echo "DoClear: set gsm_pwr/brightness to 0"
fi




#####################################
# PCIE POWER OFF
#####################################
if [ ! -d "/sys/class/leds/pcie_pwr" ]
then
    echo "DoClear: WARNING pcie_pwr does not exist"
else
    if [ "$(cat /sys/class/leds//pcie_pwr/brightness)" -eq 1 ]; then
        echo 0 > /sys/class/leds//pcie_pwr/brightness
        echo "DoClear: set /pcie_pwr/brightness to 0"
    fi
fi



#####################################
# DOM OFF
#####################################




#beep
