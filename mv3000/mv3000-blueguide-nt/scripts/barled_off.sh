#!/bin/sh
#===================================================
# BARLED OFF
#===================================================


#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------




pv3000()
{
    echo 0      > /sys/class/leds/LED0/brightness
    echo 0      > /sys/class/leds/LED1/brightness
    echo 0      > /sys/class/leds/LED2/brightness

    echo 0      > /sys/class/leds/red/brightness
    echo 0      > /sys/class/leds/green/brightness
}

mv3000()
{
    echo 0      > /sys/class/leds/red1/brightness
    echo 0      > /sys/class/leds/red2/brightness

    echo 0      > /sys/class/leds/green1/brightness
    echo 0      > /sys/class/leds/green2/brightness
}


if [ $DEVICE_NAME = "pv3000" ]
then
    pv3000
fi

if [ $DEVICE_NAME = "mv3000" ]
then
    mv3000
fi

