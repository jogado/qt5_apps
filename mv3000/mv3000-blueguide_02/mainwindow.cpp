#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>

QString aTmp;
QString aTmp_barco;

/*
QString aTitle;
QString MyTime;
*/


int NbAdult=0;
int NbChild=0;
float Total=0;


#define MAIN_SCREEN       0
#define SPLASH_SCREEN     1

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label1;
QString Label2;
QString Label3;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);

    ui->Counter->setAttribute   (Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute (Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);

    /*
    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;
    DelayTimer = 50;
    */

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;
    //DelayTimer = 10;



    system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    system("echo \"-----\" > /tmp/text.txt");
    //system("echo \"\" > moti.tmp");
    //system("echo \"\" > rakinda.tmp");


    ui->pb_ValidCard->setVisible(false);
    ui->pb_InvalidCard->setVisible(false);
    system("echo 0 > /sys/class/leds/green1/brightness");
    system("echo 0 > /sys/class/leds/red1/brightness");
    system("echo 0 > /sys/class/leds/green2/brightness");
    system("echo 0 > /sys/class/leds/red2/brightness");


    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

}

MainWindow::~MainWindow()
{
    delete ui;
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

//=============================================================================================
void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }
    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}
//=============================================================================================
void MainWindow::Update_Values()
{
    Total=((float)NbAdult * (float)2.2) + ((float)NbChild * (float)1.5);

    Label1.sprintf("%d",NbAdult);
    ui->AdultCount->setText(Label1);

    Label2.sprintf("%d",NbChild);
    ui->ChildCount->setText(Label2);

    Label3.sprintf("€ %4.2f",Total);
    ui->TotalValue->setText(Label3);

    QCoreApplication::processEvents();
}
//=========================================================================================





int loop=0;
int read_ip=0;
int refresh=0;

int BARLED_IS_ON = 0;
int BARLED_COLOR = 0;

char aBuf[256];
const char *aBuf2;
int BeepIsOn=1;
int init_done=0;
int Timeout=50;




void MainWindow::MainTask_loop()
{
    QString res = "reset";
    int sec,min,hour,days;
    int len;
    unsigned char c;
    int countdown=0;

    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime   = current.toString("hh:mm:ss");
    QString TheDate   = current.toString("dd/MM/yyyy");

    ui->TheTime->setText(TheTime);
    ui->TheDate->setText(TheDate);

    loop++;

    //=============================================================================
    if( DelayTimer)
    {
        DelayTimer--;
        Label2.sprintf("%d",DelayTimer);
        ui->Counter_2->setText(Label2);

        Label2.sprintf("%d",DelayTimer/10);
        ui->free_text->setText(Label2);
    }
    //=============================================================================
    if( DelayTimer == 0 && Total != 0.0)
    {
        NbAdult=0;
        NbChild=0;
        Total= 0.0;

        Update_Values();

        ui->pb_InvalidCard->setVisible(true);
        DelayTimer = 5;
        BARLED_IS_ON = 7; // (7 x 100 ms)

        system("echo 64   > /sys/class/leds/red1/brightness");
        system("echo 64   > /sys/class/leds/red2/brightness");
        system("echo 0   > /sys/class/leds/green1/brightness");
        system("echo 0   > /sys/class/leds/green2/brightness");
        system("echo \"\" > /tmp/moti.data");

        if( BeepIsOn )  system("beep &");
    }
    //=============================================================================


    //=============================================================================
    sec = loop/10;
    min = sec/60;
    hour = min/60;
    days = hour/24;
    Label2.sprintf("(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
    ui->Counter->setText(Label2);
    //=============================================================================


    //=============================================================================
    if( refresh > 0)
    {
        refresh--;
        if(refresh==0)
        {
            ui->free_text->setText("");
        }
    }
    //=============================================================================



    //=============================================================================
    if( read_ip == 0)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
    }
    //=============================================================================
    if( loop%10==3)
    {
            system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    }
    //=============================================================================
    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
    }
    //=============================================================================


    //=============================================================================
    read("/tmp/moti.data");

    if(aTmp.length()>10 )
    {
        refresh=30;

        len=aTmp.length();
        aBuf2=aTmp.toLatin1().data();
        c = aBuf2[len-4];

        system("echo \"\" > /tmp/moti.data");
        DelayTimer = 5;


        if(NbAdult || NbChild )
        {

            NbAdult=0;
            NbChild=0;
            Total= 0.0;

            Update_Values();

            if ( c%2==0 )
            {
                ui->pb_InvalidCard->setVisible(true);
                system("echo 0 > /sys/class/leds/green1/brightness");
                system("echo 0 > /sys/class/leds/green2/brightness");
                system("echo 255 > /sys/class/leds/red1/brightness");
                system("echo 255 > /sys/class/leds/red2/brightness");
                system("echo \"\" > /tmp/moti.data");
                if( BeepIsOn )  system("beep &");
            }

            if ( c%2==1 )
            {
                ui->pb_ValidCard->setVisible(true);
                system("echo 255 > /sys/class/leds/green1/brightness");
                system("echo 255 > /sys/class/leds/green2/brightness");
                system("echo 0 > /sys/class/leds/red1/brightness");
                system("echo 0 > /sys/class/leds/red2/brightness");
                system("echo \"\" > /tmp/moti.data");
                if( BeepIsOn )  system("beep &");
            }
        }

        BARLED_IS_ON = 7; // (7 x 100 ms)

        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
        system("echo \"\" > /tmp/moti.data");
        QCoreApplication::processEvents();
    }
    //=============================================================================
    if( BARLED_IS_ON > 0 )
    {
         BARLED_IS_ON--;
         if( BARLED_IS_ON == 0 )
         {
             ui->pb_InvalidCard->setVisible(false);
             ui->pb_ValidCard->setVisible(false);
             system("echo 0 > /sys/class/leds/green1/brightness");
             system("echo 0 > /sys/class/leds/red1/brightness");
             system("echo 0 > /sys/class/leds/green2/brightness");
             system("echo 0 > /sys/class/leds/red2/brightness");
          }
    }
    //=============================================================================
    system("echo \"\" > /tmp/moti.data");
    //=============================================================================
    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::on_pb_adult_minus_clicked()
{
    DelayTimer = 50;

    if(NbAdult<=0) return ;
    NbAdult--;

    Update_Values();

    system("/usr/bin/start_moti.sh &");
    system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_adult_plus_clicked()
{
    DelayTimer = 50;

    if(NbAdult>=9) return;

    NbAdult++;

    Update_Values();

    system("/usr/bin/start_moti.sh &");
    system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_child_minus_clicked()
{
    DelayTimer = 50;

    if(NbChild<=0) return;

    NbChild--;

    Update_Values();

    system("/usr/bin/start_moti.sh &");
    system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_child_plus_clicked()
{
    DelayTimer = 50;

    if(NbChild>=9) return;

    NbChild++;

    Update_Values();

    system("/usr/bin/start_moti.sh &");
    system("beep -f 2000 -l 10 &");
}
//=============================================================================
