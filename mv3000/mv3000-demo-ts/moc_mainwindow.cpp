/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[29];
    char stringdata0[509];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 18), // "MasterTimer_update"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 13), // "MainTask_loop"
QT_MOC_LITERAL(4, 45, 25), // "on_pb_adult_minus_clicked"
QT_MOC_LITERAL(5, 71, 24), // "on_pb_adult_plus_clicked"
QT_MOC_LITERAL(6, 96, 25), // "on_pb_child_minus_clicked"
QT_MOC_LITERAL(7, 122, 24), // "on_pb_child_plus_clicked"
QT_MOC_LITERAL(8, 147, 7), // "Led_RED"
QT_MOC_LITERAL(9, 155, 9), // "Led_GREEN"
QT_MOC_LITERAL(10, 165, 7), // "Led_OFF"
QT_MOC_LITERAL(11, 173, 18), // "on_pb_moti_clicked"
QT_MOC_LITERAL(12, 192, 21), // "on_pb_barcode_clicked"
QT_MOC_LITERAL(13, 214, 20), // "on_pb_reboot_clicked"
QT_MOC_LITERAL(14, 235, 21), // "Clear_Service_buttons"
QT_MOC_LITERAL(15, 257, 11), // "read_header"
QT_MOC_LITERAL(16, 269, 6), // "header"
QT_MOC_LITERAL(17, 276, 17), // "Clear_Fan_buttons"
QT_MOC_LITERAL(18, 294, 21), // "on_pb_fan_off_clicked"
QT_MOC_LITERAL(19, 316, 20), // "on_pb_fan_10_clicked"
QT_MOC_LITERAL(20, 337, 20), // "on_pb_fan_50_clicked"
QT_MOC_LITERAL(21, 358, 21), // "on_pb_fan_100_clicked"
QT_MOC_LITERAL(22, 380, 22), // "on_pb_fan_auto_clicked"
QT_MOC_LITERAL(23, 403, 22), // "on_pb_heat_off_clicked"
QT_MOC_LITERAL(24, 426, 21), // "on_pb_heat_on_clicked"
QT_MOC_LITERAL(25, 448, 19), // "Update_lookup_table"
QT_MOC_LITERAL(26, 468, 19), // "Get_fan_temperature"
QT_MOC_LITERAL(27, 488, 16), // "Get_fan_register"
QT_MOC_LITERAL(28, 505, 3) // "reg"

    },
    "MainWindow\0MasterTimer_update\0\0"
    "MainTask_loop\0on_pb_adult_minus_clicked\0"
    "on_pb_adult_plus_clicked\0"
    "on_pb_child_minus_clicked\0"
    "on_pb_child_plus_clicked\0Led_RED\0"
    "Led_GREEN\0Led_OFF\0on_pb_moti_clicked\0"
    "on_pb_barcode_clicked\0on_pb_reboot_clicked\0"
    "Clear_Service_buttons\0read_header\0"
    "header\0Clear_Fan_buttons\0on_pb_fan_off_clicked\0"
    "on_pb_fan_10_clicked\0on_pb_fan_50_clicked\0"
    "on_pb_fan_100_clicked\0on_pb_fan_auto_clicked\0"
    "on_pb_heat_off_clicked\0on_pb_heat_on_clicked\0"
    "Update_lookup_table\0Get_fan_temperature\0"
    "Get_fan_register\0reg"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x08 /* Private */,
       3,    0,  140,    2, 0x08 /* Private */,
       4,    0,  141,    2, 0x08 /* Private */,
       5,    0,  142,    2, 0x08 /* Private */,
       6,    0,  143,    2, 0x08 /* Private */,
       7,    0,  144,    2, 0x08 /* Private */,
       8,    0,  145,    2, 0x08 /* Private */,
       9,    0,  146,    2, 0x08 /* Private */,
      10,    0,  147,    2, 0x08 /* Private */,
      11,    0,  148,    2, 0x08 /* Private */,
      12,    0,  149,    2, 0x08 /* Private */,
      13,    0,  150,    2, 0x08 /* Private */,
      14,    0,  151,    2, 0x08 /* Private */,
      15,    1,  152,    2, 0x08 /* Private */,
      17,    0,  155,    2, 0x08 /* Private */,
      18,    0,  156,    2, 0x08 /* Private */,
      19,    0,  157,    2, 0x08 /* Private */,
      20,    0,  158,    2, 0x08 /* Private */,
      21,    0,  159,    2, 0x08 /* Private */,
      22,    0,  160,    2, 0x08 /* Private */,
      23,    0,  161,    2, 0x08 /* Private */,
      24,    0,  162,    2, 0x08 /* Private */,
      25,    0,  163,    2, 0x08 /* Private */,
      26,    0,  164,    2, 0x08 /* Private */,
      27,    1,  165,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int,
    QMetaType::Int, QMetaType::Int,   28,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->MasterTimer_update(); break;
        case 1: _t->MainTask_loop(); break;
        case 2: _t->on_pb_adult_minus_clicked(); break;
        case 3: _t->on_pb_adult_plus_clicked(); break;
        case 4: _t->on_pb_child_minus_clicked(); break;
        case 5: _t->on_pb_child_plus_clicked(); break;
        case 6: _t->Led_RED(); break;
        case 7: _t->Led_GREEN(); break;
        case 8: _t->Led_OFF(); break;
        case 9: _t->on_pb_moti_clicked(); break;
        case 10: _t->on_pb_barcode_clicked(); break;
        case 11: _t->on_pb_reboot_clicked(); break;
        case 12: _t->Clear_Service_buttons(); break;
        case 13: _t->read_header((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->Clear_Fan_buttons(); break;
        case 15: _t->on_pb_fan_off_clicked(); break;
        case 16: _t->on_pb_fan_10_clicked(); break;
        case 17: _t->on_pb_fan_50_clicked(); break;
        case 18: _t->on_pb_fan_100_clicked(); break;
        case 19: _t->on_pb_fan_auto_clicked(); break;
        case 20: _t->on_pb_heat_off_clicked(); break;
        case 21: _t->on_pb_heat_on_clicked(); break;
        case 22: _t->Update_lookup_table(); break;
        case 23: { int _r = _t->Get_fan_temperature();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 24: { int _r = _t->Get_fan_register((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
