/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *MainScreen;
    QLabel *Counter;
    QLabel *ip_address;
    QLabel *free_text;
    QPushButton *pushButton;
    QPushButton *pb_adult_plus;
    QPushButton *pb_child_plus;
    QPushButton *pb_adult_minus;
    QPushButton *pb_child_minus;
    QLabel *ChildCount;
    QLabel *AdultCount;
    QLabel *TotalValue;
    QLabel *Counter_2;
    QLabel *TheDate;
    QLabel *TheTime;
    QPushButton *pb_ValidCard;
    QPushButton *pb_InvalidCard;
    QLabel *label_header_01;
    QLabel *label_header_02;
    QLabel *label_header_03;
    QLabel *label_header_04;
    QLabel *label_header_05;
    QLabel *label_header_06;
    QLabel *label_header_07;
    QLabel *label_header_08;
    QLabel *label_header_00;
    QLabel *label_header_09;
    QWidget *SplashScreen;
    QPushButton *pb_reboot;
    QPushButton *pb_moti;
    QPushButton *pb_barcode;
    QWidget *Service;
    QPushButton *pb_header;
    QLabel *Counter_3;
    QGroupBox *groupBox;
    QPushButton *pb_heat_off;
    QPushButton *pb_heat_on;
    QGroupBox *groupBox_2;
    QPushButton *pb_fan_off;
    QPushButton *pb_fan_50;
    QPushButton *pb_fan_100;
    QPushButton *pb_fan_auto;
    QPushButton *pb_fan_10;
    QGroupBox *groupBox_3;
    QLabel *label_input_2;
    QLabel *label_input_1;
    QLabel *label_fan_pwm;
    QLabel *label_fan_temperature;
    QLabel *label_cpu_temp;
    QLabel *label_i2c_temp;
    QGroupBox *groupBox_4;
    QLabel *label_lookup_14;
    QLabel *label_lookup_11;
    QLabel *label_lookup_05;
    QLabel *label_lookup_04;
    QLabel *label_lookup_08;
    QLabel *label_lookup_10;
    QLabel *label_lookup_07;
    QLabel *label_lookup_09;
    QLabel *label_lookup_13;
    QLabel *label_lookup_02;
    QLabel *label_lookup_03;
    QLabel *label_lookup_12;
    QLabel *label_lookup_01;
    QLabel *label_lookup_06;
    QLabel *label_lookup_15;
    QLabel *label_lookup_16;
    QLabel *label_lookup_17;
    QLabel *label_lookup_18;
    QLabel *label_lookup_19;
    QLabel *label_lookup_20;
    QLabel *label_lookup_21;
    QLabel *label_lookup_22;
    QLabel *label_lookup_23;
    QLabel *label_lookup_24;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(480, 640);
        MainWindow->setStyleSheet(QString::fromUtf8("color:black;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 480, 640));
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QString::fromUtf8(""));
        MainScreen = new QWidget();
        MainScreen->setObjectName(QString::fromUtf8("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QString::fromUtf8(""));
        Counter = new QLabel(MainScreen);
        Counter->setObjectName(QString::fromUtf8("Counter"));
        Counter->setEnabled(true);
        Counter->setGeometry(QRect(321, 30, 140, 20));
        QFont font;
        font.setFamily(QString::fromUtf8("Liberation Sans"));
        font.setPointSize(12);
        font.setItalic(false);
        Counter->setFont(font);
        Counter->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        Counter->setLineWidth(2);
        Counter->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        ip_address = new QLabel(MainScreen);
        ip_address->setObjectName(QString::fromUtf8("ip_address"));
        ip_address->setGeometry(QRect(290, 14, 171, 20));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Liberation Sans"));
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setItalic(false);
        ip_address->setFont(font1);
        ip_address->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        ip_address->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        free_text = new QLabel(MainScreen);
        free_text->setObjectName(QString::fromUtf8("free_text"));
        free_text->setGeometry(QRect(190, 520, 31, 21));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Liberation Sans"));
        font2.setPointSize(10);
        font2.setBold(true);
        free_text->setFont(font2);
        free_text->setStyleSheet(QString::fromUtf8("color:blue;\n"
""));
        free_text->setLineWidth(2);
        free_text->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        free_text->setWordWrap(true);
        pushButton = new QPushButton(MainScreen);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setEnabled(false);
        pushButton->setGeometry(QRect(0, 0, 480, 640));
        pushButton->setStyleSheet(QString::fromUtf8("border: none;\n"
"background-image: url(:/bmp/main_480x640_TS_empty.bmp);"));
        pb_adult_plus = new QPushButton(MainScreen);
        pb_adult_plus->setObjectName(QString::fromUtf8("pb_adult_plus"));
        pb_adult_plus->setGeometry(QRect(352, 203, 79, 78));
        pb_adult_plus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_plus.bmp);\n"
"border: none;"));
        pb_child_plus = new QPushButton(MainScreen);
        pb_child_plus->setObjectName(QString::fromUtf8("pb_child_plus"));
        pb_child_plus->setGeometry(QRect(350, 305, 79, 78));
        pb_child_plus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_plus.bmp);\n"
"border: none;"));
        pb_adult_minus = new QPushButton(MainScreen);
        pb_adult_minus->setObjectName(QString::fromUtf8("pb_adult_minus"));
        pb_adult_minus->setGeometry(QRect(50, 203, 79, 78));
        pb_adult_minus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_min.bmp);\n"
"border: none;"));
        pb_child_minus = new QPushButton(MainScreen);
        pb_child_minus->setObjectName(QString::fromUtf8("pb_child_minus"));
        pb_child_minus->setGeometry(QRect(50, 306, 80, 78));
        pb_child_minus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_min.bmp);\n"
"border: none;"));
        ChildCount = new QLabel(MainScreen);
        ChildCount->setObjectName(QString::fromUtf8("ChildCount"));
        ChildCount->setEnabled(true);
        ChildCount->setGeometry(QRect(263, 308, 80, 70));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Liberation Sans"));
        font3.setPointSize(35);
        font3.setBold(true);
        font3.setItalic(false);
        ChildCount->setFont(font3);
        ChildCount->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        ChildCount->setLineWidth(2);
        ChildCount->setAlignment(Qt::AlignCenter);
        AdultCount = new QLabel(MainScreen);
        AdultCount->setObjectName(QString::fromUtf8("AdultCount"));
        AdultCount->setEnabled(true);
        AdultCount->setGeometry(QRect(263, 212, 80, 60));
        AdultCount->setFont(font3);
        AdultCount->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        AdultCount->setLineWidth(2);
        AdultCount->setAlignment(Qt::AlignCenter);
        TotalValue = new QLabel(MainScreen);
        TotalValue->setObjectName(QString::fromUtf8("TotalValue"));
        TotalValue->setEnabled(true);
        TotalValue->setGeometry(QRect(117, 434, 220, 40));
        TotalValue->setFont(font3);
        TotalValue->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        TotalValue->setLineWidth(2);
        TotalValue->setAlignment(Qt::AlignCenter);
        Counter_2 = new QLabel(MainScreen);
        Counter_2->setObjectName(QString::fromUtf8("Counter_2"));
        Counter_2->setEnabled(true);
        Counter_2->setGeometry(QRect(217, 520, 30, 20));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Liberation Sans"));
        font4.setPointSize(10);
        font4.setItalic(false);
        Counter_2->setFont(font4);
        Counter_2->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        Counter_2->setLineWidth(2);
        Counter_2->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        TheDate = new QLabel(MainScreen);
        TheDate->setObjectName(QString::fromUtf8("TheDate"));
        TheDate->setEnabled(true);
        TheDate->setGeometry(QRect(200, 14, 100, 20));
        TheDate->setFont(font);
        TheDate->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
""));
        TheDate->setLineWidth(2);
        TheDate->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        TheTime = new QLabel(MainScreen);
        TheTime->setObjectName(QString::fromUtf8("TheTime"));
        TheTime->setEnabled(true);
        TheTime->setGeometry(QRect(200, 36, 100, 20));
        TheTime->setFont(font);
        TheTime->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        TheTime->setLineWidth(2);
        TheTime->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        pb_ValidCard = new QPushButton(MainScreen);
        pb_ValidCard->setObjectName(QString::fromUtf8("pb_ValidCard"));
        pb_ValidCard->setGeometry(QRect(10, 546, 461, 84));
        pb_ValidCard->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_valid.bmp);\n"
"border: none;"));
        pb_InvalidCard = new QPushButton(MainScreen);
        pb_InvalidCard->setObjectName(QString::fromUtf8("pb_InvalidCard"));
        pb_InvalidCard->setGeometry(QRect(10, 546, 461, 84));
        pb_InvalidCard->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_invalid.bmp);\n"
"border: none;"));
        label_header_01 = new QLabel(MainScreen);
        label_header_01->setObjectName(QString::fromUtf8("label_header_01"));
        label_header_01->setGeometry(QRect(18, 13, 281, 40));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Liberation Sans"));
        font5.setPointSize(28);
        font5.setBold(true);
        font5.setItalic(false);
        label_header_01->setFont(font5);
        label_header_01->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_header_01->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_header_02 = new QLabel(MainScreen);
        label_header_02->setObjectName(QString::fromUtf8("label_header_02"));
        label_header_02->setGeometry(QRect(15, 80, 450, 41));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Liberation Sans"));
        font6.setPointSize(24);
        font6.setBold(true);
        font6.setItalic(false);
        label_header_02->setFont(font6);
        label_header_02->setStyleSheet(QString::fromUtf8("color:rgb(255, 255, 255\n"
")\n"
"\n"
""));
        label_header_02->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_header_03 = new QLabel(MainScreen);
        label_header_03->setObjectName(QString::fromUtf8("label_header_03"));
        label_header_03->setGeometry(QRect(14, 141, 460, 50));
        QFont font7;
        font7.setFamily(QString::fromUtf8("Liberation Sans"));
        font7.setPointSize(36);
        font7.setBold(true);
        font7.setItalic(false);
        label_header_03->setFont(font7);
        label_header_03->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_header_03->setAlignment(Qt::AlignCenter);
        label_header_04 = new QLabel(MainScreen);
        label_header_04->setObjectName(QString::fromUtf8("label_header_04"));
        label_header_04->setGeometry(QRect(137, 218, 121, 20));
        QFont font8;
        font8.setFamily(QString::fromUtf8("Liberation Sans"));
        font8.setPointSize(18);
        font8.setBold(true);
        font8.setItalic(false);
        label_header_04->setFont(font8);
        label_header_04->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_header_04->setAlignment(Qt::AlignCenter);
        label_header_05 = new QLabel(MainScreen);
        label_header_05->setObjectName(QString::fromUtf8("label_header_05"));
        label_header_05->setGeometry(QRect(126, 247, 121, 20));
        label_header_05->setFont(font8);
        label_header_05->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_header_05->setAlignment(Qt::AlignCenter);
        label_header_06 = new QLabel(MainScreen);
        label_header_06->setObjectName(QString::fromUtf8("label_header_06"));
        label_header_06->setGeometry(QRect(134, 320, 121, 20));
        label_header_06->setFont(font8);
        label_header_06->setContextMenuPolicy(Qt::NoContextMenu);
        label_header_06->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_header_06->setAlignment(Qt::AlignCenter);
        label_header_07 = new QLabel(MainScreen);
        label_header_07->setObjectName(QString::fromUtf8("label_header_07"));
        label_header_07->setGeometry(QRect(127, 349, 121, 20));
        label_header_07->setFont(font8);
        label_header_07->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_header_07->setAlignment(Qt::AlignCenter);
        label_header_08 = new QLabel(MainScreen);
        label_header_08->setObjectName(QString::fromUtf8("label_header_08"));
        label_header_08->setGeometry(QRect(134, 410, 210, 30));
        QFont font9;
        font9.setFamily(QString::fromUtf8("Liberation Sans"));
        font9.setPointSize(14);
        font9.setBold(true);
        font9.setItalic(false);
        label_header_08->setFont(font9);
        label_header_08->setStyleSheet(QString::fromUtf8("color:rgb(255, 255, 255\n"
")\n"
"\n"
""));
        label_header_08->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_header_00 = new QLabel(MainScreen);
        label_header_00->setObjectName(QString::fromUtf8("label_header_00"));
        label_header_00->setGeometry(QRect(20, 400, 91, 16));
        QFont font10;
        font10.setFamily(QString::fromUtf8("Liberation Sans"));
        font10.setPointSize(10);
        font10.setBold(true);
        font10.setItalic(false);
        label_header_00->setFont(font10);
        label_header_00->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_header_00->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_header_09 = new QLabel(MainScreen);
        label_header_09->setObjectName(QString::fromUtf8("label_header_09"));
        label_header_09->setGeometry(QRect(20, 420, 81, 16));
        label_header_09->setFont(font10);
        label_header_09->setStyleSheet(QString::fromUtf8("color:rgb(255, 255, 255\n"
")\n"
"\n"
""));
        label_header_09->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        stackedWidget->addWidget(MainScreen);
        pushButton->raise();
        free_text->raise();
        ip_address->raise();
        ChildCount->raise();
        AdultCount->raise();
        TotalValue->raise();
        Counter_2->raise();
        TheDate->raise();
        TheTime->raise();
        pb_ValidCard->raise();
        pb_InvalidCard->raise();
        pb_adult_plus->raise();
        pb_child_plus->raise();
        pb_adult_minus->raise();
        pb_child_minus->raise();
        label_header_01->raise();
        label_header_02->raise();
        label_header_03->raise();
        label_header_04->raise();
        label_header_05->raise();
        label_header_06->raise();
        label_header_07->raise();
        label_header_08->raise();
        label_header_00->raise();
        label_header_09->raise();
        Counter->raise();
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QString::fromUtf8("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QString::fromUtf8("image:url(:/bmp/IT-Trans.bmp)"));
        pb_reboot = new QPushButton(SplashScreen);
        pb_reboot->setObjectName(QString::fromUtf8("pb_reboot"));
        pb_reboot->setGeometry(QRect(10, 290, 461, 76));
        QFont font11;
        font11.setFamily(QString::fromUtf8("Liberation Sans"));
        font11.setPointSize(23);
        font11.setBold(true);
        pb_reboot->setFont(font11);
        pb_reboot->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_moti = new QPushButton(SplashScreen);
        pb_moti->setObjectName(QString::fromUtf8("pb_moti"));
        pb_moti->setGeometry(QRect(10, 90, 461, 76));
        pb_moti->setFont(font11);
        pb_moti->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_moti->setFlat(false);
        pb_barcode = new QPushButton(SplashScreen);
        pb_barcode->setObjectName(QString::fromUtf8("pb_barcode"));
        pb_barcode->setGeometry(QRect(10, 190, 461, 76));
        pb_barcode->setFont(font11);
        pb_barcode->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        stackedWidget->addWidget(SplashScreen);
        Service = new QWidget();
        Service->setObjectName(QString::fromUtf8("Service"));
        pb_header = new QPushButton(Service);
        pb_header->setObjectName(QString::fromUtf8("pb_header"));
        pb_header->setGeometry(QRect(0, 0, 480, 40));
        pb_header->setFont(font11);
        pb_header->setStyleSheet(QString::fromUtf8("background-color: rgb(20, 60, 118);\n"
"border: none;\n"
"color:white;\n"
"\n"
""));
        Counter_3 = new QLabel(Service);
        Counter_3->setObjectName(QString::fromUtf8("Counter_3"));
        Counter_3->setEnabled(true);
        Counter_3->setGeometry(QRect(420, 10, 41, 21));
        QFont font12;
        font12.setFamily(QString::fromUtf8("Liberation Sans"));
        font12.setPointSize(12);
        font12.setBold(true);
        font12.setItalic(false);
        Counter_3->setFont(font12);
        Counter_3->setStyleSheet(QString::fromUtf8("color:white"));
        Counter_3->setLineWidth(2);
        Counter_3->setAlignment(Qt::AlignCenter);
        groupBox = new QGroupBox(Service);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(250, 220, 111, 402));
        QFont font13;
        font13.setFamily(QString::fromUtf8("Liberation Sans"));
        font13.setPointSize(18);
        font13.setBold(true);
        groupBox->setFont(font13);
        groupBox->setStyleSheet(QString::fromUtf8("background-color: rgb(252, 175, 62);\n"
"\n"
"border: 1px solid rgb(32, 74, 135);"));
        groupBox->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);
        pb_heat_off = new QPushButton(groupBox);
        pb_heat_off->setObjectName(QString::fromUtf8("pb_heat_off"));
        pb_heat_off->setGeometry(QRect(10, 40, 90, 80));
        pb_heat_off->setFont(font11);
        pb_heat_off->setStyleSheet(QString::fromUtf8("background-color: rgb(32, 74, 135);\n"
"border-color: white;\n"
"color: white;\n"
""));
        pb_heat_on = new QPushButton(groupBox);
        pb_heat_on->setObjectName(QString::fromUtf8("pb_heat_on"));
        pb_heat_on->setGeometry(QRect(10, 130, 90, 80));
        pb_heat_on->setFont(font11);
        pb_heat_on->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        groupBox_2 = new QGroupBox(Service);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(367, 42, 110, 581));
        groupBox_2->setFont(font13);
        groupBox_2->setAutoFillBackground(false);
        groupBox_2->setStyleSheet(QString::fromUtf8("background-color: rgb(207, 250, 246);\n"
"border: 1px solid rgb(32, 74, 135);"));
        groupBox_2->setAlignment(Qt::AlignCenter);
        pb_fan_off = new QPushButton(groupBox_2);
        pb_fan_off->setObjectName(QString::fromUtf8("pb_fan_off"));
        pb_fan_off->setGeometry(QRect(10, 64, 90, 80));
        pb_fan_off->setFont(font11);
        pb_fan_off->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_fan_50 = new QPushButton(groupBox_2);
        pb_fan_50->setObjectName(QString::fromUtf8("pb_fan_50"));
        pb_fan_50->setGeometry(QRect(10, 258, 90, 80));
        pb_fan_50->setFont(font11);
        pb_fan_50->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_fan_100 = new QPushButton(groupBox_2);
        pb_fan_100->setObjectName(QString::fromUtf8("pb_fan_100"));
        pb_fan_100->setGeometry(QRect(10, 348, 90, 80));
        pb_fan_100->setFont(font11);
        pb_fan_100->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_fan_auto = new QPushButton(groupBox_2);
        pb_fan_auto->setObjectName(QString::fromUtf8("pb_fan_auto"));
        pb_fan_auto->setGeometry(QRect(10, 438, 90, 80));
        pb_fan_auto->setFont(font11);
        pb_fan_auto->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_fan_10 = new QPushButton(groupBox_2);
        pb_fan_10->setObjectName(QString::fromUtf8("pb_fan_10"));
        pb_fan_10->setGeometry(QRect(10, 168, 90, 80));
        pb_fan_10->setFont(font11);
        pb_fan_10->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        groupBox_3 = new QGroupBox(Service);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(-1, 43, 351, 171));
        groupBox_3->setFont(font13);
        groupBox_3->setStyleSheet(QString::fromUtf8("background-color: rgb(215, 247, 183);"));
        groupBox_3->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);
        label_input_2 = new QLabel(groupBox_3);
        label_input_2->setObjectName(QString::fromUtf8("label_input_2"));
        label_input_2->setGeometry(QRect(77, 94, 200, 20));
        QFont font14;
        font14.setFamily(QString::fromUtf8("Liberation Mono"));
        font14.setPointSize(16);
        font14.setBold(false);
        font14.setItalic(false);
        label_input_2->setFont(font14);
        label_input_2->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_input_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_input_1 = new QLabel(groupBox_3);
        label_input_1->setObjectName(QString::fromUtf8("label_input_1"));
        label_input_1->setGeometry(QRect(77, 74, 200, 20));
        label_input_1->setFont(font14);
        label_input_1->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_input_1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_fan_pwm = new QLabel(groupBox_3);
        label_fan_pwm->setObjectName(QString::fromUtf8("label_fan_pwm"));
        label_fan_pwm->setGeometry(QRect(77, 34, 200, 20));
        label_fan_pwm->setFont(font14);
        label_fan_pwm->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_fan_pwm->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_fan_temperature = new QLabel(groupBox_3);
        label_fan_temperature->setObjectName(QString::fromUtf8("label_fan_temperature"));
        label_fan_temperature->setGeometry(QRect(77, 54, 200, 20));
        label_fan_temperature->setFont(font14);
        label_fan_temperature->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_fan_temperature->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_cpu_temp = new QLabel(groupBox_3);
        label_cpu_temp->setObjectName(QString::fromUtf8("label_cpu_temp"));
        label_cpu_temp->setGeometry(QRect(77, 114, 200, 20));
        label_cpu_temp->setFont(font14);
        label_cpu_temp->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_cpu_temp->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_i2c_temp = new QLabel(groupBox_3);
        label_i2c_temp->setObjectName(QString::fromUtf8("label_i2c_temp"));
        label_i2c_temp->setGeometry(QRect(77, 134, 200, 20));
        label_i2c_temp->setFont(font14);
        label_i2c_temp->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        label_i2c_temp->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        groupBox_4 = new QGroupBox(Service);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(1, 220, 181, 400));
        groupBox_4->setFont(font13);
        groupBox_4->setStyleSheet(QString::fromUtf8("background-color: rgb(211, 215, 207);\n"
""));
        groupBox_4->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);
        label_lookup_14 = new QLabel(groupBox_4);
        label_lookup_14->setObjectName(QString::fromUtf8("label_lookup_14"));
        label_lookup_14->setGeometry(QRect(27, 229, 130, 15));
        QFont font15;
        font15.setFamily(QString::fromUtf8("Liberation Mono"));
        font15.setPointSize(12);
        font15.setBold(false);
        font15.setItalic(false);
        label_lookup_14->setFont(font15);
        label_lookup_14->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_14->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_11 = new QLabel(groupBox_4);
        label_lookup_11->setObjectName(QString::fromUtf8("label_lookup_11"));
        label_lookup_11->setGeometry(QRect(27, 184, 130, 15));
        label_lookup_11->setFont(font15);
        label_lookup_11->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_11->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_05 = new QLabel(groupBox_4);
        label_lookup_05->setObjectName(QString::fromUtf8("label_lookup_05"));
        label_lookup_05->setGeometry(QRect(27, 94, 130, 15));
        label_lookup_05->setFont(font15);
        label_lookup_05->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: red\n"
"\n"
"\n"
""));
        label_lookup_05->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_04 = new QLabel(groupBox_4);
        label_lookup_04->setObjectName(QString::fromUtf8("label_lookup_04"));
        label_lookup_04->setGeometry(QRect(27, 79, 130, 15));
        label_lookup_04->setFont(font15);
        label_lookup_04->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_04->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_08 = new QLabel(groupBox_4);
        label_lookup_08->setObjectName(QString::fromUtf8("label_lookup_08"));
        label_lookup_08->setGeometry(QRect(27, 139, 130, 15));
        label_lookup_08->setFont(font15);
        label_lookup_08->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_08->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_10 = new QLabel(groupBox_4);
        label_lookup_10->setObjectName(QString::fromUtf8("label_lookup_10"));
        label_lookup_10->setGeometry(QRect(27, 169, 130, 15));
        label_lookup_10->setFont(font15);
        label_lookup_10->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_10->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_07 = new QLabel(groupBox_4);
        label_lookup_07->setObjectName(QString::fromUtf8("label_lookup_07"));
        label_lookup_07->setGeometry(QRect(27, 124, 130, 15));
        label_lookup_07->setFont(font15);
        label_lookup_07->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_07->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_09 = new QLabel(groupBox_4);
        label_lookup_09->setObjectName(QString::fromUtf8("label_lookup_09"));
        label_lookup_09->setGeometry(QRect(27, 154, 130, 15));
        label_lookup_09->setFont(font15);
        label_lookup_09->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_09->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_13 = new QLabel(groupBox_4);
        label_lookup_13->setObjectName(QString::fromUtf8("label_lookup_13"));
        label_lookup_13->setGeometry(QRect(27, 214, 130, 15));
        label_lookup_13->setFont(font15);
        label_lookup_13->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_13->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_02 = new QLabel(groupBox_4);
        label_lookup_02->setObjectName(QString::fromUtf8("label_lookup_02"));
        label_lookup_02->setGeometry(QRect(27, 49, 130, 15));
        label_lookup_02->setFont(font15);
        label_lookup_02->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_02->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_03 = new QLabel(groupBox_4);
        label_lookup_03->setObjectName(QString::fromUtf8("label_lookup_03"));
        label_lookup_03->setGeometry(QRect(27, 64, 130, 15));
        label_lookup_03->setFont(font15);
        label_lookup_03->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_03->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_12 = new QLabel(groupBox_4);
        label_lookup_12->setObjectName(QString::fromUtf8("label_lookup_12"));
        label_lookup_12->setGeometry(QRect(27, 199, 130, 15));
        label_lookup_12->setFont(font15);
        label_lookup_12->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_12->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_01 = new QLabel(groupBox_4);
        label_lookup_01->setObjectName(QString::fromUtf8("label_lookup_01"));
        label_lookup_01->setGeometry(QRect(27, 34, 130, 15));
        label_lookup_01->setFont(font15);
        label_lookup_01->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_01->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_06 = new QLabel(groupBox_4);
        label_lookup_06->setObjectName(QString::fromUtf8("label_lookup_06"));
        label_lookup_06->setGeometry(QRect(27, 109, 130, 15));
        label_lookup_06->setFont(font15);
        label_lookup_06->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white"));
        label_lookup_06->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_15 = new QLabel(groupBox_4);
        label_lookup_15->setObjectName(QString::fromUtf8("label_lookup_15"));
        label_lookup_15->setGeometry(QRect(27, 244, 130, 15));
        label_lookup_15->setFont(font15);
        label_lookup_15->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_15->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_16 = new QLabel(groupBox_4);
        label_lookup_16->setObjectName(QString::fromUtf8("label_lookup_16"));
        label_lookup_16->setGeometry(QRect(27, 259, 130, 15));
        label_lookup_16->setFont(font15);
        label_lookup_16->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_16->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_17 = new QLabel(groupBox_4);
        label_lookup_17->setObjectName(QString::fromUtf8("label_lookup_17"));
        label_lookup_17->setGeometry(QRect(27, 274, 130, 15));
        label_lookup_17->setFont(font15);
        label_lookup_17->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_17->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_18 = new QLabel(groupBox_4);
        label_lookup_18->setObjectName(QString::fromUtf8("label_lookup_18"));
        label_lookup_18->setGeometry(QRect(27, 289, 130, 15));
        label_lookup_18->setFont(font15);
        label_lookup_18->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_18->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_19 = new QLabel(groupBox_4);
        label_lookup_19->setObjectName(QString::fromUtf8("label_lookup_19"));
        label_lookup_19->setGeometry(QRect(27, 304, 130, 15));
        label_lookup_19->setFont(font15);
        label_lookup_19->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_19->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_20 = new QLabel(groupBox_4);
        label_lookup_20->setObjectName(QString::fromUtf8("label_lookup_20"));
        label_lookup_20->setGeometry(QRect(27, 319, 130, 15));
        label_lookup_20->setFont(font15);
        label_lookup_20->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_20->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_21 = new QLabel(groupBox_4);
        label_lookup_21->setObjectName(QString::fromUtf8("label_lookup_21"));
        label_lookup_21->setGeometry(QRect(27, 334, 130, 15));
        label_lookup_21->setFont(font15);
        label_lookup_21->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_21->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_22 = new QLabel(groupBox_4);
        label_lookup_22->setObjectName(QString::fromUtf8("label_lookup_22"));
        label_lookup_22->setGeometry(QRect(27, 349, 130, 15));
        label_lookup_22->setFont(font15);
        label_lookup_22->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_22->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_23 = new QLabel(groupBox_4);
        label_lookup_23->setObjectName(QString::fromUtf8("label_lookup_23"));
        label_lookup_23->setGeometry(QRect(27, 364, 130, 15));
        label_lookup_23->setFont(font15);
        label_lookup_23->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_23->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_lookup_24 = new QLabel(groupBox_4);
        label_lookup_24->setObjectName(QString::fromUtf8("label_lookup_24"));
        label_lookup_24->setGeometry(QRect(27, 379, 130, 15));
        label_lookup_24->setFont(font15);
        label_lookup_24->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118);\n"
"background-color: white\n"
"\n"
"\n"
""));
        label_lookup_24->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        stackedWidget->addWidget(Service);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        Counter->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        ip_address->setText(QCoreApplication::translate("MainWindow", "000.000.000.000", nullptr));
        free_text->setText(QCoreApplication::translate("MainWindow", "---", nullptr));
        pushButton->setText(QString());
        pb_adult_plus->setText(QString());
        pb_child_plus->setText(QString());
        pb_adult_minus->setText(QString());
        pb_child_minus->setText(QString());
        ChildCount->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        AdultCount->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TotalValue->setText(QCoreApplication::translate("MainWindow", "\342\202\254 0.00", nullptr));
        Counter_2->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TheDate->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TheTime->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        pb_ValidCard->setText(QString());
        pb_InvalidCard->setText(QString());
        label_header_01->setText(QCoreApplication::translate("MainWindow", "PV3000", nullptr));
        label_header_02->setText(QCoreApplication::translate("MainWindow", "UITP Barcelona 2023", nullptr));
        label_header_03->setText(QCoreApplication::translate("MainWindow", "BUY TICKETS", nullptr));
        label_header_04->setText(QCoreApplication::translate("MainWindow", "ADULT", nullptr));
        label_header_05->setText(QCoreApplication::translate("MainWindow", "$2.20", nullptr));
        label_header_06->setText(QCoreApplication::translate("MainWindow", "CHILD", nullptr));
        label_header_07->setText(QCoreApplication::translate("MainWindow", "$1.50", nullptr));
        label_header_08->setText(QCoreApplication::translate("MainWindow", "TOTAL TO PAY", nullptr));
        label_header_00->setText(QCoreApplication::translate("MainWindow", "header_00", nullptr));
        label_header_09->setText(QCoreApplication::translate("MainWindow", "header_09", nullptr));
        pb_reboot->setText(QCoreApplication::translate("MainWindow", "Reboot Device", nullptr));
        pb_moti->setText(QCoreApplication::translate("MainWindow", "Restart Moti", nullptr));
        pb_barcode->setText(QCoreApplication::translate("MainWindow", "Restart Barcode", nullptr));
        pb_header->setText(QCoreApplication::translate("MainWindow", "SERVICE SCREEN", nullptr));
        Counter_3->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "Heat", nullptr));
        pb_heat_off->setText(QCoreApplication::translate("MainWindow", "Off", nullptr));
        pb_heat_on->setText(QCoreApplication::translate("MainWindow", "On", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "Fan", nullptr));
        pb_fan_off->setText(QCoreApplication::translate("MainWindow", "Off", nullptr));
        pb_fan_50->setText(QCoreApplication::translate("MainWindow", "50%", nullptr));
        pb_fan_100->setText(QCoreApplication::translate("MainWindow", "100%", nullptr));
        pb_fan_auto->setText(QCoreApplication::translate("MainWindow", "Auto", nullptr));
        pb_fan_10->setText(QCoreApplication::translate("MainWindow", "20%", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "STATUS", nullptr));
        label_input_2->setText(QCoreApplication::translate("MainWindow", "Input 2  : 0", nullptr));
        label_input_1->setText(QCoreApplication::translate("MainWindow", "Input 1  : 0", nullptr));
        label_fan_pwm->setText(QCoreApplication::translate("MainWindow", "Fan PWM  : 000", nullptr));
        label_fan_temperature->setText(QCoreApplication::translate("MainWindow", "Fan Temp : 00.00", nullptr));
        label_cpu_temp->setText(QCoreApplication::translate("MainWindow", "CPU temp : 00.00", nullptr));
        label_i2c_temp->setText(QCoreApplication::translate("MainWindow", "i2c temp : 00.00", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MainWindow", "Lookup", nullptr));
        label_lookup_14->setText(QCoreApplication::translate("MainWindow", "43-44 : 000", nullptr));
        label_lookup_11->setText(QCoreApplication::translate("MainWindow", "37-38 : 000", nullptr));
        label_lookup_05->setText(QCoreApplication::translate("MainWindow", "25-26 : 000", nullptr));
        label_lookup_04->setText(QCoreApplication::translate("MainWindow", "23-24 : 000", nullptr));
        label_lookup_08->setText(QCoreApplication::translate("MainWindow", "31-32 : 000", nullptr));
        label_lookup_10->setText(QCoreApplication::translate("MainWindow", "35-36 : 000", nullptr));
        label_lookup_07->setText(QCoreApplication::translate("MainWindow", "29-30 : 000", nullptr));
        label_lookup_09->setText(QCoreApplication::translate("MainWindow", "33-34 : 000", nullptr));
        label_lookup_13->setText(QCoreApplication::translate("MainWindow", "41-42 : 000", nullptr));
        label_lookup_02->setText(QCoreApplication::translate("MainWindow", " 19-20 : 000", nullptr));
        label_lookup_03->setText(QCoreApplication::translate("MainWindow", "21-22 : 000", nullptr));
        label_lookup_12->setText(QCoreApplication::translate("MainWindow", "39-40 : 000", nullptr));
        label_lookup_01->setText(QCoreApplication::translate("MainWindow", " 0-18 : 000", nullptr));
        label_lookup_06->setText(QCoreApplication::translate("MainWindow", "27-28 : 000", nullptr));
        label_lookup_15->setText(QCoreApplication::translate("MainWindow", "45-46 : 000", nullptr));
        label_lookup_16->setText(QCoreApplication::translate("MainWindow", "47-48 : 000", nullptr));
        label_lookup_17->setText(QCoreApplication::translate("MainWindow", "49-50 : 000", nullptr));
        label_lookup_18->setText(QCoreApplication::translate("MainWindow", "49-50 : 000", nullptr));
        label_lookup_19->setText(QCoreApplication::translate("MainWindow", "49-50 : 000", nullptr));
        label_lookup_20->setText(QCoreApplication::translate("MainWindow", "49-50 : 000", nullptr));
        label_lookup_21->setText(QCoreApplication::translate("MainWindow", "49-50 : 000", nullptr));
        label_lookup_22->setText(QCoreApplication::translate("MainWindow", "49-50 : 000", nullptr));
        label_lookup_23->setText(QCoreApplication::translate("MainWindow", "49-50 : 000", nullptr));
        label_lookup_24->setText(QCoreApplication::translate("MainWindow", "49-50 : 000", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
