#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>

QString aTmp;
QString aTmp_barco;

#define MAIN_SCREEN       0
#define SPLASH_SCREEN     1

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 20;
QString Label2;



//=============================================================================================================================
int ReadDataOneLine( char *ifile , char *data , int MaxLen)
{
    FILE *fp;

    fp = fopen(ifile,"r");

    if(fp == NULL)
    {
            perror("Error opening file");
            return(-1);
    }

    if( fgets (data, MaxLen, fp)!=NULL )
    {
        fclose(fp);
        return(strlen(data));
    }

    fclose(fp);
    return(0);
}
//=============================================================================================================================





MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->loop->setAttribute   (Qt::WA_TranslucentBackground,true);
    ui->mytime->setAttribute (Qt::WA_TranslucentBackground,true);
    ui->info->setAttribute      (Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->ip->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->temperature->setAttribute(Qt::WA_TranslucentBackground,true);

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;
    DelayTimer = 20;

    system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    system("echo \"-----\" > /tmp/text.txt");
    system("echo \"\" > /tmp/moti.data");
    system("echo \"\" > /tmp/rakinda.data");
    system("echo \"\" > /tmp/barcode.data");

    //system("echo 37 > /sys/class/gpio/export");
    //system("echo out > /sys/class/gpio/gpio37/direction");
    //system("echo 1 > /sys/class/gpio/gpio37/value");




   //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}


int loop=0;
int read_ip=0;
int refresh=0;


int step=0;
int old_step=0;
int Card_Present=0;


int mytime_err=0;
int moti_err=0;
int temp_err=0;
int net_err=0;

int ReadEMVError = 0;
int ReadNetError = 0;


int BarLedLoop=0;

void Update_BarLeds()
{

    switch(BarLedLoop%4)
    {
        case 0:
                system("echo 255 > /sys/class/leds/red1/brightness");
                system("echo 255 > /sys/class/leds/red2/brightness");
                break;
        case 1:
                system("echo 0   > /sys/class/leds/red1/brightness");
                system("echo 0   > /sys/class/leds/red2/brightness");
                system("echo 255 > /sys/class/leds/green1/brightness");
                system("echo 255 > /sys/class/leds/green2/brightness");
                break;
        case 2:
                system("echo 0   > /sys/class/leds/green1/brightness");
                system("echo 0   > /sys/class/leds/green2/brightness");
                system("echo 255 > /sys/class/leds/blue1/brightness");
                system("echo 255 > /sys/class/leds/blue2/brightness");
                break;
        default:
                system("echo 0  > /sys/class/leds/blue1/brightness");
                system("echo 0  > /sys/class/leds/blue2/brightness");
                break;
    }
    BarLedLoop++;
}


QString AppVersion;

void MainWindow::MainTask_loop()
{
    QString res = "reset";
    int sec;
    char aBuff[200];

    loop++;
    sec = loop/10;


    if( ReadDataOneLine((char *)"/tmp/moti.data",aBuff,200) < 6 )
    {
        ReadEMVError++;
        if(ReadEMVError < 30)
        {
            /*
            sprintf(aBuff,"Waiting card %d/100",ReadEMVError );
            AppVersion.sprintf(aBuff );
            ui->ip->setText(AppVersion);
            */
            return;
        }
    }

    if( ReadDataOneLine((char *)"/tmp/network.txt",aBuff,200) > 0 )
    {
        if(strstr(aBuff,"BAD") )
        {
            ReadNetError++;

            if(ReadNetError < 50)
            {
                /*
                sprintf(aBuff,"Waiting Network %d/100",ReadNetError );
                AppVersion.sprintf(aBuff );
                ui->ip->setText(AppVersion);
                */
                return;
            }
        }
    }



    step=sec%2;
    if(step==old_step) return;
    old_step=step;




    if(step%2==0)
    {
        system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");

        AppVersion.sprintf("%d",mytime_err );
        ui->mytime->setText(AppVersion);

        AppVersion.sprintf("%d",moti_err );
        ui->info->setText(AppVersion);

        AppVersion.sprintf("%d",temp_err );
        ui->temperature->setText(AppVersion);

        AppVersion.sprintf("%d",net_err );
        ui->ip_address->setText(AppVersion);

        return;
    }



    if( ReadDataOneLine((char *)"/tmp/ip.txt",aBuff,200) >0 )
    {
        AppVersion.sprintf("%s",aBuff );
        ui->ip->setText(AppVersion);
    }


    //=============================================================================
    AppVersion.sprintf("Loop: %d",(sec/2)+1 );
    ui->loop->setText(AppVersion);
    //=============================================================================

    Update_BarLeds();


    //=============================================================================
     if( ReadDataOneLine((char *)"/tmp/time.txt",aBuff,200) >0 )
     {
         if(strstr(aBuff,"Error") )
         {
             mytime_err++;
         }
         AppVersion.sprintf("%s",aBuff );
         ui->mytime->setText(AppVersion);
     }
     //=============================================================================

     //=============================================================================
     if( ReadDataOneLine((char *)"/tmp/temperature.txt",aBuff,200) >0 )
     {
         if(strstr(aBuff,"Error") )
         {
             temp_err++;
         }
         AppVersion.sprintf("%s",aBuff );
         ui->temperature->setText(AppVersion);
     }
     //=============================================================================
     if( ReadDataOneLine((char *)"/tmp/network.txt",aBuff,200) >0 )
     {
         if(strstr(aBuff,"BAD") )
         {
             net_err++;
         }
         else
         {
             ReadNetError=0;
         }
         AppVersion.sprintf("%s",aBuff );
         ui->ip_address->setText(AppVersion);
     }
     //=============================================================================
     if( ReadDataOneLine((char *)"/tmp/moti.data",aBuff,200) > 6 )
     {
         AppVersion.sprintf("%s",aBuff );
         ui->info->setText(AppVersion);
         system("echo \"\" > /tmp/moti.data &");
         system("beep &");
         ReadEMVError=0;
     }
     else
     {
         moti_err++;
         AppVersion.sprintf("-" );
         ui->info->setText(AppVersion);
    }
    //=============================================================================

    QCoreApplication::processEvents();
    return;
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}

