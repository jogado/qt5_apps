#!/bin/sh


DD=$(date +'%H%M%S')


TARGET_DIR="backup_$DD"
mkdir $TARGET_DIR 

cp /usr/bin/moti        $TARGET_DIR
cp /usr/bin/moti.sh     $TARGET_DIR
cp /usr/bin/demo_qt5.sh $TARGET_DIR
cp /usr/bin/TheDemo     $TARGET_DIR
cp /usr/bin/mv3000-emc  $TARGET_DIR

chmod +x restore.sh
cp ./restore.sh         $TARGET_DIR

chmod +x ./update.sh
cp ./update.sh         $TARGET_DIR


cp ./moti        /usr/bin/
cp ./moti.sh     /usr/bin/
cp ./demo_qt5.sh /usr/bin/
cp ./mv3000-emc  /usr/bin/

sync


echo "Done"
