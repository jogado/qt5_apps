#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>

QString aTmp;
QString aTmp_barco;

/*
QString aTitle;
QString MyTime;
*/





#define MAIN_SCREEN       0
#define SPLASH_SCREEN     1

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 300;
QString Label2;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);

    ui->Counter->setAttribute   (Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute (Qt::WA_TranslucentBackground,true);
    ui->Head->setAttribute      (Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);

    ui->Hello->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->Hello_2->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->Hello_3->setAttribute(Qt::WA_TranslucentBackground,true);

    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;
    DelayTimer = 20;

    system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    system("echo \"-----\" > /tmp/text.txt");
    //system("echo \"\" > moti.tmp");
    //system("echo \"\" > rakinda.tmp");

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

}

MainWindow::~MainWindow()
{
    delete ui;
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}


int loop=0;
int read_ip=0;
int refresh=0;

int BARLED_IS_ON = 0;
int BARLED_COLOR = 0;

char aBuf[256];
const char *aBuf2;

void MainWindow::MainTask_loop()
{
    QString res = "reset";
    int sec,min,hour,days;
    int len;
    unsigned char c;

    loop++;



    //=============================================================================

    /*
    if( DelayTimer) DelayTimer--;

    if( (DelayTimer == 0) && (CurrentScreen != MAIN_SCREEN))
    {
       ui->stackedWidget->setCurrentIndex(0);
       CurrentScreen = MAIN_SCREEN ;
    }
    */
    //=============================================================================


    //=============================================================================
    sec = loop/10;
    min = sec/60;
    hour = min/60;
    days = hour/24;
    Label2.sprintf("(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
    ui->Counter->setText(Label2);
    //=============================================================================


    //=============================================================================
    if( refresh > 0)
    {
        refresh--;
        if(refresh==0)
        {
            ui->free_text->setText("");
        }
    }
    //=============================================================================



    //=============================================================================
    if( read_ip == 0)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
     }

    if( loop%10==3)
    {
            system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    }

    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
    }
    //=============================================================================


    //=============================================================================
    read_barcode("/tmp/rakinda.data");

    if(aTmp_barco.length()>5)
    {
        refresh=30;

        len=aTmp_barco.length();
        aBuf2=aTmp_barco.toLatin1().data();
        c = aBuf2[len-2];

        //printf("len = %d %s [%c-%02x]\n\r",len,aBuf2,c,c);

        if( c%2 == 0 )
        {
            system("echo 255 > /sys/class/leds/red1/brightness");
            system("echo 255 > /sys/class/leds/red2/brightness");
            system("echo 0   > /sys/class/leds/green1/brightness");
            system("echo 0   > /sys/class/leds/green2/brightness");
        }
        if ( c%2==1 )
        {
            system("echo 255 > /sys/class/leds/green1/brightness");
            system("echo 255 > /sys/class/leds/green2/brightness");
            system("echo 0   > /sys/class/leds/red1/brightness");
            system("echo 0   > /sys/class/leds/red2/brightness");
        }
        BARLED_IS_ON = 7; // (7 x 100 ms)


        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
        system("beep");
        system("echo \"\" > /tmp/rakinda.data");
        QCoreApplication::processEvents();
    }
    //=============================================================================




    //=============================================================================
    read("/tmp/moti.data");
    if(aTmp.length()>5)
    {
        refresh=30;

        len=aTmp.length();
        aBuf2=aTmp.toLatin1().data();
        c = aBuf2[len-4];

       // printf("len = %d %s [%c-%02x]\n\r",len,aBuf2,c,c);

        if( c%2 == 0 )
        {
            system("echo 255 > /sys/class/leds/red1/brightness");
            system("echo 255 > /sys/class/leds/red2/brightness");
            system("echo 0   > /sys/class/leds/green1/brightness");
            system("echo 0   > /sys/class/leds/green2/brightness");
        }
        if ( c%2==1 )
        {
            system("echo 255 > /sys/class/leds/green1/brightness");
            system("echo 255 > /sys/class/leds/green2/brightness");
            system("echo 0   > /sys/class/leds/red1/brightness");
            system("echo 0   > /sys/class/leds/red2/brightness");
        }
        BARLED_IS_ON = 7; // (7 x 100 ms)


        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
        system("beep &");
        system("echo \"\" > /tmp/moti.data");
        QCoreApplication::processEvents();

    }

    {
        if( BARLED_IS_ON > 0 )
        {
            BARLED_IS_ON--;

            if( BARLED_IS_ON == 0 )
            {
                system("echo 0 > /sys/class/leds/green1/brightness");
                system("echo 0 > /sys/class/leds/green2/brightness");
                system("echo 0 > /sys/class/leds/red1/brightness");
                system("echo 0 > /sys/class/leds/red2/brightness");
            }
        }


    }
    //=============================================================================
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}

