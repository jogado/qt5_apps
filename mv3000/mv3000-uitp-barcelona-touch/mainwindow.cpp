#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>

QString aTmp;
QString aTmp_barco;

int ret=0;

int NbAdult=0;
int NbChild=0;
float Total=0;


#define MAIN_SCREEN       0
#define SPLASH_SCREEN     1

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label1;
QString Label2;
QString Label3;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);

    ui->Counter->setAttribute   (Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute (Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);

    /*
    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;
    DelayTimer = 50;
    */

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;
    //DelayTimer = 10;



    ret=system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    ret=system("echo \"-----\" > /tmp/text.txt");
    //system("echo \"\" > moti.tmp");
    //system("echo \"\" > rakinda.tmp");


    ui->pb_ValidCard->setVisible(false);
    ui->pb_InvalidCard->setVisible(false);


    ret=system("echo 0 > /sys/class/leds/green1/brightness");
    ret=system("echo 0 > /sys/class/leds/red1/brightness");
    ret=system("echo 0 > /sys/class/leds/green2/brightness");
    ret=system("echo 0 > /sys/class/leds/red2/brightness");

     ui->free_text->setVisible(false);

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

}

MainWindow::~MainWindow()
{
    delete ui;
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

//=============================================================================================
void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }
    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}
//=============================================================================================
void MainWindow::Update_Values()
{
    char aTmp[100];

    Total=((float)NbAdult * (float)2.2) + ((float)NbChild * (float)1.5);


    sprintf(aTmp,"%d",NbAdult);
    ui->AdultCount->setText(aTmp);

    sprintf(aTmp,"%d",NbChild);
    ui->ChildCount->setText(aTmp);


    sprintf(aTmp,"€ %4.2f",Total);
    ui->TotalValue->setText(aTmp);

    QCoreApplication::processEvents();
}
//=========================================================================================





int loop=0;
int read_ip=0;
int refresh=0;

int BARLED_IS_ON = 0;
int BARLED_COLOR = 0;

char aBuf[256];
const char *aBuf2;
int BeepIsOn=1;
int init_done=0;
int Timeout=50;







//=========================================================================================
void MainWindow::Led_RED()
{
    ui->pb_InvalidCard->setVisible(true);
    QCoreApplication::processEvents();

    ret=system("edgeled -c red -w 10 -v760");

    ret=system("echo 0 > /sys/class/leds/green1/brightness");
    ret=system("echo 0 > /sys/class/leds/green2/brightness");
    ret=system("echo 255 > /sys/class/leds/red1/brightness");
    ret=system("echo 255 > /sys/class/leds/red2/brightness");
    ret=system("echo \"\" > /tmp/moti.data");

    if( BeepIsOn )  ret=system("beep -f 100 -l 300 &");
}
//=========================================================================================
void MainWindow::Led_GREEN()
{
    ui->pb_ValidCard->setVisible(true);
    QCoreApplication::processEvents();

    ret=system("edgeled -c green -w 10 -v760");

    ret=system("echo 255 > /sys/class/leds/green1/brightness");
    ret=system("echo 255 > /sys/class/leds/green2/brightness");
    ret=system("echo 0 > /sys/class/leds/red1/brightness");
    ret=system("echo 0 > /sys/class/leds/red2/brightness");
    ret=system("echo \"\" > /tmp/moti.data");

    if( BeepIsOn )  ret=system("beep &");
}
//=========================================================================================
void MainWindow::Led_OFF()
{
    ui->pb_ValidCard->setVisible(false);
    ui->pb_InvalidCard->setVisible(false);

    QCoreApplication::processEvents();

    ui->stackedWidget->setCurrentIndex(1);
    ui->stackedWidget->setCurrentIndex(0);

    ret=system("echo 0 > /sys/class/leds/green1/brightness");
    ret=system("echo 0 > /sys/class/leds/green2/brightness");
    ret=system("echo 0 > /sys/class/leds/red1/brightness");
    ret=system("echo 0 > /sys/class/leds/red2/brightness");
    ret=system("echo \"\" > /tmp/moti.data");

}
//=========================================================================================


















void MainWindow::MainTask_loop()
{
    QString res = "reset";
    int sec,min,hour,days;
    int len;
    unsigned char c;
    char aBuf[100];


    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime   = current.toString("hh:mm:ss");
    QString TheDate   = current.toString("dd/MM/yyyy");

    ui->TheTime->setText(TheTime);
    ui->TheDate->setText(TheDate);

    loop++;

    //=============================================================================
    if( DelayTimer)
    {
        DelayTimer--;
        sprintf(aBuf,"%d",DelayTimer);
        ui->Counter_2->setText(aBuf);

        sprintf(aBuf,"%d",DelayTimer/10);
        ui->free_text->setText(aBuf);
    }
    //=============================================================================
    if( DelayTimer == 0 && Total != 0.0)
    {
        NbAdult=0;
        NbChild=0;
        Total= 0.0;

        Update_Values();

         /*
        ui->pb_InvalidCard->setVisible(true);
        DelayTimer = 5;
        BARLED_IS_ON = 7; // (7 x 100 ms)

        ret=system("echo 64   > /sys/class/leds/red1/brightness");
        ret=system("echo 64   > /sys/class/leds/red2/brightness");
        ret=system("echo 0   > /sys/class/leds/green1/brightness");
        ret=system("echo 0   > /sys/class/leds/green2/brightness");
        ret=system("echo \"\" > /tmp/moti.data");

        if( BeepIsOn )  system("beep &");
        */
    }
    //=============================================================================


    //=============================================================================
    sec = loop/10;
    min = sec/60;
    hour = min/60;
    days = hour/24;

    sprintf(aBuf,"(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
    ui->Counter->setText(aBuf);

    //=============================================================================


    //=============================================================================
    if( refresh > 0)
    {
        refresh--;
        if(refresh==0)
        {
            ui->free_text->setText("");
        }
    }
    //=============================================================================



    //=============================================================================
    if( read_ip == 0)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
    }
    //=============================================================================
    if( loop%10==3)
    {
            ret=system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    }
    //=============================================================================
    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
    }
    //=============================================================================







    //=============================================================================
    read_barcode("/tmp/rakinda.data");
    if(aTmp_barco.length()>5)
    {
        refresh=30;

        len=aTmp_barco.length();
        aBuf2=aTmp_barco.toLatin1().data();
        c = aBuf2[len-2];

        ret=system("echo \"\" > /tmp/rakinda.data");
        DelayTimer = 5;

        if(NbAdult || NbChild )
        {
            NbAdult=0;
            NbChild=0;
            Total= 0.0;

            Update_Values();

            if ( c%2==0 ) Led_RED();
            if ( c%2==1 ) Led_GREEN();
        }
        BARLED_IS_ON = 7; // (7 x 100 ms)

        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
        ret=system("echo \"\" > /tmp/rakinda.data");
        QCoreApplication::processEvents();
    }
    //=============================================================================

    //=============================================================================
    read("/tmp/moti.data");

    if(aTmp.length()>10 )
    {
        refresh=30;

        len=aTmp.length();
        aBuf2=aTmp.toLatin1().data();
        c = aBuf2[len-4];

        ret=system("echo \"\" > /tmp/moti.data");
        DelayTimer = 5;

        if(NbAdult || NbChild )
        {
            NbAdult=0;
            NbChild=0;
            Total= 0.0;

            Update_Values();

            if ( c%2==0 ) Led_RED();
            if ( c%2==1 ) Led_GREEN();
        }

        BARLED_IS_ON = 7; // (7 x 100 ms)

        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
        ret=system("echo \"\" > /tmp/moti.data");
        QCoreApplication::processEvents();
    }
    //=============================================================================
    if( BARLED_IS_ON > 0 )
    {
         BARLED_IS_ON--;
         if( BARLED_IS_ON == 0 )
         {
              Led_OFF();
         }
    }
    //=============================================================================
    ret=system("echo \"\" > /tmp/moti.data");
    ret=system("echo \"\" > /tmp/rakinda.data");
    //=============================================================================
    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::on_pb_adult_minus_clicked()
{
    DelayTimer = 50;

    if(NbAdult<=0) return ;
    NbAdult--;

    Update_Values();

    ret=system("/usr/bin/start_moti.sh &");
    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_adult_plus_clicked()
{
    DelayTimer = 50;

    if(NbAdult>=9) return;

    NbAdult++;

    Update_Values();

    ret=system("/usr/bin/start_moti.sh &");
    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_child_minus_clicked()
{
    DelayTimer = 50;

    if(NbChild<=0) return;

    NbChild--;

    Update_Values();

    ret=system("/usr/bin/start_moti.sh &");
    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_child_plus_clicked()
{
    DelayTimer = 50;

    if(NbChild>=9) return;

    NbChild++;

    Update_Values();

    ret=system("/usr/bin/start_moti.sh &");
    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
