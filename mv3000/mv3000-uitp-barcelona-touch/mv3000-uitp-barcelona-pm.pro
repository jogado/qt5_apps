#-------------------------------------------------
#
# Project created by QtCreator 2018-07-19T11:34:41
#
#-------------------------------------------------

QT  += core gui

target.path = /usr/bin
INSTALLS += target

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mv3000-uitp-barcelona-pm
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    myresource.qrc
