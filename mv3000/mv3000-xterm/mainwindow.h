#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QPushButton allBt[401];
    void populate();

protected:
    void changeEvent(QEvent *e);

public slots:
   void clickedSlot();

private slots:
    void update();

    void on_pb_display_yes_pressed();
    void on_pb_display_no_pressed();
    void on_pb_response_yes_pressed();
    void on_pb_response_nos_pressed();



private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
