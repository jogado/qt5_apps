#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWebEngineWidgets/QWebEngineView>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QWebEngineView *view = new QWebEngineView(this);
    view->setUrl(QUrl("https://www.google.com")); // Replace with your desired URL
    setCentralWidget(view);
}

MainWindow::~MainWindow()
{
    delete ui;
}

