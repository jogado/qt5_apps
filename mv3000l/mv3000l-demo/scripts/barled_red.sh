#!/bin/sh

#===================================================
# BARLED RED
#===================================================
edgeled -w 11 -c red -stay_on

echo 255    > /sys/class/leds/red1/brightness
echo 255    > /sys/class/leds/red2/brightness
echo 0      > /sys/class/leds/green1/brightness
echo 0      > /sys/class/leds/green2/brightness

