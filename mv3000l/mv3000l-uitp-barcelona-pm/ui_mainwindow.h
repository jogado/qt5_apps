/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *SplashScreen;
    QPushButton *Splash_bkg;
    QWidget *MainScreen;
    QLabel *Counter;
    QLabel *ip_address;
    QLabel *free_text;
    QPushButton *pushButton;
    QPushButton *pb_adult_plus;
    QPushButton *pb_child_plus;
    QPushButton *pb_adult_minus;
    QPushButton *pb_child_minus;
    QLabel *ChildCount;
    QLabel *AdultCount;
    QLabel *TotalValue;
    QLabel *TheDate;
    QLabel *TheTime;
    QPushButton *pb_ValidCard;
    QPushButton *pb_InvalidCard;
    QPushButton *pb_waiting;
    QWidget *Service;
    QPushButton *pb_reboot;
    QPushButton *pb_barcode;
    QPushButton *pb_header;
    QPushButton *pb_moti;
    QLabel *Counter_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(480, 800);
        MainWindow->setStyleSheet(QString::fromUtf8("color:black;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 480, 800));
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QString::fromUtf8(""));
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QString::fromUtf8("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QString::fromUtf8(""));
        Splash_bkg = new QPushButton(SplashScreen);
        Splash_bkg->setObjectName(QString::fromUtf8("Splash_bkg"));
        Splash_bkg->setEnabled(false);
        Splash_bkg->setGeometry(QRect(0, 0, 480, 800));
        Splash_bkg->setStyleSheet(QString::fromUtf8(""));
        stackedWidget->addWidget(SplashScreen);
        MainScreen = new QWidget();
        MainScreen->setObjectName(QString::fromUtf8("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QString::fromUtf8(""));
        Counter = new QLabel(MainScreen);
        Counter->setObjectName(QString::fromUtf8("Counter"));
        Counter->setEnabled(true);
        Counter->setGeometry(QRect(325, 17, 141, 20));
        QFont font;
        font.setFamily(QString::fromUtf8("Liberation Sans"));
        font.setPointSize(12);
        font.setItalic(false);
        Counter->setFont(font);
        Counter->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        Counter->setLineWidth(2);
        Counter->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        ip_address = new QLabel(MainScreen);
        ip_address->setObjectName(QString::fromUtf8("ip_address"));
        ip_address->setGeometry(QRect(290, 37, 171, 20));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Liberation Sans"));
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        ip_address->setFont(font1);
        ip_address->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        ip_address->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        free_text = new QLabel(MainScreen);
        free_text->setObjectName(QString::fromUtf8("free_text"));
        free_text->setGeometry(QRect(190, 520, 31, 21));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Liberation Sans"));
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        free_text->setFont(font2);
        free_text->setStyleSheet(QString::fromUtf8("color:blue;\n"
""));
        free_text->setLineWidth(2);
        free_text->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        free_text->setWordWrap(true);
        pushButton = new QPushButton(MainScreen);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setEnabled(false);
        pushButton->setGeometry(QRect(0, 0, 480, 800));
        pushButton->setStyleSheet(QString::fromUtf8("border: none;\n"
"background-image: url(:/bmp/MV3000L_Main_PM3.bmp);"));
        pb_adult_plus = new QPushButton(MainScreen);
        pb_adult_plus->setObjectName(QString::fromUtf8("pb_adult_plus"));
        pb_adult_plus->setGeometry(QRect(349, 202, 80, 80));
        pb_adult_plus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_plus.bmp);\n"
"border: none;"));
        pb_child_plus = new QPushButton(MainScreen);
        pb_child_plus->setObjectName(QString::fromUtf8("pb_child_plus"));
        pb_child_plus->setGeometry(QRect(349, 305, 80, 80));
        pb_child_plus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_plus.bmp);\n"
"border: none;"));
        pb_adult_minus = new QPushButton(MainScreen);
        pb_adult_minus->setObjectName(QString::fromUtf8("pb_adult_minus"));
        pb_adult_minus->setGeometry(QRect(50, 202, 80, 80));
        pb_adult_minus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_min.bmp);\n"
"border: none;"));
        pb_child_minus = new QPushButton(MainScreen);
        pb_child_minus->setObjectName(QString::fromUtf8("pb_child_minus"));
        pb_child_minus->setGeometry(QRect(50, 304, 80, 80));
        pb_child_minus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_min.bmp);\n"
"border: none;"));
        ChildCount = new QLabel(MainScreen);
        ChildCount->setObjectName(QString::fromUtf8("ChildCount"));
        ChildCount->setEnabled(true);
        ChildCount->setGeometry(QRect(263, 308, 80, 70));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Liberation Sans"));
        font3.setPointSize(35);
        font3.setBold(true);
        font3.setItalic(false);
        font3.setWeight(75);
        ChildCount->setFont(font3);
        ChildCount->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        ChildCount->setLineWidth(2);
        ChildCount->setAlignment(Qt::AlignCenter);
        AdultCount = new QLabel(MainScreen);
        AdultCount->setObjectName(QString::fromUtf8("AdultCount"));
        AdultCount->setEnabled(true);
        AdultCount->setGeometry(QRect(263, 212, 80, 60));
        AdultCount->setFont(font3);
        AdultCount->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        AdultCount->setLineWidth(2);
        AdultCount->setAlignment(Qt::AlignCenter);
        TotalValue = new QLabel(MainScreen);
        TotalValue->setObjectName(QString::fromUtf8("TotalValue"));
        TotalValue->setEnabled(true);
        TotalValue->setGeometry(QRect(117, 434, 220, 40));
        TotalValue->setFont(font3);
        TotalValue->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        TotalValue->setLineWidth(2);
        TotalValue->setAlignment(Qt::AlignCenter);
        TheDate = new QLabel(MainScreen);
        TheDate->setObjectName(QString::fromUtf8("TheDate"));
        TheDate->setEnabled(true);
        TheDate->setGeometry(QRect(20, 670, 200, 20));
        TheDate->setFont(font);
        TheDate->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
""));
        TheDate->setLineWidth(2);
        TheDate->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);
        TheTime = new QLabel(MainScreen);
        TheTime->setObjectName(QString::fromUtf8("TheTime"));
        TheTime->setEnabled(true);
        TheTime->setGeometry(QRect(240, 670, 100, 20));
        TheTime->setFont(font);
        TheTime->setStyleSheet(QString::fromUtf8("color:rgb(20, 60, 118)\n"
"\n"
""));
        TheTime->setLineWidth(2);
        TheTime->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        pb_ValidCard = new QPushButton(MainScreen);
        pb_ValidCard->setObjectName(QString::fromUtf8("pb_ValidCard"));
        pb_ValidCard->setGeometry(QRect(10, 714, 460, 76));
        pb_ValidCard->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_valid.bmp);\n"
"border: none;"));
        pb_InvalidCard = new QPushButton(MainScreen);
        pb_InvalidCard->setObjectName(QString::fromUtf8("pb_InvalidCard"));
        pb_InvalidCard->setGeometry(QRect(10, 714, 460, 76));
        pb_InvalidCard->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/pm_invalid.bmp);\n"
"border: none;"));
        pb_waiting = new QPushButton(MainScreen);
        pb_waiting->setObjectName(QString::fromUtf8("pb_waiting"));
        pb_waiting->setGeometry(QRect(10, 714, 460, 76));
        QFont font4;
        font4.setPointSize(18);
        font4.setBold(true);
        font4.setWeight(75);
        pb_waiting->setFont(font4);
        pb_waiting->setStyleSheet(QString::fromUtf8("border: none;\n"
"color : white;\n"
"background-image: url(:/bmp/Arrow_ON.bmp);"));
        stackedWidget->addWidget(MainScreen);
        pushButton->raise();
        Counter->raise();
        free_text->raise();
        ip_address->raise();
        ChildCount->raise();
        AdultCount->raise();
        TotalValue->raise();
        TheDate->raise();
        TheTime->raise();
        pb_ValidCard->raise();
        pb_InvalidCard->raise();
        pb_adult_plus->raise();
        pb_child_plus->raise();
        pb_adult_minus->raise();
        pb_child_minus->raise();
        pb_waiting->raise();
        Service = new QWidget();
        Service->setObjectName(QString::fromUtf8("Service"));
        pb_reboot = new QPushButton(Service);
        pb_reboot->setObjectName(QString::fromUtf8("pb_reboot"));
        pb_reboot->setGeometry(QRect(10, 430, 451, 76));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Liberation Sans"));
        font5.setPointSize(23);
        font5.setBold(true);
        font5.setWeight(75);
        pb_reboot->setFont(font5);
        pb_reboot->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_barcode = new QPushButton(Service);
        pb_barcode->setObjectName(QString::fromUtf8("pb_barcode"));
        pb_barcode->setGeometry(QRect(10, 330, 451, 76));
        pb_barcode->setFont(font5);
        pb_barcode->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_header = new QPushButton(Service);
        pb_header->setObjectName(QString::fromUtf8("pb_header"));
        pb_header->setGeometry(QRect(10, 20, 460, 76));
        pb_header->setFont(font5);
        pb_header->setStyleSheet(QString::fromUtf8("background-color: rgb(20, 60, 118);\n"
"border: none;\n"
"color:white;\n"
"\n"
""));
        pb_moti = new QPushButton(Service);
        pb_moti->setObjectName(QString::fromUtf8("pb_moti"));
        pb_moti->setGeometry(QRect(10, 230, 451, 76));
        pb_moti->setFont(font5);
        pb_moti->setStyleSheet(QString::fromUtf8("background-color: white;\n"
"border-color: rgb(32, 74, 135);\n"
"color: rgb(20, 60, 118);\n"
"border: 3px solid rgb(32, 74, 135);"));
        pb_moti->setFlat(false);
        Counter_2 = new QLabel(Service);
        Counter_2->setObjectName(QString::fromUtf8("Counter_2"));
        Counter_2->setEnabled(true);
        Counter_2->setGeometry(QRect(435, 71, 30, 20));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Liberation Sans"));
        font6.setPointSize(10);
        font6.setItalic(false);
        Counter_2->setFont(font6);
        Counter_2->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        Counter_2->setLineWidth(2);
        Counter_2->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        stackedWidget->addWidget(Service);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        Splash_bkg->setText(QString());
        Counter->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        ip_address->setText(QCoreApplication::translate("MainWindow", "000.000.000.000", nullptr));
        free_text->setText(QCoreApplication::translate("MainWindow", "---", nullptr));
        pushButton->setText(QString());
        pb_adult_plus->setText(QString());
        pb_child_plus->setText(QString());
        pb_adult_minus->setText(QString());
        pb_child_minus->setText(QString());
        ChildCount->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        AdultCount->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TotalValue->setText(QCoreApplication::translate("MainWindow", "\342\202\254 0.00", nullptr));
        TheDate->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TheTime->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        pb_ValidCard->setText(QString());
        pb_InvalidCard->setText(QString());
        pb_waiting->setText(QString());
        pb_reboot->setText(QCoreApplication::translate("MainWindow", "Reboot Device", nullptr));
        pb_barcode->setText(QCoreApplication::translate("MainWindow", "Restart Barcode", nullptr));
        pb_header->setText(QCoreApplication::translate("MainWindow", "SERVICE SCREEN", nullptr));
        pb_moti->setText(QCoreApplication::translate("MainWindow", "Restart Moti", nullptr));
        Counter_2->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
