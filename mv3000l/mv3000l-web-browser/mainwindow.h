#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void MasterTimer_update();
    void MainTask_loop();

    void on_pb_adult_minus_clicked();

    void on_pb_adult_plus_clicked();

    void on_pb_child_minus_clicked();

    void on_pb_child_plus_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
