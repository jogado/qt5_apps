/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *MainScreen;
    QLabel *Counter;
    QLabel *ip_address;
    QLabel *free_text;
    QPushButton *pushButton;
    QPushButton *pb_adult_plus;
    QPushButton *pb_child_plus;
    QPushButton *pb_adult_minus;
    QPushButton *pb_child_minus;
    QLabel *ChildCount;
    QLabel *AdultCount;
    QLabel *TotalValue;
    QLabel *Counter_2;
    QLabel *TheDate;
    QLabel *TheTime;
    QWebView *webView;
    QWidget *SplashScreen;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(480, 800);
        MainWindow->setStyleSheet(QString::fromUtf8("color:black;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 480, 800));
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QString::fromUtf8(""));
        MainScreen = new QWidget();
        MainScreen->setObjectName(QString::fromUtf8("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QString::fromUtf8(""));
        Counter = new QLabel(MainScreen);
        Counter->setObjectName(QString::fromUtf8("Counter"));
        Counter->setEnabled(true);
        Counter->setGeometry(QRect(154, 768, 140, 20));
        QFont font;
        font.setFamily(QString::fromUtf8("Liberation Sans"));
        font.setPointSize(10);
        font.setItalic(false);
        Counter->setFont(font);
        Counter->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        Counter->setLineWidth(2);
        Counter->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        ip_address = new QLabel(MainScreen);
        ip_address->setObjectName(QString::fromUtf8("ip_address"));
        ip_address->setGeometry(QRect(12, 768, 110, 20));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Liberation Sans"));
        font1.setPointSize(10);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        ip_address->setFont(font1);
        ip_address->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        ip_address->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        free_text = new QLabel(MainScreen);
        free_text->setObjectName(QString::fromUtf8("free_text"));
        free_text->setGeometry(QRect(14, 554, 450, 60));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Liberation Sans"));
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setWeight(75);
        free_text->setFont(font2);
        free_text->setStyleSheet(QString::fromUtf8("color:white;\n"
""));
        free_text->setLineWidth(2);
        free_text->setAlignment(Qt::AlignCenter);
        free_text->setWordWrap(true);
        pushButton = new QPushButton(MainScreen);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setEnabled(false);
        pushButton->setGeometry(QRect(0, 0, 480, 800));
        pushButton->setStyleSheet(QString::fromUtf8("border: none;\n"
"background-image: url(:/bmp/MV3000L_Main.bmp);"));
        pb_adult_plus = new QPushButton(MainScreen);
        pb_adult_plus->setObjectName(QString::fromUtf8("pb_adult_plus"));
        pb_adult_plus->setGeometry(QRect(349, 203, 79, 78));
        pb_adult_plus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/Button+.bmp);\n"
"border: none;"));
        pb_child_plus = new QPushButton(MainScreen);
        pb_child_plus->setObjectName(QString::fromUtf8("pb_child_plus"));
        pb_child_plus->setGeometry(QRect(349, 305, 79, 78));
        pb_child_plus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/Button+.bmp);\n"
"border: none;"));
        pb_adult_minus = new QPushButton(MainScreen);
        pb_adult_minus->setObjectName(QString::fromUtf8("pb_adult_minus"));
        pb_adult_minus->setGeometry(QRect(50, 203, 79, 78));
        pb_adult_minus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/Button-.bmp);\n"
"border: none;"));
        pb_child_minus = new QPushButton(MainScreen);
        pb_child_minus->setObjectName(QString::fromUtf8("pb_child_minus"));
        pb_child_minus->setGeometry(QRect(49, 305, 80, 78));
        pb_child_minus->setStyleSheet(QString::fromUtf8("background-image: url(:/bmp/Button-.bmp);\n"
"border: none;"));
        ChildCount = new QLabel(MainScreen);
        ChildCount->setObjectName(QString::fromUtf8("ChildCount"));
        ChildCount->setEnabled(true);
        ChildCount->setGeometry(QRect(263, 308, 80, 70));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Liberation Sans"));
        font3.setPointSize(35);
        font3.setBold(true);
        font3.setItalic(false);
        font3.setWeight(75);
        ChildCount->setFont(font3);
        ChildCount->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        ChildCount->setLineWidth(2);
        ChildCount->setAlignment(Qt::AlignCenter);
        AdultCount = new QLabel(MainScreen);
        AdultCount->setObjectName(QString::fromUtf8("AdultCount"));
        AdultCount->setEnabled(true);
        AdultCount->setGeometry(QRect(263, 212, 80, 60));
        AdultCount->setFont(font3);
        AdultCount->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        AdultCount->setLineWidth(2);
        AdultCount->setAlignment(Qt::AlignCenter);
        TotalValue = new QLabel(MainScreen);
        TotalValue->setObjectName(QString::fromUtf8("TotalValue"));
        TotalValue->setEnabled(true);
        TotalValue->setGeometry(QRect(129, 434, 220, 40));
        TotalValue->setFont(font3);
        TotalValue->setStyleSheet(QString::fromUtf8("color:black;\n"
"\n"
""));
        TotalValue->setLineWidth(2);
        TotalValue->setAlignment(Qt::AlignCenter);
        Counter_2 = new QLabel(MainScreen);
        Counter_2->setObjectName(QString::fromUtf8("Counter_2"));
        Counter_2->setEnabled(true);
        Counter_2->setGeometry(QRect(435, 768, 31, 20));
        Counter_2->setFont(font);
        Counter_2->setStyleSheet(QString::fromUtf8("color:white;\n"
"\n"
""));
        Counter_2->setLineWidth(2);
        Counter_2->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        TheDate = new QLabel(MainScreen);
        TheDate->setObjectName(QString::fromUtf8("TheDate"));
        TheDate->setEnabled(true);
        TheDate->setGeometry(QRect(370, 14, 100, 20));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Liberation Sans"));
        font4.setPointSize(12);
        font4.setItalic(false);
        TheDate->setFont(font4);
        TheDate->setStyleSheet(QString::fromUtf8("color:white;\n"
""));
        TheDate->setLineWidth(2);
        TheDate->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        TheTime = new QLabel(MainScreen);
        TheTime->setObjectName(QString::fromUtf8("TheTime"));
        TheTime->setEnabled(true);
        TheTime->setGeometry(QRect(370, 36, 100, 20));
        TheTime->setFont(font4);
        TheTime->setStyleSheet(QString::fromUtf8("color:white\n"
"\n"
""));
        TheTime->setLineWidth(2);
        TheTime->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        webView = new QWebView(MainScreen);
        webView->setObjectName(QString::fromUtf8("webView"));
        webView->setGeometry(QRect(0, 0, 480, 800));
        webView->setUrl(QUrl(QString::fromUtf8("https://www.google.com/")));
        stackedWidget->addWidget(MainScreen);
        pushButton->raise();
        Counter->raise();
        free_text->raise();
        ip_address->raise();
        pb_adult_plus->raise();
        pb_child_plus->raise();
        pb_adult_minus->raise();
        pb_child_minus->raise();
        ChildCount->raise();
        AdultCount->raise();
        TotalValue->raise();
        Counter_2->raise();
        TheDate->raise();
        TheTime->raise();
        webView->raise();
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QString::fromUtf8("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QString::fromUtf8("image:url(:/bmp/IT-Trans.bmp)"));
        stackedWidget->addWidget(SplashScreen);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        Counter->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        ip_address->setText(QCoreApplication::translate("MainWindow", "000.000.000.000", nullptr));
        free_text->setText(QCoreApplication::translate("MainWindow", "---", nullptr));
        pushButton->setText(QString());
        pb_adult_plus->setText(QString());
        pb_child_plus->setText(QString());
        pb_adult_minus->setText(QString());
        pb_child_minus->setText(QString());
        ChildCount->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        AdultCount->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TotalValue->setText(QCoreApplication::translate("MainWindow", "\342\202\254 0.00", nullptr));
        Counter_2->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TheDate->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        TheTime->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
