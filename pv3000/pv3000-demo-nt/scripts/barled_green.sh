#!/bin/sh

#===================================================
# BARLED GREEN
#===================================================

echo 255    > /sys/class/leds/LED1/brightness
echo 0      > /sys/class/leds/LED0/brightness
echo 0      > /sys/class/leds/LED2/brightness

edgeled -w 11 -c green -stay_on

echo 255    > /sys/class/leds/green/brightness
echo 0      > /sys/class/leds/red/brightness


