#!/bin/sh

#===================================================
# BARLED RED
#===================================================
echo 0      > /sys/class/leds/LED0/brightness
echo 0      > /sys/class/leds/LED1/brightness
echo 0      > /sys/class/leds/LED2/brightness

echo 0      > /sys/class/leds/red/brightness
echo 0      > /sys/class/leds/green/brightness

