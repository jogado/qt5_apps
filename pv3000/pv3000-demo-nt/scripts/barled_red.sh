#!/bin/sh

#===================================================
# BARLED RED
#===================================================


echo 255    > /sys/class/leds/LED0/brightness
echo 0      > /sys/class/leds/LED1/brightness
echo 0      > /sys/class/leds/LED2/brightness
usleep 50000
edgeled -w 11 -c red -stay_on

echo 255    > /sys/class/leds/red/brightness
echo 0      > /sys/class/leds/green/brightness

