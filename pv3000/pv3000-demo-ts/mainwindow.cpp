#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>

QString aTmp;
QString aTmp_barco;

int ret=0;

int NbAdult=0;
int NbChild=0;
float Total=0;

float fAdult=2.2;
float fChild=1.5;


#define MAIN_SCREEN       0
#define SPLASH_SCREEN     1
#define SERVICE_SCREEN    2

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label1;
QString Label2;
QString Label3;


char aBufOld[256];
char *pBufOld;


int loop=0;
int read_ip=0;
int refresh=0;

int BARLED_IS_ON = 0;
int BARLED_COLOR = 0;

char aBuf[256];
const char *aBuf2;


int BeepIsOn=1;
int init_done=0;
int Timeout=50;


char Currency_char[10];





MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    pBufOld = &aBufOld[0];

    ui->setupUi(this);

    ui->Counter->setAttribute   (Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute (Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;



    ret=system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
    ret=system("echo \"-----\" > /tmp/text.txt");
    ret=system("echo \"\" > /tmp/moti.data");
    ret=system("echo \"\" > /tmp/rakinda.data");


    ret=system("/usr/bin/barled_off.sh");

    ui->TheDate->setVisible         (false);
    ui->TheTime->setVisible         (false);

    ui->pb_ValidCard->setVisible(false);
    ui->pb_InvalidCard->setVisible(false);
    ui->free_text->setVisible(false);
    ui->label_header_00->setVisible(false);
    ui->label_header_09->setVisible(false);

    Update_Values();


    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

    read_header(0);
    read_header(1);
    read_header(2);
    read_header(3);
    read_header(4);
    read_header(5);
    read_header(6);
    read_header(7);
    read_header(8);
    read_header(9);
}

MainWindow::~MainWindow()
{
    delete ui;
}




int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

//=============================================================================================
void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }
    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}
//=============================================================================================
void MainWindow::read_header(int header)
{
    QLabel *l = nullptr;
    QString Empty = "****";
    char aBuffer[20];

    if(header==0)
    {
        read("/home/root/demo/header_00.txt");
        sprintf(Currency_char,"%s",aTmp.toLatin1().data());
        return;
    }

    if(header== 0) { read("/home/root/demo/header_00.txt"); l=ui->label_header_00;}
    if(header== 1) { read("/home/root/demo/header_01.txt"); l=ui->label_header_01;}
    if(header== 2) { read("/home/root/demo/header_02.txt"); l=ui->label_header_02;}
    if(header== 3) { read("/home/root/demo/header_03.txt"); l=ui->label_header_03;}
    if(header== 4) { read("/home/root/demo/header_04.txt"); l=ui->label_header_04;}
    if(header== 5) { read("/home/root/demo/header_05.txt"); l=ui->label_header_05;}
    if(header== 6) { read("/home/root/demo/header_06.txt"); l=ui->label_header_06;}
    if(header== 7) { read("/home/root/demo/header_07.txt"); l=ui->label_header_07;}
    if(header== 8) { read("/home/root/demo/header_08.txt"); l=ui->label_header_08;}
    if(header== 9) { read("/home/root/demo/header_09.txt"); l=ui->label_header_09;}

    if( header == 5  )
    {
        fAdult = aTmp.toFloat();
    }

    if( header == 7  )
    {
        fChild = aTmp.toFloat();
    }

    if (l == nullptr)
    {
        return;
    }

    if( header == 5 || header == 7 )
    {
        sprintf(aBuffer,"  %s",aTmp.toLatin1().data());
        aBuffer[0]=Currency_char[0];
        l->setText(aBuffer);
        return;
    }


    if(aTmp.length()>0)
    {
        l->setText(aTmp);
    }
    else
    {
        l->setText(Empty);
    }

}
//=========================================================================================







void MainWindow::Update_Values()
{
    char aTmp[100];

    Total=((float)NbAdult * (float)2.2) + ((float)NbChild * (float)1.5);


    sprintf(aTmp,"%d",NbAdult);
    ui->AdultCount->setText(aTmp);

    sprintf(aTmp,"%d",NbChild);
    ui->ChildCount->setText(aTmp);


    sprintf(aTmp,"  %4.2f",Total);
    aTmp[0]=Currency_char[0];
    ui->TotalValue->setText(aTmp);

    QCoreApplication::processEvents();
}
//=========================================================================================





//=========================================================================================
void MainWindow::Led_RED()
{
    ui->pb_InvalidCard->setVisible ( true  );
    ui->pb_ValidCard->setVisible   ( false );

    QCoreApplication::processEvents();

    /*
    ret=system("edgeled -w 11 -c red -stay_on");

    ret=system("echo 0 > /sys/class/leds/green/brightness");
    ret=system("echo 255 > /sys/class/leds/red/brightness");

    ret=system("echo 255   > /sys/class/leds/LED0/brightness");
    ret=system("echo 0 > /sys/class/leds/LED1/brightness");
    ret=system("echo 0   > /sys/class/leds/LED2/brightness");
    */

    ret=system("/usr/bin/barled_red.sh");

    if( BeepIsOn )  ret=system("beep -f 100 -l 300 &");
}
//=========================================================================================
void MainWindow::Led_GREEN()
{
    ui->pb_InvalidCard->setVisible( false );
    ui->pb_ValidCard->setVisible  ( true  );

    QCoreApplication::processEvents();


    /*
    ret=system("edgeled -w 11 -c green -stay_on ");

    ret=system("echo 255 > /sys/class/leds/green/brightness");
    ret=system("echo 0 > /sys/class/leds/red/brightness");

    ret=system("echo 0   > /sys/class/leds/LED0/brightness");
    ret=system("echo 255 > /sys/class/leds/LED1/brightness");
    ret=system("echo 0   > /sys/class/leds/LED2/brightness");
    */

    ret=system("/usr/bin/barled_green.sh");

    if( BeepIsOn )  ret=system("beep &");
}
//=========================================================================================
void MainWindow::Led_OFF()
{
    ui->pb_ValidCard->setVisible    ( false );
    ui->pb_InvalidCard->setVisible  ( false );

    QCoreApplication::processEvents();

    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);



    ret=system("echo 0 > /sys/class/leds/green/brightness");
    ret=system("echo 0 > /sys/class/leds/red/brightness");

    ret=system("echo 0 > /sys/class/leds/LED0/brightness");
    ret=system("echo 0 > /sys/class/leds/LED1/brightness");
    ret=system("echo 0 > /sys/class/leds/LED2/brightness");

    QCoreApplication::processEvents();
}
//=========================================================================================




char aTmp2[256];
int Card_Present=0;
int Toggle=0;
int init_gpio = 0;

void MainWindow::MainTask_loop()
{
    QString res = "reset";
    int sec,min,hour,days;
    int len;
    unsigned char c;
    char aBuf[100];

    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime   = current.toString("hh:mm:ss");
    QString TheDate   = current.toString("dd/MM/yyyy");

    ui->TheTime->setText(TheTime);
    ui->TheDate->setText(TheDate);

    loop++;


    //=============================================================================
    if( loop%40==1 )
    {
        ret=system("/usr/bin/start_moti.sh &");
    }
    //=============================================================================



    //=============================================================================
    if( DelayTimer)
    {
        DelayTimer--;
        sprintf(aBuf,"%d",DelayTimer);
        ui->Counter_2->setText(aBuf);

        sprintf(aBuf,"%d",DelayTimer/10);
        ui->free_text->setText(aBuf);

        if( DelayTimer == 0 )
        {
            NbAdult=1;
            NbChild=0;
            Update_Values();
        }
    }
    //=============================================================================
    if( DelayTimer == 0 && CurrentScreen != MAIN_SCREEN)
    {
        CurrentScreen = MAIN_SCREEN;
        ui->stackedWidget->setCurrentIndex(CurrentScreen);
        NbAdult=1;
        NbChild=0;
        Update_Values();
    }
    //=========================================================================

    //=========================================================================
    if(loop%10==0)
    {
        sec = loop/10;
        min = sec/60;
        hour = min/60;
        days = hour/24;
        sprintf(aBuf,"(%d) %02d:%02d:%02d ",days,hour%24,min%60,sec%60);
        ui->Counter->setText(aBuf);
    }
    //=========================================================================


    //=============================================================================
    if( refresh > 0)
    {
        refresh--;
        if(refresh==0)
        {
            ui->free_text->setText("");
        }
    }
    //=============================================================================



    //=============================================================================
    if( loop%10==5)
    {
        ret=system("ifconfig eth0 |grep 'inet addr' |cut -d: -f2 | awk '{print $1}' > /tmp/ip.txt");
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
    }
    //=============================================================================


    if( loop%10==3 && CurrentScreen == SERVICE_SCREEN)
    {
        Update_lookup_table();
    }
    //=============================================================================
    if( loop%10==7 && CurrentScreen == SERVICE_SCREEN)
    {
        sprintf(aBuf,"%d",DelayTimer/10);
        ui->Counter_3->setText(aBuf);

        //=========================================================================================================
        ret=system("echo -n -e \"Fan PWM  : \"  > /tmp/fan_pwm1.data");
        ret=system("cat /sys/bus/i2c/devices/i2c-4/4-0050/hwmon/hwmon2/pwm1 >> /tmp/fan_pwm1.data");
        read_barcode("/tmp/fan_pwm1.data");
        aBuf2=aTmp_barco.toLatin1().data();
        ui->label_fan_pwm->setText(aBuf2);
        //=========================================================================================================


        //=========================================================================================================
        ret=system("cat /sys/bus/i2c/devices/i2c-4/4-0050/hwmon/hwmon2/temp2_input| cut -c 1-2 > /tmp/fan_temp");
        ret=system("cat /sys/bus/i2c/devices/i2c-4/4-0050/hwmon/hwmon2/temp2_input| cut -c 3-4 > /tmp/fan_temp2");
        ret=system("echo -n -e \"Fan Temp : \"  >  /tmp/fan_temp.data");
        ret=system("cat /tmp/fan_temp  | tr -d '\n\r'     >> /tmp/fan_temp.data");
        ret=system("echo -n -e \".\"        >> /tmp/fan_temp.data");
        ret=system("cat /tmp/fan_temp2 | tr -d '\n\r'     >> /tmp/fan_temp.data");

        read_barcode("/tmp/fan_temp.data");
        aBuf2=aTmp_barco.toLatin1().data();
        ui->label_fan_temperature->setText(aBuf2);
        //=========================================================================================================


        //=========================================================================================================

        ret=system("cat /sys/class/i2c-dev/i2c-0/device/0-0027/hwmon/hwmon0/temp1_input| cut -c 1-2 > /tmp/i2c_temp");
        ret=system("cat /sys/class/i2c-dev/i2c-0/device/0-0027/hwmon/hwmon0/temp1_input| cut -c 3-4 > /tmp/i2c_temp2");
        ret=system("echo -n -e \"i2c Temp : \"  >  /tmp/i2c_temp.data");
        ret=system("cat /tmp/i2c_temp  | tr -d '\n\r'     >> /tmp/i2c_temp.data");
        ret=system("echo -n -e \".\"        >> /tmp/i2c_temp.data");
        ret=system("cat /tmp/i2c_temp2 | tr -d '\n\r'     >> /tmp/i2c_temp.data");

        read_barcode("/tmp/i2c_temp.data");
        aBuf2=aTmp_barco.toLatin1().data();
        ui->label_i2c_temp->setText(aBuf2);

        //=========================================================================================================



        //=========================================================================================================
        ret=system("cat /sys/devices/virtual/thermal/thermal_zone0/temp| cut -c 1-2 > /tmp/cpu_temp");
        ret=system("cat /sys/devices/virtual/thermal/thermal_zone0/temp| cut -c 3-4 > /tmp/cpu_temp2");
        ret=system("echo -n -e \"cpu Temp : \"  >  /tmp/cpu_temp.data");
        ret=system("cat /tmp/cpu_temp  | tr -d '\n\r'     >> /tmp/cpu_temp.data");
        ret=system("echo -n -e \".\"        >> /tmp/cpu_temp.data");
        ret=system("cat /tmp/cpu_temp2 | tr -d '\n\r'     >> /tmp/cpu_temp.data");

        read_barcode("/tmp/cpu_temp.data");
        aBuf2=aTmp_barco.toLatin1().data();
        ui->label_cpu_temp->setText(aBuf2);
        //=========================================================================================================


        //=========================================================================================================
        if(!init_gpio)
        {
            ret=system("echo 504 > /sys/class/gpio/export");
            ret=system("echo 505 > /sys/class/gpio/export");
            ret=system("echo in > /sys/class/gpio/gpio504/direction");
            ret=system("echo in > /sys/class/gpio/gpio505/direction");
            init_gpio++;
        }



        ret=system("echo -n -e \"Input 1  : \"  >  /tmp/gpio_input504.data");
        ret=system("cat /sys/class/gpio/gpio504/value >> /tmp/gpio_input504.data");
        read_barcode("/tmp/gpio_input504.data");
        aBuf2=aTmp_barco.toLatin1().data();
        ui->label_input_1->setText(aBuf2);


        ret=system("echo -n -e \"Input 2  : \"  >  /tmp/gpio_input505.data");
        ret=system("cat /sys/class/gpio/gpio505/value >> /tmp/gpio_input505.data");
        read_barcode("/tmp/gpio_input505.data");
        aBuf2=aTmp_barco.toLatin1().data();
        ui->label_input_2->setText(aBuf2);
        //=========================================================================================================
    }
    //=============================================================================




 //=============================================================================
    read("/tmp/moti.data");
    if(aTmp.length()>10 )
    {
        Card_Present = 1;
    }
    else
    {
        Card_Present = 0;
        if(loop%10==5)
        {
            sprintf(pBufOld,"%s","");
        }
    }
    //=============================================================================


    //=============================================================================
    if( (DelayTimer%30) == 15 )
    {
        ret=system("echo \"\" > /tmp/moti.data");
        ret=system("echo \"Clearing moti.data ....\n\r\" ");
    }
    //=============================================================================

  if(Card_Present )
    {
        refresh=30;
        len=aTmp.length();
        aBuf2=aTmp.toLatin1().data();
        c = aBuf2[len-4];

        ret=system("echo \"\" > /tmp/moti.data");


        if( strstr ( aBuf2,"[04 35 C4 8A 68 67 80 ]")!= NULL  ||  strstr ( aBuf2,"[04 BB 34 8A 68 67 84 ]") != NULL )
        {

            if( CurrentScreen != SERVICE_SCREEN )
            {
                ret=system("amixer sset 'PCM' 95% ");
                ret=system("amixer sset 'Lineout' 95% ");
                ret=system("aplay /home/root/demo/pleaseshowid6.wav");

                Clear_Service_buttons();
                Update_lookup_table();
                CurrentScreen = SERVICE_SCREEN;
                ui->stackedWidget->setCurrentIndex(CurrentScreen);
                DelayTimer = 1800;
                return;
            }
            else
            {
                CurrentScreen = MAIN_SCREEN;
                ui->stackedWidget->setCurrentIndex(CurrentScreen);
                DelayTimer = 150;
                return;
            }
        }

        if( strcmp ( aBuf2,pBufOld) == 0 )
        {
            sprintf(aTmp2,"echo \"EQUAL: <%s><%s>\n\r\"",aBuf2,pBufOld );
            ret=system(aTmp2);
            return;
        }
        else
        {
            sprintf(aTmp2,"echo \"DIFFERENT: <%s><%s>\n\r\"",aBuf2,pBufOld );
            ret=system(aTmp2);
            sprintf(pBufOld,"%s",aBuf2);
            QCoreApplication::processEvents();
        }

        DelayTimer = 30;

        if(NbAdult || NbChild )
        {
            NbAdult=1;
            NbChild=0;

            Update_Values();

            if ( c%2==0 ) Led_RED();
            if ( c%2==1 ) Led_GREEN();
        }

        BARLED_IS_ON = 7; // (7 x 100 ms)
        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
    }
    QCoreApplication::processEvents();
    //=============================================================================







  //=============================================================================
    read_barcode("/tmp/rakinda.data");
    if(aTmp_barco.length()>5)
    {
        refresh=30;

        len=aTmp_barco.length(); QCoreApplication::processEvents();
        aBuf2=aTmp_barco.toLatin1().data();
        c = aBuf2[len-2];
        ret=system("echo \"\" > /tmp/rakinda.data");

        if( strstr ( aBuf2,"ESC_SERVICECARD1")  )
        {
            if( CurrentScreen != SERVICE_SCREEN )
            {
                ret=system("amixer sset 'PCM' 95% ");
                ret=system("amixer sset 'Lineout' 95% ");
                ret=system("aplay /home/root/demo/pleaseshowid6.wav");

                Clear_Service_buttons();
                Update_lookup_table();
                CurrentScreen = SERVICE_SCREEN;
                ui->stackedWidget->setCurrentIndex(CurrentScreen);
                DelayTimer = 1800;
                return;
            }
            else
            {
                CurrentScreen = MAIN_SCREEN;
                ui->stackedWidget->setCurrentIndex(CurrentScreen);
                DelayTimer = 150;
                return;
            }
        }







        DelayTimer = 30;

        if(NbAdult || NbChild )
        {
            NbAdult=1;
            NbChild=0;

            Update_Values();

            if ( c%2==0 ) Led_RED();
            if ( c%2==1 ) Led_GREEN();
        }
        BARLED_IS_ON = 7; // (7 x 100 ms)

        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
        //ret=system("echo \"\" > /tmp/rakinda.data");
        //QCoreApplication::processEvents();
    }
    //=============================================================================





    //=============================================================================
    if( BARLED_IS_ON > 0 )
    {
         BARLED_IS_ON--;
         if( BARLED_IS_ON == 0 )
         {
              Led_OFF();
         }
    }
    //=============================================================================
    //ret=system("echo \"\" > /tmp/moti.data");
    //ret=system("echo \"\" > /tmp/rakinda.data");
    //=============================================================================
    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}
//=============================================================================
void MainWindow::on_pb_adult_minus_clicked()
{
    DelayTimer = 50;

    if(NbAdult<=0) return ;
    NbAdult--;

    Update_Values();

    ret=system("/usr/bin/start_moti.sh &");
    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_adult_plus_clicked()
{
    DelayTimer = 50;

    if(NbAdult>=9) return;

    NbAdult++;

    Update_Values();

    ret=system("/usr/bin/start_moti.sh &");
    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_child_minus_clicked()
{
    DelayTimer = 50;

    if(NbChild<=0) return;

    NbChild--;

    Update_Values();

    ret=system("/usr/bin/start_moti.sh &");
    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================
void MainWindow::on_pb_child_plus_clicked()
{
    DelayTimer = 50;

    if(NbChild>=9) return;

    NbChild++;

    Update_Values();

    ret=system("/usr/bin/start_moti.sh &");
    ret=system("beep -f 2000 -l 10 &");
}
//=============================================================================



void MainWindow::Clear_Service_buttons()
{
    ui->pb_moti->setStyleSheet("background-color: white;\nborder-color: rgb(32, 74, 135);\ncolor: rgb(20, 60, 118);\nborder: 3px solid rgb(32, 74, 135);");
    ui->pb_barcode->setStyleSheet("background-color: white;\nborder-color: rgb(32, 74, 135);\ncolor: rgb(20, 60, 118);\nborder: 3px solid rgb(32, 74, 135);");
    ui->pb_reboot->setStyleSheet("background-color: white;\nborder-color: rgb(32, 74, 135);\ncolor: rgb(20, 60, 118);\nborder: 3px solid rgb(32, 74, 135);");
}



//=========================================================================================
void MainWindow::on_pb_moti_clicked()
{
    return;
    ui->pb_moti->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("beep &");
    DelayTimer = 5;
    ret=system("echo \"Killall moti\"");
    ret=system("killall moti");
    QCoreApplication::processEvents();
}
//=========================================================================================

//=========================================================================================
void MainWindow::on_pb_barcode_clicked()
{
    return;
    ui->pb_barcode->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("beep &");
    DelayTimer = 5;
    ret=system("echo \"Killall barcode\"");
    ret=system("killall barcode");
    QCoreApplication::processEvents();
}

//=========================================================================================
void MainWindow::on_pb_reboot_clicked()
{
    return;
    ui->pb_reboot->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    Led_OFF();
    ret=system("beep &");
    ret=system("reboot &");
    QCoreApplication::processEvents();
}
//=========================================================================================





//=========================================================================================
void MainWindow::Clear_Fan_buttons()
{
    ui->pb_fan_off->setStyleSheet ("background-color: white;\nborder-color: white;\ncolor: rgb(32, 74, 135);\nborder: 3px solid rgb(32, 74, 135);");
    ui->pb_fan_10->setStyleSheet  ("background-color: white;\nborder-color: white;\ncolor: rgb(32, 74, 135);\nborder: 3px solid rgb(32, 74, 135);");
    ui->pb_fan_50->setStyleSheet  ("background-color: white;\nborder-color: white;\ncolor: rgb(32, 74, 135);\nborder: 3px solid rgb(32, 74, 135);");
    ui->pb_fan_100->setStyleSheet ("background-color: white;\nborder-color: white;\ncolor: rgb(32, 74, 135);\nborder: 3px solid rgb(32, 74, 135);");
    ui->pb_fan_auto->setStyleSheet("background-color: white;\nborder-color: white;\ncolor: rgb(32, 74, 135);\nborder: 3px solid rgb(32, 74, 135);");
}


void MainWindow::on_pb_fan_off_clicked()
{
    Clear_Fan_buttons();

    ui->pb_fan_off->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("i2cset -f -y 4 0x50 0x01 0x71");
    ret=system("echo 0  >   /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1");
    DelayTimer=1800;
    QCoreApplication::processEvents();
}

void MainWindow::on_pb_fan_10_clicked()
{
    Clear_Fan_buttons();

    ui->pb_fan_10->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("i2cset -f -y 4 0x50 0x01 0x71");
    ret=system("echo 64  >   /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1");
    DelayTimer=1800;
    QCoreApplication::processEvents();
}


void MainWindow::on_pb_fan_50_clicked()
{
    Clear_Fan_buttons();

    ui->pb_fan_50->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("i2cset -f -y 4 0x50 0x01 0x71");
    ret=system("echo 128  >   /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1");
    DelayTimer=1800;
    QCoreApplication::processEvents();
}

void MainWindow::on_pb_fan_100_clicked()
{
    Clear_Fan_buttons();

    ui->pb_fan_100->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("i2cset -f -y 4 0x50 0x01 0x71");
    ret=system("echo 255  >   /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1");
    DelayTimer=1800;
    QCoreApplication::processEvents();
}

void MainWindow::on_pb_fan_auto_clicked()
{
    Clear_Fan_buttons();

    ui->pb_fan_auto->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ret=system("i2cset -f -y 4 0x50 0x01 0x72");
    DelayTimer=1800;
    QCoreApplication::processEvents();
}
//=========================================================================================



//=========================================================================================
void MainWindow::on_pb_heat_off_clicked()
{
    ui->pb_heat_off->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;\nborder: 3px solid white;");
    ui->pb_heat_on->setStyleSheet("background-color: white;\nborder-color: white;\ncolor: rgb(32, 74, 135);");
    ret=system("echo 0 > /sys/class/leds/output4/brightness");
}
void MainWindow::on_pb_heat_on_clicked()
{
    ui->pb_heat_on->setStyleSheet("background-color: rgb(32, 74, 135);\nborder-color: white;\ncolor: white;");
    ui->pb_heat_off->setStyleSheet("background-color: white;\nborder-color: white;\ncolor: rgb(32, 74, 135);\nborder: 3px solid rgb(32, 74, 135);");

    ret=system("echo 1 > /sys/class/leds/output4/brightness");
}
//=========================================================================================




//=========================================================================================
int MainWindow::Get_fan_temperature()
{
    int val;

    ret=system("cat /sys/bus/i2c/devices/i2c-4/4-0050/hwmon/hwmon2/temp2_input| cut -c 1-2 > /tmp/fan_temp");
    read_barcode("/tmp/fan_temp");
    val=aTmp_barco.toInt();

    return(val);
}

int MainWindow::Get_fan_register(int point)
{
    QString filePath = QString("/sys/bus/i2c/devices/i2c-4/4-0050/hwmon/hwmon2/pwm1_auto_point%1_pwm").arg(point+1);

    QFile file(filePath);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qWarning() << "Cannot open file:" << filePath;
        return -1;  // Return an error code if file cannot be opened
    }

    QTextStream in(&file);
    QString fileContent = in.readLine();  // Read the first line from the file
    bool ok;
    int value = fileContent.toInt(&ok);   // Convert string to integer

    if (!ok) {
        qWarning() << "Conversion to integer failed for content:" << fileContent;
        return -1;  // Return an error code if conversion fails
    }
    return value;
}
//=========================================================================================
void MainWindow::Update_lookup_table( )
{
    char aTmp[20];
    int i;
    int temperature;
    int val;

    QLabel *pArray[24]=
    {
        ui->label_lookup_01,ui->label_lookup_02,ui->label_lookup_03,ui->label_lookup_04,ui->label_lookup_05,
        ui->label_lookup_06,ui->label_lookup_07,ui->label_lookup_08,ui->label_lookup_09,ui->label_lookup_10,
        ui->label_lookup_11,ui->label_lookup_12,ui->label_lookup_13,ui->label_lookup_14,ui->label_lookup_15,
        ui->label_lookup_16,ui->label_lookup_17,ui->label_lookup_18,ui->label_lookup_19,ui->label_lookup_20,
        ui->label_lookup_21,ui->label_lookup_22,ui->label_lookup_23,ui->label_lookup_24
    };


    temperature = Get_fan_temperature();

    if(temperature < 64 )
    {
        for( i=0 ; i < 24 ; i++ )
        {
            val=Get_fan_register(i);
            sprintf(aTmp,"%02d-%02d : %03d",i*2+16,i*2+18,val);
            pArray[i]->setText(aTmp);

            if(temperature >= ((i*2)+16)  && temperature < ((i*2)+18) )
            {
                pArray[i]->setStyleSheet("background-color: red;\ncolor:white;");
            }
            else
            {
                pArray[i]->setStyleSheet("background-color: white;\ncolor:rgb(32, 74, 135);");
            }
        }
    }
    else
    {
        for( i=0 ; i < 24 ; i++ )
        {
            val=Get_fan_register(i+24);
            sprintf(aTmp,"%03d-%03d : %03d",i*2+64,i*2+66,val);
            pArray[i]->setText(aTmp);

            if(temperature >= ((i*2)+64)  && temperature < ((i*2)+66) )
            {
                pArray[i]->setStyleSheet("background-color: red;\ncolor:white;");
            }
            else
            {
                pArray[i]->setStyleSheet("background-color: white;\ncolor:rgb(32, 74, 135);");
            }

        }
    }
}

//=========================================================================================
