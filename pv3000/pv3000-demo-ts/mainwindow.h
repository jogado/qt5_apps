#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Update_Values();

private slots:
    void MasterTimer_update();
    void MainTask_loop();

    void on_pb_adult_minus_clicked();
    void on_pb_adult_plus_clicked();
    void on_pb_child_minus_clicked();
    void on_pb_child_plus_clicked();

    void Led_RED();
    void Led_GREEN();
    void Led_OFF();

    void on_pb_moti_clicked();
    void on_pb_barcode_clicked();
    void on_pb_reboot_clicked();
    void Clear_Service_buttons();

    void read_header(int header);

    void Clear_Fan_buttons();
    void on_pb_fan_off_clicked();
    void on_pb_fan_10_clicked();
    void on_pb_fan_50_clicked();
    void on_pb_fan_100_clicked();
    void on_pb_fan_auto_clicked();


    void on_pb_heat_off_clicked();
    void on_pb_heat_on_clicked();
    void Update_lookup_table(void);
    int Get_fan_temperature(void);
    int Get_fan_register(int reg);



private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
